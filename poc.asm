;----- 定数 -----
overwrite:equ 0
x0:equ 66
xm:equ 498
y0:equ 18
ym:equ 210
t:equ 80h
f:equ 0
xdots:equ 144
ydots:equ 48
screen:equ 864
fps:equ 38
tire_restmax:equ 42000  ;6D60h
tire_rest_010:equ 4200
tire_consumption:equ 42h
cpu_max_speed:equ 230
cpu_pit_g_count:equ 1bh

physics_num:equ 35
physics:equ 0 - physics_num
dir:equ physics + 0
;degree:equ physics + 4
acceleration:equ physics + 6
speed:equ physics + 10
;slip_x:equ physics + 14
;gravity:equ physics + 15
pos_x:equ physics + 17
pos_y:equ physics + 25
;pos_mod_x:equ physics + 33
;pos_mod_y:equ physics + 34

add_deg_value_cache:equ 0
hundle_slipdir:equ 1
hundle_quick:equ 2
now_point:equ 6
computer_spd:equ 10
auto_pitin_flag:equ 14
lp_end:equ 18

;engine:equ 0
;tune:equ 1
;radiator:equ 2
;brake:equ 3
;engine_brake:equ 4
;tire:equ 5
;wing:equ 6
;hundle:equ 7
;transmission:equ 8
;shiftup:equ 9
;limit:equ 10
;suspension:equ 11
;rpm:equ 12

;gear_ratio:equ 13
;tire_rest:equ 19

;fuel:equ 21
;shiftdown:equ 22
;weight:equ 28

speed_level:equ 16400

grp:equ 0bfd0h
msp:equ 0bff1h
kwait:equ 0bcfdh
kscan:equ 0be53h
vram_view:equ 790dh

org 100h
start:
 ld A, 0a7h
 out (40h), A
 ld A, 40h
 out (40h), A
 call cls
 
 ld A, 0;vram表示開始位置
 ld (vram_view), A
 ld HL, 0
 add HL, SP
 ld (retstack), HL 

 ld C, 2
show_title:
 ld HL, title
 ld DE, 0003h
 ld B, C
 call grp
 ld HL, title + 106
 ld DE, 0103h
 ld B, C
 call grp
 ld HL, title + 212
 ld DE, 0203h
 ld B, C
 call grp
 call TIM10
 inc C
 inc C
 ld A, C
 cp 108
 jr nz, show_title

 ld HL, title_push_z
 ld DE, 0405h
 ld A, 6
 ld C, 6
 call show_block_mes
 call TIM30
show_title_lp:
 call kwait	;ここでkwaitを呼んでおき、入力の誤作動を防止
 cp 51h
 jp z, endpoint
 cp 12h	;X
 jr nz, show_title_lp
 ld (kscan_prev), A

show_menu:
 xor A
 ld (select_menu_prev_index), A
 ld (select_menu_index), A
show_menu_draw:
 call cls
 ld HL, title
 ld DE, 0003h
 ld B, 106
 call grp
 ld HL, title + 106
 ld DE, 0103h
 ld B, 106
 call grp
 ld HL, title + 212
 ld DE, 0203h
 ld B, 106
 call grp

 ld HL, title_menu_1
 ld DE, 0404h
 ld A, 2
 ld C, 6
 call show_block_mes
 
select_menu:
 call select_menu_prompt_draw
select_menu_lp:
 ld B, 111
select_menu_rand_lp:
 call RAND	;乱数を回す回す
 djnz select_menu_rand_lp
 call kscan_ex
 jr z, select_menu_lp
 call keysc		;0bit (left,right,z,x,tab,space,down,up) 7bit
 ld B, A
 ld A, (select_menu_index)
 ld (select_menu_prev_index), A
 bit 0, B
 jr nz, select_menu_lf
 bit 1, B
 jr nz, select_menu_ri
 bit 6, B
 jr nz, select_menu_dw
 bit 7, B
 jr nz, select_menu_up
 bit 3, B
 jp nz, mode_decide

 jr select_menu_lp

select_menu_lf:
 dec A
 jp p, select_menu_comend
 ld A, 4
 jr select_menu_comend
select_menu_ri:
 inc A
 cp 5
 jr c, select_menu_comend
 ld A, 0
 jr select_menu_comend
select_menu_dw:
 ld B, A
 inc A
 inc A
 cp 4
 jr c, select_menu_comend
 ld A, B
 dec A
 dec A
 and 1
 jr select_menu_comend
select_menu_up: 
 ld B, A
 cp 2
 jr c, select_menu_up_2
 dec A
 dec A
 and 1
 jr select_menu_comend
select_menu_up_2:
 inc A
 inc A
 jr select_menu_comend

select_menu_comend:
 ld (select_menu_index), A
 call select_menu_prompt_draw
 jr select_menu_lp

select_menu_prompt_draw:
 ld A, (select_menu_prev_index)
 ld HL, prompt_draw_getDE_table
 call prompt_draw_getDE
 ld HL, chara_del
 ld B, 6
 call grp
 ld A, (select_menu_index)
 ld HL, prompt_draw_getDE_table
 call prompt_draw_getDE
 call prompt_r_view
 ret

prompt_draw_getDE_table:
 dw 0403h, 040ch, 0501h, 0509h, 0510h

;------------ HLテーブルとAインデックスを入れて共通の座標取得 -----------
prompt_draw_getDE:;in A=index HL=table -> ret DE 座標
 ld D, 0
 ld E, A
 add HL, DE
 add HL, DE
 ld E, (HL)
 inc HL
 ld D, (HL)
 ret

mode_decide_table:
 dw setting, free_running, sprint, endurance, option
mode_decide:
 ld A, (select_menu_index)
 ld HL, mode_decide_table

;--------- モードセレクト共通ジャンプ -----------
mode_jump:
 ld D, 0
 ld E, A
 add HL, DE
 add HL, DE
 ld E, (HL)
 inc HL
 ld D, (HL)
 ex DE, HL
 jp (HL) 

setting_menu_index:
 db overwrite
setting_menu_prev_index:
 db overwrite
setting_set_index:
 db overwrite
 
setting:  ;タイトルから
 ld HL, show_menu_draw
 push HL
setting_show_menu:
 call cls
 ld HL, setting_title
 ld DE, 0
 ld A, 3
 ld C, 6
 call show_block_mes
 call reset_setting
 ld HL, (car_weight_st)
 ld H, 0
 call ASCN
 ld HL, NMEMRY + 3
 ld DE, 0107h
 ld B, 2
 call msp
 
 ld A, 2
 ld (setting_menu_index), A
 ld (setting_menu_prev_index), A
 xor A
 ld (setting_set_index), A
 call setting_prompt_draw
 jp setting_select_menu

setting_title:
 db 23, " ", 0eah, "ｾｯﾃｨﾝｸﾞ", 0eah, "   ｾｰﾌﾞ  ﾛｰﾄﾞ"
 db 4, "ｵﾓｻ:"
setting_text:
 db 16, "ﾌﾟﾘｾｯﾄ     ｳｨﾝｸﾞ"
 db 16, "ｴﾝｼﾞﾝ      ﾊﾝﾄﾞﾙ"
 db 20, "ﾌﾞﾚｰｷ      ﾄﾗﾝｽﾐｯｼｮﾝ"
 db 19, "ﾀｲﾔ        ｻｽﾍﾟﾝｼｮﾝ"

setting_select_menu:
 call kscan_ex
 jr z, setting_select_menu
 call keysc		;0bit (left,right,z,x,tab,space,down,up) 7bit
 ld B, A
 ld A, (setting_menu_index)
 ld (setting_menu_prev_index), A
 bit 0, B
 jr nz, setting_select_lf
 bit 1, B
 jr nz, setting_select_ri
 bit 6, B
 jr nz, setting_select_dw
 bit 7, B
 jr nz, setting_select_up
 bit 3, B
 jp nz, setting_mode_decide
 bit 2, B
 jp nz, setting_to_back

 jr setting_select_menu

setting_select_lf:
 or A
 jr z, setting_select_menu
 dec A
 jr setting_select_comend
setting_select_ri:
 cp 9
 jr z, setting_select_menu
 inc A
 jr setting_select_comend
setting_select_dw:
 cp 8
 jr nc, setting_select_menu
 inc A
 inc A
 jr setting_select_comend
setting_select_up:
 cp 2
 jr c, setting_select_menu
 dec A
 dec A
setting_select_comend:
 ld (setting_menu_index), A
 call setting_prompt_draw
 jr setting_select_menu

setting_prompt_draw:
 ld A, (setting_menu_prev_index)
 ld HL, setting_prompt_pos
 call prompt_draw_getDE
 call prompt_delete
 ld A, (setting_menu_index)
 ld HL, setting_prompt_pos
 call prompt_draw_getDE
 call prompt_r_view
 ret

setting_prompt_pos:
 dw 000ch, 0012h, 0202h, 020dh, 0302h, 030dh, 0402h, 040dh, 0502h, 050dh

setting_mode_decide:
 ld HL, setting_mode_decide_table
 jp mode_jump

setting_mode_decide_table:
 dw setting_save, setting_load
 dw setting_preset, setting_wing, setting_engine, setting_hundle
 dw setting_brake, setting_transmission, setting_tire, setting_suspension

setting_to_back:
 ;jp show_menu_draw
 ret

setting_save:
 ld DE, 0004h
 call sub_window_draw
 call choose_file_prt
 cp 255
 jp z, setting_show_menu  ;indexに設定がある
 ld DE, 0002h
 call sub_window_draw
 ld HL, setting_saveload_input
 ld B, 24
 ld DE, 0000h
 call msp
 ld HL, sprint_newgame_mes1 + 16
 ld B, 24
 ld DE, 0100h
 call msp
 call TIM30
 
 ld A, (setting_save_index)
 ld HL, SAVE_SETTING
 ld DE, 25 + 8
 call xsan
 ld (setting_save_write + 1), HL

 ld BC, 8
 ld DE, input_name_memory
 ldir
 
 ld B, 8
 call input_name
 cp 255
 jp z, setting_show_menu

 ld A, (input_name_memory)
 or A
 jp z, setting_show_menu

setting_save_write:
 ld DE, overwrite
 ld HL, input_name_memory
 ld BC, 8
 ldir

 ld HL, car_st_value
 ld BC, 25
 ldir

 ld DE, 0201h
 call sub_window_draw
 ld HL, save_completed_mes
 ld B, 9
 ld DE, 0308h
 call msp
setting_save_lp:
 call kscan_ex
 cp 12h	;X
 jr nz, setting_save_lp

 jp setting_show_menu

save_completed_mes:
 db "ｾｰﾌﾞ ｼﾏｼﾀ"

choose_file_prt:
 ld HL, setting_saveload_mes
 ld DE, 0104h
 ld A, 4
 ld C, 5
 call show_block_mes

 ld HL, SAVE_SETTING
 ld D, 2
choose_file_lp:
 ld E, 0ch
 ld B, 8
 call msp
 inc D
 ld BC, 26
 add HL, BC
 ld A, D
 cp 5
 jr nz, choose_file_lp
 
setting_svld_lp:
 ld A, (setting_save_prev)
 ld D, A
 inc D
 inc D
 ld E, 3
 call prompt_delete

 ld A, (setting_save_index)
 ld D, A
 inc D
 inc D
 ld E, 3
 ld A, ' '
 call prompt_r_view

setting_svld_keylp:
 call kscan_ex
 jr z, setting_svld_keylp
 call keysc
 or A
 jr z, setting_svld_keylp

 ld B, A
 ld HL, setting_save_index
 ld A, (HL)
 ld (setting_save_prev), A
 bit 3, B
 jr nz, setting_done_key
 bit 2, B
 jr nz, setting_cancel_key
 bit 6, B
 jr nz, setting_save_keydw
 bit 7, B
 jr nz, setting_save_keyup
 
 jr setting_svld_lp
 ;jp setting_show_menu

setting_done_key:;ret A
 ret

setting_cancel_key:
 ld A, 255
 ret

setting_save_keydw:
 inc A
 cp 3
 jr c, setting_save_keydw2
 xor A
setting_save_keydw2:
 ld (HL), A
 jr setting_svld_lp

setting_save_keyup:
 dec A
 jp p, setting_save_keydw2
 ld A, 2
 jr setting_save_keydw2
 

setting_save_index:
 db overwrite
setting_save_prev:
 db overwrite

setting_load:
 ld DE, 0004h
 call sub_window_draw
setting_load_lp:
 call choose_file_prt
 cp 255
 jp z, setting_show_menu  ;indexに設定がある
 
 ld A, (setting_save_index)
 ld HL, SAVE_SETTING
 ld DE, 25 + 8
 call xsan
 ld A, (HL) ;セーブ名が存在するかチェック
 or A
 jr z, setting_load_lp

 ld DE, 8
 add HL, DE
 ld DE, car_st_value
 ld BC, 25
 ldir
 
 ld DE, 0201h
 call sub_window_draw
 ld HL, load_completed_mes
 ld B, 9
 ld DE, 0308h
 call msp
setting_load_lp2:
 call kscan_ex
 cp 12h	;X
 jr nz, setting_load_lp2

 jp setting_show_menu

load_completed_mes:
 db "ﾛｰﾄﾞ ｼﾏｼﾀ"
setting_saveload_mes:
 db 17,"ﾌｧｲﾙ ｦ ｴﾗﾝﾃﾞｸﾀﾞｻｲ"
setting_saveload_file:
 db 7,"file 1:"
 db 7,"file 2:"
 db 7,"file 3:"
setting_saveload_input:
 db "   ﾌｧｲﾙﾒｲ ｦ ｲﾚﾃｸﾀﾞｻｲ    "

input_name: ;B=残り入力文字
 push BC
 ld DE, 0201h
 call sub_window_draw

 ld HL, input_name_memory ;シーク
 pop BC
 ld A, B
 ld (input_name_BS_CP + 1), A
 ;ld B, 8  ;残り入力文字

input_name_seek:
 ld A, (HL)
 or A
 jr z, input_name_lp
 inc HL
 dec B
 jr z, input_name_lp
 jr input_name_seek

input_name_lp:
 push HL
 push BC

 ld DE, 0308h
 ld HL, input_name_memory
 ld B, 8
 call msp

 call input_key
 cp 255
 jr z, input_name_endlp
 cp 254 ;BS
 jr z, input_name_BS
 cp 253 ;return
 jr z, input_name_RETURN
 
 pop BC
 pop HL

 ld C, A
 ld A, B
 or A
 jr z, input_name_lp

 ld A, C
 ld (HL), A ;文字入力
 inc HL
 dec B
 jr input_name_lp

input_name_endlp:
 pop BC
 pop HL
 jr input_name_lp

input_name_BS:
 pop BC
 pop HL
 ld A, B
input_name_BS_CP:
 cp overwrite
 ld A, 255  ;キャンセル
 ret z ;入力キャンセル

 inc B
 xor A
 dec HL
 ld (HL), A ;文字を消す
 jr input_name_lp

input_name_RETURN:
 pop BC
 pop HL
 xor A
 ret        ;決定処理

input_name_memory:
 db overwrite, overwrite, overwrite, overwrite
 db overwrite, overwrite, overwrite, overwrite

setting_preset:
 ld DE, 0202h
 call sub_window_draw
 ld DE, 0404h
 call prompt_l_view
 ld DE, 0414h
 call prompt_r_view
 ld HL, setting_preset_mes
 ld DE, 0306h
 ld B, 12
 call msp
setting_preset_select:
 ld HL, setting_preset_mes_kind
 ld A, (setting_preset_index)
 ld DE, 12
 call XSAN
 ld DE, 0406h
 ld B, 12
 call msp
setting_preset_select_lp:
 call kscan_ex
 jr z, setting_preset_select_lp
 call keysc		;0bit (left,right,z,x,tab,space,down,up) 7bit

 ld B, A
 ld A, (setting_preset_index)

 bit 0, B		;left
 jr z, setting_preset_select_2
 dec A
 jp p, setting_preset_select_comend
 ld A, 3
 jr setting_preset_select_comend

setting_preset_select_2:
 bit 1, B		;right
 jr z, setting_preset_select_3
 inc A
 cp 4
 jr c, setting_preset_select_comend
 xor A
 jr setting_preset_select_comend

setting_preset_select_3:	;決定
 bit 3, B
 jr z, setting_preset_select_4
 jr setting_preset_apply

setting_preset_select_4:	;キャンセル
 bit 2, B
 jr z, setting_preset_select_comend
 jp setting_show_menu

setting_preset_select_comend:
 ld (setting_preset_index), A
 jr setting_preset_select

setting_preset_apply:
 ld A, (setting_preset_index)	;耐久 ハイスピード テクニカル ノーマル
 add A, A
 ;ld E, A
 ;ld D, 0
 ld HL, setting_preset_table
 ;add HL, DE
 call HL_ADD_A
 ld E, (HL)
 inc HL
 ld D, (HL)
 ex DE, HL
 ld DE, car_st_value
 ld BC, 25
 ldir

 call show_done_setting
 jp setting_show_menu

setting_preset_index:
 db overwrite
setting_preset_mes:
 db "ﾍﾞｰｽ ｾｯﾃｨﾝｸﾞ"
setting_preset_mes_kind:
 db "   ﾀｲｷｭｳ    "
 db "  ﾊｲｽﾋﾟｰﾄﾞ  "
 db "   ﾃｸﾆｶﾙ    "
 db "   ﾉｰﾏﾙ     "
setting_preset_table:
 dw car_engine_endrun, car_engine_hispeed, car_engine_technical, car_engine_normal

setting_wing:
 ld DE, 0202h
 call sub_window_draw
 ld DE, 0303h
 ld B, 19
 ld HL, setting_wing_mes
 call msp
 ld A, (car_wing_st)
 call setting_select_level_com
 cp 255
 jp z, setting_show_menu
 ld (car_wing_st), A
 
 call show_done_setting
 jp setting_show_menu
setting_wing_mes:
 db "ｺｰﾅｰﾘﾝｸﾞ   ﾊｲｽﾋﾟｰﾄﾞ"

setting_engine:
 ld DE, 0202h
 call sub_window_draw
setting_engine_1:
 ld DE, 0303h
 ld B, 19
 ld HL, setting_engine_mes1
 call msp
 ld A, (car_engine_st)
 call setting_select_level_com
 cp 255
 jp z, setting_show_menu
 ld (car_engine_st), A
setting_engine_2:
 ld DE, 0303h
 ld B, 19
 ld HL, setting_engine_mes2
 call msp
 ld A, (car_tune_st)
 call setting_select_level_com
 cp 255
 jp z, setting_engine_1
 ld (car_tune_st), A

 ld DE, 0303h
 ld B, 19
 ld HL, setting_engine_mes3
 call msp
 ld A, (car_radiator_st)
 call setting_select_level_com
 cp 255
 jp z, setting_engine_2
 ld (car_radiator_st), A
 
 call show_done_setting
 jp setting_show_menu
setting_engine_mes1:
 db "ｹｲﾘｮｳ  ｴﾝｼﾞﾝ  ﾀｲｷｭｳ"
setting_engine_mes2:
 db "ﾃｲｿｸ   ﾁｭｰﾝ   ｺｳｿｸ "
setting_engine_mes3:
 db "ﾁｲｻﾒ  ﾗｼﾞｴｰﾀ  ｵｵｷﾒ "

setting_hundle:
 ld DE, 0202h
 call sub_window_draw
 ld DE, 0303h
 ld B, 19
 ld HL, setting_hundle_mes
 call msp
 ld A, (car_hundle_st)
 ld B, A
 ld A, 7
 sub B		;?ｿｽn?ｿｽ?ｿｽ?ｿｽh?ｿｽ?ｿｽ?ｿｽﾌみ費ｿｽ?ｿｽ]?ｿｽ?ｿｽ?ｿｽﾄ設抵ｿｽ
 call setting_select_level_com
 cp 255
 jp z, setting_show_menu
 ld B, A
 ld A, 7
 sub B		;?ｿｽn?ｿｽ?ｿｽ?ｿｽh?ｿｽ?ｿｽ?ｿｽﾌみ費ｿｽ?ｿｽ]?ｿｽ?ｿｽ?ｿｽﾄ設抵ｿｽ
 ld (car_hundle_st), A

 call show_done_setting
 jp setting_show_menu
setting_hundle_mes:
 db "ｶﾙｲ            ｵﾓｲ "

setting_brake:
 ld DE, 0202h
 call sub_window_draw
setting_brake_1:
 ld DE, 0303h
 ld B, 19
 ld HL, setting_brake_mes
 call msp
 ld A, (car_brake_st)
 call setting_select_level_com
 cp 255
 jp z, setting_show_menu
 ld (car_brake_st), A

 ld DE, 0303h
 ld B, 19
 ld HL, setting_engine_brake_mes
 call msp
 ld A, (car_engine_brake_st)
 call setting_select_level_com
 cp 255
 jp z, setting_brake_1
 ld (car_engine_brake_st), A
 call show_done_setting
 jp setting_show_menu
setting_brake_mes:
 db "ｷｶﾅｲ   ﾌﾞﾚｰｷ   ﾖｸｷｸ"
setting_engine_brake_mes:
 db "ｷｶﾅｲ   ｴﾝﾌﾞﾚ   ﾖｸｷｸ"
 
setting_transmission:
 ld DE, 0202h
 call sub_window_draw
setting_transmission_1:
 ld DE, 0303h
 ld B, 19
 ld HL, setting_transmission_ms1
 call msp
 ld A, (car_transmission_gear_st)
 call setting_select_level_com
 cp 255
 jp z, setting_show_menu
 ld (car_transmission_gear_st), A
setting_transmission_2:
 ld DE, 0303h
 ld B, 19
 ld HL, setting_transmission_ms2
 call msp
 ld A, (car_transmission_shiftup_st)
 ld B, 255
 ld HL, setting_transmission_tbl
setting_transmission_2_lp:
 ld C, (HL)
 inc HL
 inc B
 cp C
 jr c, setting_transmission_2_2
 jr nz, setting_transmission_2_lp
setting_transmission_2_2:
 ld A, B
 call setting_select_level_com
 cp 255
 jp z, setting_transmission_1
 ld HL, setting_transmission_tbl
 call HL_ADD_A
 ld A, (HL)
 ld (car_transmission_shiftup_st), A

 call show_done_setting
 jp setting_show_menu
setting_transmission_ms1:
 db "ﾃｲｿｸﾖﾘ ｷﾞｱﾋ  ｻｲｺｳｿｸ"
setting_transmission_ms2:
 db "ﾃｲｶｲﾃﾝ ﾎﾟｲﾝﾄ ｺｳｶｲﾃﾝ"
setting_transmission_tbl:
 db 65, 80, 95, 109, 121, 132, 140, 147
;----------- トランスミッション情報設定 -------------
setup_gear_ratio:
 ld HL, gear_ration_base
 ld DE, car_gear_ratio_st
 ld BC, 12
 ldir
 ld A, (car_transmission_gear_st)
 ld B, A
 ld A, 7
 sub B  ;A = 7 - (0 ~ 7)  基礎増分 / 2
 or A
 ret z
 ld B, 7;掛ける数
 ld HL, car_gear_ratio_st
setup_gear_ratio_lp:
 push AF
 push BC
 ld E, (HL)
 inc HL
 ld D, (HL)
 dec HL
 ex DE, HL
 ld C, A
setup_gear_ratio_lp2:
 add A, C
 djnz setup_gear_ratio_lp2
 call HL_ADD_A
 ex DE, HL
 ld (HL), E
 inc HL
 ld (HL), D
 inc HL
 pop BC
 pop AF
 dec B
 dec B
 ret z
 inc B
 jr setup_gear_ratio_lp
 ret

gear_ration_base:
 dw 280, 182, 138, 101, 82, 70
;最大で
;326, 84
;+49(*7) +14(*2)

setup_shift_down:
 ld HL, car_transmission_shiftdown_st
 xor A
 ld (HL), A
 inc HL
 ld B, 5
 exx
 ;スピードテーブル設定
 ld HL, car_gear_speed_table_st
 ld DE, car_gear_ratio_st
 call init_speed_table
 ld HL, car_gear_speed_table_st		;車による IX
 ;シフトダウン設定
setting_transmission_comp_l:
 ld A, (car_transmission_shiftup_st)
 ld E, A
 ld D, 0
 add HL, DE
 ld A, (HL)		;シフトアップ時のスピードテーブルの値
 ld B, A
 ld C, 0
 or A
 sbc HL, DE
 ld DE, 160
 add HL, DE		;次のスピードテーブルへ
 push HL	;
setting_transmission_comp_ll:
 ld A, (HL)		;次のギアのスピードテーブルの値と、シフトアップ時のスピードテーブルの値を比較
 cp B			;
 jr nc, setting_transmission_comp_lll
 inc HL
 inc C
 jr setting_transmission_comp_ll
setting_transmission_comp_lll:
 dec C
 ld A, C
 exx
 ld (HL), A		;car_gear_speed_table_st + (1 ~ 5)
 ;call log_A_kwait
 ;call log_HL_kwait
 inc HL
 dec B
 exx
 pop HL		;
 jp nz, setting_transmission_comp_l

 ret

setting_transmission_prev:
 db overwrite
setting_transmission_index:
 db overwrite
;setting_transmission_mes1:
; db "ﾀｲﾌﾟ"
;setting_transmission_mes2:
; db 3,"ｼﾌﾄ"
;setting_transmission_mes3:
; db 4,"ﾘﾐｯﾄ"
;setting_transmission_mes4:
; db 8,"ｷﾞﾔﾋｾｯﾃｲ"
;setting_transmission_mes5:
; db 3,"ｵﾜﾙ"
;setting_transmission_mes6:
; db "over."
;setting_transmission_mes:
; db "ｵｰﾄﾏﾁｯｸ"
; db " ﾏﾆｭｱﾙ "
;setting_gear_ratio_mes:
; db 13,"1st       4th"
; db 13,"2nd       5th"
; db 13,"3rd       6th"

setting_tire:
 call tire_setting
 jp z, setting_show_menu
 ld (car_tire_st), A
 call show_done_setting
 jp setting_show_menu
setting_tire_mes:
 db "ﾛﾝｸﾞﾗｲﾌ    ﾊｲｸﾞﾘｯﾌﾟ"

tire_setting:
 ld DE, 0202h
 call sub_window_draw
 ld DE, 0303h
 ld B, 19
 ld HL, setting_tire_mes
 call msp
 ld A, (car_tire_st)
 call setting_select_level_com
 cp 255
 ret

setting_suspension:
 ld DE, 0202h
 call sub_window_draw
 ld DE, 0303h
 ld B, 19
 ld HL, setting_suspension_mes
 call msp
 ld A, (car_suspension_st)
 call setting_select_level_com
 cp 255
 jp z, setting_show_menu
 ld (car_suspension_st), A
 call show_done_setting
 jp setting_show_menu
setting_suspension_mes:
 db "ﾔﾜﾗｶ           ｶﾀﾒ "

;-------- セッティング完了ウィンドウ --------
show_done_setting:
 ld DE, 0201h
 call sub_window_draw
 ld HL, setting_completed_mes
 ld B, 12
 ld DE, 0306h
 call msp
show_done_setting_lp:
 call kscan_ex
 cp 12h	;X
 jr nz, show_done_setting_lp
 ret

;-------- レベルを選択する処理 --------
setting_select_level_com:;A=初期設定値
 and 00000111b
 ld (setting_select_level_com_index), A
 ld (setting_select_level_com_prev_index), A
 ld DE, 0405h
 ld A, D
 ld (setting_select_level_com_D1 + 1), A	;削除行
 ld (setting_select_level_com_D2 + 1), A	;描画行
 ld (setting_select_level_com_D3 + 1), A	;削除行
 ld HL, setting_set_level_mes
 ld B, 15
 call msp
setting_select_level_com_prompt:
 ld A, (setting_select_level_com_prev_index)
 ld HL, setting_select_level_com_table
 call prompt_draw_getDE
setting_select_level_com_D1:
 ld D, overwrite
 ld HL, grp
 ld B, 6
 call prompt_delete
 ld A, (setting_select_level_com_index)
 ld HL, setting_select_level_com_table
 call prompt_draw_getDE
setting_select_level_com_D2:
 ld D, overwrite
 call prompt_r_view
setting_select_ksc:
 call kscan_ex
 jr z, setting_select_ksc
 call keysc
 ld B, A
 ld A, (setting_select_level_com_index)

 bit 0, B
 jr z, setting_select_level_prompt_2
 dec A
 jp p, setting_select_level_prompt_com
 ld A, 7
 jr setting_select_level_prompt_com
setting_select_level_prompt_2:
 bit 1, B
 jr z, setting_select_level_prompt_3
 inc A
 cp 8
 jr c, setting_select_level_prompt_com
 xor A
 jr setting_select_level_prompt_com
setting_select_level_prompt_3:
 bit 3, B
 jr z, setting_select_level_prompt_4
setting_select_level_prompt_del:
 ld HL, setting_select_level_com_table
 call prompt_draw_getDE
setting_select_level_com_D3:
 ld D, overwrite
 ld HL, chara_del
 ld B, 6
 call prompt_delete
 ld A, (setting_select_level_com_index)
 ret

setting_select_level_prompt_4:
 bit 2, B
 jr z, setting_select_level_prompt_com
 ld C, A
 ld A, 255
 ld (setting_select_level_com_index), A
 ld A, C
 jr setting_select_level_prompt_del

setting_select_level_prompt_com:
 ld C, A
 ld A, (setting_select_level_com_index)
 ld (setting_select_level_com_prev_index), A
 ld A, C
 ld (setting_select_level_com_index), A
 jp setting_select_level_com_prompt

setting_select_level_com_index:
 db overwrite
setting_select_level_com_prev_index:
 db overwrite
setting_select_level_com_table:
 dw 0004h, 0006h, 0008h, 000ah, 000ch, 000eh, 0010h, 0012h
setting_completed_mes:
 db "ｾｯﾃｨﾝｸﾞ ｼﾏｼﾀ"
setting_set_level_mes:
 db "1 2 3 4 5 6 7 8"

;--------- ウィンドウ描画 ------------
;D=開始行,E=空白の行数 開始行の1行前から描画する
sub_window_draw:
 ld B, E
 ld A, D
 inc A
 ld (sub_window_draw_next_linecheck + 1), A

 ld HL, sub_window_img_map
 ld (sub_window_draw_lp + 1), HL
 ld E, 2
sub_window_draw_lp:
 ld HL, overwrite
 push DE
 ld E, (HL)
 inc HL
 ld D, (HL)
 inc HL
 ld HL, (sub_window_draw_lp + 1)
 inc HL
 inc HL
 ld (sub_window_draw_lp + 1), HL
 ex DE, HL
 pop DE
sub_window_draw_chara_ptr:
 push HL
 call chara_ptr
 pop HL
 ld A, E
 inc A
 ld E, A
 cp 3	;横始まり-
 jr z, sub_window_draw_lp
 cp 22	;最後]
 jr z, sub_window_draw_lp
 cp 23	;最後 次の行へ
 jr z, sub_window_draw_next
 jr sub_window_draw_chara_ptr
sub_window_draw_next:
 ld A, B
 or A
 ret z	;最後の行で空白行カウンタがゼロならreturn

 inc D		;改行
 ld E, 2	;列リセット
sub_window_draw_next_linecheck:
 ld A, overwrite
 cp D
 jr z, sub_window_draw_space
 dec B
 jr z, sub_window_draw_lp	; 最後の行の描画
 ld HL, sub_window_img_map + 6
 ld (sub_window_draw_lp + 1), HL
sub_window_draw_space:
 push DE
 push HL
 push BC
 ld A, ' '
 ld B, 22
 call 0bfeeh
 pop BC
 pop HL
 pop DE
 jr sub_window_draw_lp	; 空白行の描画

chara_ptr:;DE=座標
 push HL
 push DE
 push BC
 ld A, D
 or A
 jr z, chara_ptr_E
 ld B, D
 ld A, 0
chara_ptr_D:
 add A, 8
 djnz chara_ptr_D
 ld D, A
chara_ptr_E:
 ld A, E
 or A
 jr z, chara_ptr_drawing
 ld B, E
 ld A, 0
chara_ptr_EE:
 add A, 6
 djnz chara_ptr_EE
 ld E, A
chara_ptr_drawing:
 ld B, 6
 ld A, 3
 call grp_set_A
 pop BC
 pop DE
 pop HL
 ret

sub_window_img_map:
 dw sub_window_img_ul, sub_window_img_u, sub_window_img_ur
 dw sub_window_img_l, chara_del, sub_window_img_r
 dw sub_window_img_dl, sub_window_img_d, sub_window_img_dr

sub_window_img_ul:
 db 00,00,00,0e0h,0e0h,60h
sub_window_img_dl:
 db 0,0,0,3,3,3
sub_window_img_ur:
 db 0e0h,0e0h,0,0,0,0
sub_window_img_dr:
 db 3,3,0,0,0,0
sub_window_img_u:
 db 60h,60h,60h,60h,60h,60h
sub_window_img_d:
 db 3,3,3,3,3,3
sub_window_img_l:
 db 0,0,0,0ffh,0ffh,0
sub_window_img_r:
 db 0ffh,0ffh,0,0,0,0

free_running:
 ld A, 1
 ld (game_mode), A  ;ゲームモードを設定
free_running_cource_select:
 call fram_clear
 call cource_select
 cp 255
 jp z, show_menu_draw
 ld A, 99
 ld C, 0
 call chage_fuel
 cp 255
 jr z, free_running_cource_select
 call LAP_SETUP
 ld A, (game_mode)
 cp 1
 jp nz, race
 call remove_enemys
 jp race

sprint:;ポイント制のワールドチャンピオンシップツアー 全4コースを予定
 ld A, 2
 ld (game_mode), A  ;ゲームモードを設定
 ;セーブデータロード
 ld A, (SAVE_NEXT_COURCE)
 or A
 jp z, sprint_newgame

;ハジメカラ ツヅキカラ を選ぶ
 ld DE, 0202h
 call sub_window_draw 
 ld HL, sprint_start_select_mes
 ld DE, 030ah
 ld A, 0ah
 ld C, 5
 call show_block_mes
sprint_prompt_draw:
 ld A, (sprint_prev)
 add A, 3
 ld D, A
 ld E, 8
 call prompt_delete
 ld A, (sprint_index)
 add A, 3
 ld D, A
 ld E, 8
 call prompt_r_view
sprint_kscan:
 call kscan_ex
 jr z, sprint_kscan
 call keysc
 ld B, A
 ld HL, sprint_index
 ld A, (HL)
 ld (sprint_prev), A
 bit 2, B
 jr nz, splint_cancel
 bit 3, B
 jr nz, splint_done
 bit 6, B
 jr nz, splint_down
 bit 7, B
 jr nz, splint_up
 jr sprint_kscan
splint_cancel:
 jp show_menu_draw
splint_done:
 or A
 jp nz, sprint_continue
 jr sprint_newgame
splint_down:
 inc A
splint_down_du:
 and 00000001b
 ld (HL), A
 jr sprint_prompt_draw
splint_up:
 dec A
 jr splint_down_du

sprint_index:
 db overwrite
sprint_prev:
 db overwrite
sprint_start_select_mes:
 db 6, "ﾊｼﾞﾒｶﾗ"
 db 6, "ﾂﾂﾞｷｶﾗ"
sprint_start_select_mes_w:
 db 15, "ｾｰﾌﾞﾃﾞｰﾀ ｻｸｼﾞｮ?"
 db 8, "Yes / No"

sprint_newgame:
 call cls
 ld HL, sprint_newgame_mes1
 ld DE, 0006h
 xor A
 ld C, 2
 call show_block_mes
 call TIM30
 
 ld HL, input_name_memory
 xor A
 ld (HL), A
 ld BC, 7
 ld DE, input_name_memory + 1
 ldir
 
 ld B, 4
 call input_name
 cp 255
 jp z, show_menu_draw
 ld A, (game_mode)
 cp 3
 jp z, endurance_2
;クラス選択
sprint_class_select:
 ld DE, 0004h
 call sub_window_draw
 ld HL, sprint_newgame_mes2
 ld DE, 0104h
 ld A, 10
 ld C, 5
 call show_block_mes
sprint_class_prp_draw:
 ld E, 8
 ld A, (sprint_class_select_prev)
 inc A
 inc A
 ld D, A
 call prompt_delete
 ld E, 8
 ld A, (sprint_class_select_idx)
 inc A
 inc A
 ld D, A
 call prompt_r_view
sprint_class_input:
 call kscan_ex
 jr z, sprint_class_input
 call keysc
 ld B, A
 ld HL, sprint_class_select_idx
 ld A, (HL)
 ld (sprint_class_select_prev), A

 bit 3, B
 jr nz, sprint_class_input_done
 bit 6, B
 jr nz, sprint_class_input_down
 bit 7, B
 jr nz, sprint_class_input_up
 jp sprint_class_input
sprint_class_input_done:
;クラス、名前、次のコースををSAVE_SPRINTに保存
 push AF
;警告メッセージ
 ld A, (SAVE_NEXT_COURCE)
 or A
 jr z, sprint_class_input_done_y
 call cls
 ld DE, 0102h
 call sub_window_draw
 ld DE, 0205h
 ld HL, sprint_start_select_mes_w
 ld A, 7
 ld C, 4
 call show_block_mes
splint_done_sc:
 call kscan_ex
 jr z, splint_done_sc
 cp 7 ;Y
 jp z, sprint_class_input_done_y
 cp 16h ;N
 jp z, show_menu_draw
 jr splint_done_sc

sprint_class_input_done_y:
 ld DE, SAVE_NAME
 ld HL, input_name_memory
 ld BC, 8
 ldir
 xor A;ld A, 2;現在はテストコース;xor A      ;コース0 SAVE_NEXT_COURCE
 ld (DE), A ;次コース保存
 inc DE
 pop BC
 ld A, 2
 sub B
 ld (DE), A ;0:ﾜｰﾙﾄﾞ 1:ｺｸﾅｲ 2:ﾉｰﾋﾞｽ
 ld (game_class), A
 ld HL, SAVE_POINT
 ld (HL), 0
 ld DE, SAVE_POINT + 1
 ld BC, 3
 ldir
 jp sprint_info
sprint_class_input_down:
 inc A
 cp 3
 jr c, sprint_class_input_du
 xor A
sprint_class_input_du:
 ld (HL), A
 jp sprint_class_prp_draw
sprint_class_input_up:
 dec A
 jp p, sprint_class_input_du
 ld A, 2
 ld (HL), A
 jp sprint_class_prp_draw
 
sprint_class_select_idx:
 db overwrite
sprint_class_select_prev:
 db overwrite

sprint_continue:
 ld A, (SAVE_NEXT_COURCE)
 cp 3
 jp z, season_end
;情報表示
sprint_info:
 call cls
 ld DE, 0004h
 call sub_window_draw
 ld HL, sprint_info_mes
 ld DE, 0104h
 ld A, 3
 ld C, 5
 call show_block_mes
 ld HL, sprint_info_class
;クラス名
 ld A, (SAVE_CLASS)
 ld DE, 5
 call xsan
 ld DE, 010fh
 ld B, 5
 call msp
;◯戦
 ld A, (SAVE_NEXT_COURCE)
 inc A
 add A, 30h
 ld DE, 0203h
 call 0be62h
;周回数
 ld A, (SAVE_NEXT_COURCE)
 ld (cource_current), A
 call LAP_SETUP
 ld HL, (lap_remind)
 ld H, 0
 call ASCN
 ld HL, NMEMRY + 3
 ld DE, 0208h
 ld B, 2
 call msp
;コース名
 ld A, (SAVE_NEXT_COURCE)
 ld DE, 10
 ld HL, cource_select_n1
 call xsan
 ld DE, 020ch
 ld B, 10
 call msp
sprint_info_prompt:
 ld A, (sprint_info_prev)
 add A, 3
 ld D, A
 ld E, 6
 call prompt_delete
 ld A, (sprint_info_index)
 add A, 3
 ld D, A
 ld E, 6
 call prompt_r_view
sprint_info_input:
 call kscan_ex
 jr z, sprint_info_input
 call keysc
 ld B, A
 ld HL, sprint_info_index
 ld A, (HL)
 ld (sprint_info_prev), A
 bit 2, B
 jr nz, sprint_info_cancel
 bit 3, B
 jr nz, sprint_info_done
 bit 6, B
 jr nz, sprint_info_down
 bit 7, B
 jr nz, sprint_info_up
 jr sprint_info_input

sprint_info_cancel:
 jp show_menu_draw
sprint_info_done:
 or A
 jr z, sprint_race
 call setting_show_menu
 jp sprint_info
sprint_info_down:
 inc A
sprint_info_du:
 and 00000001b
 ld (HL), A
 jr sprint_info_prompt
sprint_info_up:
 dec A
 jr sprint_info_du

sprint_info_index:
 db overwrite
sprint_info_prev:
 db overwrite

sprint_race:
 call fram_clear
 ld A, (SAVE_NEXT_COURCE)
 call cource_done_com
 ld A, 2
 ld (game_mode), A  ;ゲームモードを設定
 ld A, 99
 ld C, 0
 call chage_fuel
 cp 255
 jp z, start
;展開処理など
 jp race

sprint_newgame_mes1:
 db 14, "ﾅﾏｴ ｦ ｲﾚﾃｸﾀﾞｻｲ"
 db 24, "ﾓｼﾞﾆｭｳﾘｮｸ BS:ｹｽ ENT:ｹｯﾃｲ"
sprint_newgame_mes2:
 db 16, "ｸﾗｽ ｦ ｴﾗﾝﾃﾞｸﾀﾞｻｲ"
 db 5, "ﾉｰﾋﾞｽ"
 db 4, "ｺｸﾅｲ"
 db 5, "ﾜｰﾙﾄﾞ"

sprint_info_mes:
 db 9, "ｽﾌﾟﾘﾝﾄﾚｰｽ"
 db 8, "_ｾﾝﾒ __L"
 db 9, "     ｽﾀｰﾄ"
 db 12, "     ｾｯﾃｨﾝｸﾞ"
sprint_info_class:
 db "ﾜｰﾙﾄﾞ"
 db "ｺｸﾅｲ "
 db "ﾉｰﾋﾞｽ"

;シーズンが終わって一位ならエンディング、そうでなければメッセージ
season_end:
 ld HL, SAVE_POINT
 ld A, (HL)
 inc HL
 ld E, 1  ;シーズン何位か
 ld B, 3
season_end_lp:
 ld C, (HL)
 inc HL
 cp C
 jr nc, season_end_2
season_end_z:
 inc E
season_end_2:
 djnz season_end_lp
 push DE

 pop DE
 ld A, E
 cp 1
 jr z, season_end_1st
 add A, 30h
 ld (season_end_mes + 11), A
season_end_print:
 call cls
 ld HL, season_end_mes
 ld DE, 0004h
 ld A, 1
 ld C, 6
 call show_block_mes
 ld A, (game_mode)
 cp 3
 jr nz, season_end_print_kw
 ld DE, 0004h
 ld B, 5
 ld A, ' '
 call 0bfeeh
 ld A, (rank)
 add A, 30h
 ld DE, 14
 call 0be62h
season_end_print_kw:
 call TIM255
 call TIM255
 call kwait
 jp start
season_end_1st:
 jp ending

season_end_mes:
 db 13, "ｼｰｽﾞﾝｾｲｾｷ _ ｲ"
 db 14, "    ｵﾂｶﾚｻﾏﾃﾞｼﾀ"
 db 21, "ﾗｲ ｼｰｽﾞﾝ ﾊ 1ｲ ｦ ﾒｻﾞｼﾃ"
 db 15, "   ｶﾞﾝﾊﾞｯﾃｸﾀﾞｻｲ"
 db 1, " "
 db 22, "THANK YOU FOR PLAYING!"


endurance_name:
 db overwrite, overwrite, overwrite, overwrite
endurance:;位置コースを長い周回数で競う
 ld A, 3
 ld (game_mode), A  ;ゲームモードを設定
 jp sprint_newgame
endurance_2:
 ld HL, input_name_memory
 ld DE, endurance_name
 ld BC, 4
 ldir
 xor A
 ld (game_class), A  ;ゲームモードを設定
 jp free_running_cource_select

option:
 call cls
 ld DE, 4
 call sub_window_draw
option_recdraw:
 ld DE, 0005h
 ld HL, option_mes
 ld A, 6
 ld C, 2
 call show_block_mes
 ld HL, cource_select_n1
 ld D, 2
 ld B, 3
option_recdraw_lp:
 push BC
 ld E, 4
 ld B, 10
 call msp
 inc D
 inc HL
 pop BC
 djnz option_recdraw_lp

 ld HL, SAVE_RECORD
 ld D, 2
 ld B, 3
option_recdraw_lp_2:
 push BC
 push HL
 ld (option_recdraw_HL + 1), HL
option_recdraw_HL:
 ld HL, (overwrite)
 push DE
 call ASCN
 ld HL, NMEMRY + 1
 ld B, 4
 pop DE
 ld E, 15
 call msp
 inc D
 pop HL
 inc HL
 inc HL
 pop BC
 djnz option_recdraw_lp_2
option_recdraw_KEY:
 call kscan_ex
 jr z, option_recdraw_KEY
 cp 12h	;X
 jr z, wait_setting
 cp 11h	;Z
 jp z, show_menu_draw
 jr option_recdraw_KEY

wait_setting:
 ld DE, 0202h
 call sub_window_draw
 ld HL, option_mes + 3
 ld B, 4
 ld DE, 030ah
 call msp
 ld A, (TIM_BEEP_WAIT_A + 1)
 srl A
 call setting_select_level_com
 cp 255
 jr z, option
 sla A
 ld (TIM_BEEP_WAIT_A + 1), A
 call show_done_setting
 jp option

option_mes:
 db 14, "X:ｳｪｲﾄ  Z:ﾓﾄﾞﾙ"
 db 11, "** ﾚｺｰﾄﾞ **"

select_menu_index:
 db overwrite
select_menu_prev_index:
 db overwrite

cource_select:
 call cls
 
 ld DE, 0004h
 call sub_window_draw

 ld HL, cource_select_mes
 ld DE, 0105h
 ld B, 16
 call msp

 ld DE, 0304h
 call prompt_l_view

 ld DE, 0314h
 call prompt_r_view

 ld C, 0
cource_select_lp:
 push BC
 ld HL, cource_select_n1
 ld A, C
 ld DE, 10
 call xsan
 ld B, 10
 ld DE, 0308h
 call msp
cource_keyinput:
 call kscan_ex
 jr z, cource_keyinput
 call keysc
 or A
 jr z, cource_keyinput

 pop BC
 ld B, A
cource_select_left:
 bit 0, B
 jr z, cource_select_right
 ld A, C
 dec A
 cp 255
 jr z, cource_select_lp
 ld C, A
 jr cource_select_lp
cource_select_right:
 bit 1, B
 jr z, cource_select_done
 ld A, C
 inc A
 cp 3
 jr z, cource_select_lp
 ld C, A
 jr z, cource_select_lp
cource_select_done:
 bit 3, B
 jr nz, cource_done
 bit 2, B
 jr nz, cource_cancel
 jr cource_select_lp

cource_done:
;コース選択
 ld A, C
 ;コースNo
cource_done_com:
 ld (cource_current), A
 xor A
 ld (cource_inited), A      ;コース選択時に直す
 call BGUNZIP
 call COURCE_SETUP
 xor A
 ret

cource_cancel:
 ld A, 255
 ret

cource_select_mes:
 db "ｺｰｽ ｦ ｴﾗﾝﾃﾞｸﾀﾞｻｲ"
cource_select_n1:
 db "ｽﾋﾟｰﾄﾞｳｪｲ "
cource_select_n2:
 db " ﾓﾝﾄ ｼﾃｨ  "
cource_select_n3:
 db "ﾍｱﾋﾟﾝﾊﾟｰｸ "

;初期燃料数設定
chage_fuel:;A=初期値 C=下限量
 ld (chage_fuel_A + 1), A
 ld A, C
 ld (chage_fuel_left + 1), A
 call cls
 
 ld DE, 0004h
 call sub_window_draw

 ld HL, chage_fuel_mes
 ld DE, 0103h
 ld B, 18
 call msp

 ld DE, 0307h
 call prompt_l_view
 ld DE, 030fh
 call prompt_r_view

chage_fuel_A:
 ld A, overwrite ;100 L
chage_fuel_lp:
 push AF

 inc A
 ld L, A
 ld H, 0
 call ASCN

 ld HL, NMEMRY + 2
 ld DE, 0309h
 ld B, 3
 call msp

 ld A, 'L'
 ld DE, 030Dh
 call 0be62h
 
chage_fuel_lp_15:
 call kscan
 cp 12h
 jr z, chage_fuel_lp_15
chage_fuel_lp2:
 call kscan
 or A
 jr z, chage_fuel_lp2
 cp 51h
 jp z, endpoint
 call keysc
 or A
 jr z, chage_fuel_lp2
 ld B, A

 pop AF

 bit 0, B
 jr nz, chage_fuel_left
 bit 1, B
 jr nz, chage_fuel_right
 bit 3, B
 jr nz, chage_fuel_done
 bit 2, B
 jr nz, chage_fuel_canc
 jr chage_fuel_lp

chage_fuel_left:
 cp overwrite
 jp z, chage_fuel_lp
 dec A
 jp chage_fuel_lp
chage_fuel_right:
 cp 254
 jp nc, chage_fuel_lp
 inc A
 jp chage_fuel_lp
chage_fuel_done:
 inc A
 ld (car_fuel_st + 1), A
 xor A
 ld (car_fuel_st), A
 ret
chage_fuel_canc:
 ld A, 11h	;Z
 ld (kscan_prev), A
 ld A, 255
 ret

chage_fuel_mes:
 db "ﾅﾝ ﾘｯﾄﾙ ﾏﾃﾞ ｲﾚﾏｽｶ?"

;DEのみ指定
prompt_delete:
 ld A, ' '
 call 0be62h
 ret

prompt_r_view:  ;DEのみ指定
 ld HL, prompt_r
prompt_view:
 ld B, 6
 call grp
 ret
prompt_l_view:
 ld HL, prompt_l
 jr prompt_view


 ;故障を直し重さを算出
reset_setting:
 ld B, 11
 ld HL, car_st_value
race_init_fixlp:
 ld A, 00000111b
 and (HL)
 ld (HL), A
 inc HL
 djnz race_init_fixlp
 ;電気系初期化
 xor A
 ld (car_elect_st), A
;初期ギア設定
 ld A, 00000001b
 ld (car_transmission_st), A
;タイヤ残り設定
 ld HL, tire_restmax
 ld (car_tire_rest_st), HL
;重さ設定
 ld A, (car_engine_st)
 and 00000111b
 ;sla A
 ld E, A
 ld A, (car_radiator_st)
 and 00000111b
 add A, E
 ;ld E, A
 ;ld A, (car_wing_st)
 ;and 00000111b
 ;srl
 ;ld B, A
 ;ld A, 7
 ;sub B
 ;add A, E
 ;call log_A_kwait
 ld (car_weight_st), A
 ret

race:
race_init:
;BEEP_WAIT設定
 ld HL, TIM_BEEP_WAIT + 1
 ld A, 2
 ld (HL), A
 ld A, (game_mode)
 cp 1
 jr nz, race_init_2
 ld A, 7
 ld (HL), A
race_init_2:
;ギアレシオ設定
 call setup_gear_ratio
;シフトダウンポイント設定
 call setup_shift_down
;画面反転解除
 ld A, 0a6h
 out (40h), A
;初期回転数設定
 ld A, 11
 ld (car_rpm_st), A
;セッティング故障初期化
 call reset_setting
;現在のobjno設定
;g_count 0初期化
;交差位置初期化
;クラッシュフラグリセット
;ピットインフラグリセット
 xor A
 ld HL, objno
 ld (HL), A
 ld DE, objno + 1
 ld BC, 13
 ldir
 ;32 byte -> 13 byte
; ld (objno), A
;;交差位置初期化
; ;ld (cross_pos), A
; ;ld (cross_be), A
;;クラッシュフラグリセット
; ld (car_crash_fl), A
;;ピットインフラグリセット
; ld (car_pitin), A
; ld (car_pitin + 1), A
; ld (car_pitin + 2), A
; ld (car_pitin + 3), A
;レースの終わりフラグリセット
 ld HL, lap_end
 ;xor A
 ld (HL), A
;各車のレース終了状況リセット
 inc HL
 ld (HL), A
 inc HL
 ld (HL), A
 inc HL
 ld (HL), A
;ピットサイン表示タイマーリセット
 ld (SET_PIT_SIGN_TIMER), A
;周回フラグリセット
 ld (lap_action_collided), A
 ld (lap_reverse_lap), A
 ld HL, lap_time
 ld (HL), A
 inc HL
 ld (HL), A
;クラッシュタイマーリセット
 ld (ENDTIMER_COUNT), A
;g_countランダム初期化
 ld HL, g_count_1
 ld B, 4
race_init_g_count:
 ld (HL), A
 inc HL
 call rand
 and 7
 ld (HL), A
 inc HL
 djnz race_init_g_count
;芝生踏み変数リセット
 ld A, 1
 ld (on_sand), A
 ld A, fps * 5
 ld (START_TIM), A

objfetch:
 ld HL, g_count_1
 ld B, 4
objfetch_inc_gcnt:
 inc (HL)
 inc HL
 jr nz, objfetch_l2
 inc (HL)
 jr nz, objfetch_l2
 dec (HL)
objfetch_l2:
 inc HL
 djnz objfetch_inc_gcnt
objfetch2:
 ld IY, objlist
 ld IX, car_temp_values	;car_status用
 xor A
 ld (obj_index), A
objfetch_loop:
 ld A, (IY)
 inc IY
 ld (objno), A
 cp 80h
 jr z, objfetch
 cp 4
 jp c, car_action	;0 ~ 3 車のアクション
 ;jp z, sand_action	;4 芝生 LCDから踏んでいるか取得する
 jp z, TIM_BEEP
 cp 6
 jp c, block_action	;5 障害物
 jp z, pit_action	;6 ピットアクション
 cp 8
 jp c, lap_action	;周回テープ処理
 jp z, tonnel_start_action
 cp 0ah
 jp c, tonnel_end_action
 jp z, cross_bottom
 cp 0ch
 jp c, cross_upper
 jp z, cross_action
 cp 0eh
 jp c, lap_action_bottom	;周回テープ処理
 jp z, lap_action_upper	;周回テープ処理
 cp 0fh
 jp z, sand_action_few
objfetch_loop_end:
 ld HL, obj_index
 inc (HL)
 jr objfetch_loop

obj_index:	;オブジェクトのインデックス
 db overwrite

get_obj_pos_rect_address:
 ld A, (obj_index)
 ld L, A
 ld H, 0
 add HL, HL ;*2
 add HL, HL ;*4
 add HL, HL ;*8
 ex DE, HL
 ld HL, objlist_pos_rect
 add HL, DE	;HL = 座標、サイズアドレス
 ret

check_collision:	;A = 補正値 HL = アドレス DE = 車のポジション変数アドレス ret cyで接触可能性 HL += 2
 ld (block_action_A + 1), A
 ex DE, HL
 ld (block_action_HL + 1), HL
 ex DE, HL
 ld E, (HL)
 inc HL
 ld D, (HL)
 inc HL
 ex DE, HL	;DE = アドレス HL = ブロック値
 push DE
check_collision_BC:
 ld BC, overwrite
 add HL, BC
 ld (block_action_posDE + 1), HL
block_action_HL:
 ld HL, (overwrite)	;指定する車座標
block_action_A:
 ld A, overwrite	;補正値 +
 ;ld D, 0
 ;add HL, DE
 call HL_ADD_A
 or A
block_action_posDE:
 ld DE, overwrite	;ブロック座標 + 領域座標
 ;call log_HL_kwait
 ;call log_DE_kwait
 sbc HL, DE	;現左上座標+補正値 - ブロック左上座標　ncyで接触以上
 pop HL
 ;call log_HL_kwait
 ret	;cy:左側 ncy:接触、侵入

sand_action_few:
 ld HL, sand_action_few_do
 jp block_action_com
sand_action_few_do:
 call sand_action
 jp objfetch_loop_end

on_sand:  ;2で芝生の上
 db overwrite
sand_action:
 ld A, (car_speed)
 cp 120
 ret c
 
 call RAND
 cp 99
 jr z, suspension_break
 cp 47
 jr z, radiator_break
 cp 134
 jr z, transmission_break
 cp 66
 jr z, electron_break
 cp 254
 jr z, brake_break

 ret

suspension_break:
 ;1/256
 ld HL, car_suspension_st
 set 4, (HL)
 jr brakeend
radiator_break:
 call RAND
 cp 128
 ret c
 ld HL, car_radiator_st ;1/512
 set 7, (HL)
 jr brakeend
transmission_break:
 call RAND
 cp 224
 ret c
 ld HL, car_transmission_st  ;1/2048
 ld A, (HL)
 bit 7, (HL)
 ret nz
transmission_break_l:
 ld L, A
 call RAND
 and 01110000b  ;1 ~ 6速のどれかが故障
 jr z, transmission_break_l
 cp 70h
 jr z, transmission_break_l
 or L
 ld HL, car_transmission_st
 set 7, A
 ld (HL), A
 jr brakeend
electron_break:
 ld HL, car_elect_st
 set 7, (HL)
 jr brakeend
brake_break:
 ld HL, car_brake_st
 set 7, (HL)
; ret
brakeend:
 ld HL, g_count_1 + 1
 ld A, 40h
 cp (HL)
 ret nc
 ld (HL), A
 ret

;------------ 障害物判定 ---------------
block_action_x:
 dw overwrite
block_action_y:
 dw overwrite
block_action:
 call check_race_flag
 jp nz, objfetch_loop_end
 ld HL, block_action_do
block_action_com:
 ld (block_action_jp + 1), HL
 ld HL, (car_pos_y)
 bit 7, H ;天井突き破り
 jp nz, block_action_do
 
 ld HL, 0
 ld (check_collision_BC + 1), HL

 call get_obj_pos_rect_address
 ld A, 7	;補正値
 ;call log_A_kwait
 ld DE, car_pos_x
 call check_collision	;現在ポジション - ブロックポジション
 jp c, objfetch_loop_end
 exx
 ld HL, (block_action_posDE + 1)
 ld (block_action_x), HL
 exx

 ld A, 10	;補正値
 ;call log_A_kwait
 ld DE, car_pos_y
 call check_collision
 jp c, objfetch_loop_end
 exx
 ld HL, (block_action_posDE + 1)
 ld (block_action_y), HL

 ld HL, (block_action_x)
 ld (check_collision_BC + 1), HL
 exx
 ld A, 4	;補正値
 ;call log_A_kwait
 ld DE, car_pos_x
 call check_collision
 jp nc, objfetch_loop_end
 
 exx
 ld HL, (block_action_y)
 ld (check_collision_BC + 1), HL
 exx
 ld A, 4	;補正値
 ;call log_A_kwait
 ld DE, car_pos_y
 call check_collision
 jp nc, objfetch_loop_end

block_action_jp:
 jp overwrite

block_action_do:
block_prevx:
 ld HL, overwrite
 ld (car_pos_x), HL
block_prevy:
 ld HL, overwrite
 ld (car_pos_y), HL
 ld A, (car_speed)
 cp 210			;クラッシュ(完全故障)速度
 jr nc, block_crash
 ld B, A
 call rand
 and 01111111b
 ld C, A
 ld A, B  ;0 ~ 255
 rra  ;0 ~ 127
 rra  ;0 ~ 63
 rra  ;0 ~ 31
 and 00011111b
 add A, C ;rand 0 ~ 127 + 0 ~ 63
 cp 128
 jp c, block_action_end
 ld A, B
 cp 40			;ウィング故障速度
 jr c, block_sus_brake
 ld HL, car_wing_st
 set 4, (HL)	;ウィングの故障
 ;jp endpoint
block_sus_brake:
 cp 110
 jr c, block_sus_nobrake
 call rand
 cp 230
 jr c, block_sus_nobrake
 ld HL, car_suspension_st
 set 4, (HL)	;サスペンションの故障9割で回避
block_sus_nobrake:
 ld HL, car_speed
 ld A, (HL)
 sub 16
 ld (HL), A
 jp nc, block_action_end
 xor A
 ld (HL), A
block_action_end:
 jp drift_beep

ENDTIMER_COUNT:
 db overwrite
block_crash:
 ld A, 32
 ld (car_crash_fl), A
 ld A, 90
 ld (ENDTIMER_COUNT), A
 xor A
 ld (IX + speed), A
 jp objfetch_loop_end

pit_action:
 call check_race_flag
 jp nz, objfetch_loop_end

 ld HL, pit_action_do
 jp block_action_com
pit_action_do:
 xor A
 ld (car_speed), A
 ld (car_dir), A

 ld A, 1
 ld (car_pitin), A
 jp objfetch_loop_end
 

;周回処理 自車の処理 敵車の処理はポイントの最後に接触したらOK
lap_action_collided:	;0:none 1:upper 2:bottom 5:upper2center 6:bottom2center
 db overwrite
lap_action:
 call check_race_flag
 jp nz, objfetch_loop_end
 ld HL, lap_action_do
 jp block_action_com
lap_action_do:
 ld HL, lap_action_collided
 ld A, (HL)
 cp 5
 jp nc, objfetch_loop_end
 add A, 4
 ld (HL), A
 jp objfetch_loop_end
 

lap_action_bottom:
 call check_race_flag
 jp nz, objfetch_loop_end
 ld HL, lap_action_bottom_do
 jp block_action_com
lap_action_bottom_do:
 ld HL, lap_action_collided
 ld A, (HL)
 or A
 jr z, lap_action_bottom_stor
 cp 2
 jp z, objfetch_loop_end
 cp 5
 push HL
 call z, lap_reverse
 pop HL
lap_action_bottom_stor:
 ld A, 2
 ld (HL), A
 jp objfetch_loop_end


lap_action_upper:
 call check_race_flag
 jp nz, objfetch_loop_end
 ld HL, lap_action_upper_do
 jp block_action_com
lap_action_upper_do:
 ld HL, lap_action_collided
 ld A, (HL)
 or A
 jr z, lap_action_upper_stor
 cp 1
 jp z, objfetch_loop_end
 cp 6
 push HL
 call z, lap_exec
 pop HL
lap_action_upper_stor:
 ld A, 1
 ld (HL), A
 jp objfetch_loop_end


check_race_flag:
 ld A, (car_pitin)
 or A
 ret nz
 ld A, (lap_end)
 or A
 ret nz
 ret

lap_exec:
 ld HL, lap_remind
 ld A, (HL)
 or A
 jr z, lap_exec1  ;ラップ数0なら無限に走れる
 dec A
 ld (HL), A
 jp z, race_end     ;A=周回数 0ならレース終了
lap_exec1:
 ld HL, lap_reverse_lap
 ld A, (HL)
 or A
 jr z, lap_exec2
 dec A
 ld (HL), A
 ret
lap_exec2:
;ラップ、ピット状態を表示
 xor A
 ld HL, lap_remind
 call regist_position
 call lap_info
 ret

lap_reverse_lap:	;何周逆走したか
 db overwrite
lap_reverse:
 ld HL, lap_remind
 ld A, (HL)
 ld B, A
 or A
 jr z, lap_reverse_lap2  ;ラップ数0なら逆走し放題
 inc A
 ld (HL), A
 or B
 jp z, race_end     ;A=周回数 B=自車名 ともに0(-256)ならレース終了
lap_reverse_lap2:
 ld HL, lap_reverse_lap
 inc (HL)
 ret

tonnel_start_action:
 ld HL, tonnel_start_action_do
 jp block_action_com
tonnel_start_action_do:
 ld A, 0a7h
 out (40h), A
 jp objfetch_loop_end

tonnel_end_action:
 ld HL, tonnel_end_action_do
 jp block_action_com
tonnel_end_action_do:
 ld A, 0a6h
 out (40h), A
 jp objfetch_loop_end

cross_be: ;高架交差点に居るか 0 or (2 or 3) Spriteの描画を飛ばす
 db overwrite
cross_pos:  ;高架の上か下か 0 or 1
 db overwrite
cross_bottom:
 ld HL, cross_bottom_do
 jp block_action_com
cross_bottom_do:
 ld HL, cross_be
 ld A, (HL)
 cp 6
 jp z, block_action_do
 xor A
 ld (HL), A
 ld A, 1
 ld (cross_pos), A
 jp objfetch_loop_end

cross_upper:
 ld HL, cross_upper_do
 jp block_action_com
cross_upper_do:
 ld HL, cross_be
 ld A, (HL)
 cp 5
 jp z, block_action_do
 xor A
 ld (HL), A
 ld A, 2
 ld (cross_pos), A
 jp objfetch_loop_end

cross_action:
 ld HL, cross_action_do
 jp block_action_com
cross_action_do:
 ld HL, cross_be
 ld A, (HL)
 or A
 jp nz, objfetch_loop_end
 ld A, (cross_pos)
 or 4
 ld (HL), A
 jp objfetch_loop_end


car_action:;A=(objno)
car_action_cache_value:
;16ビットアドレスを計算して参照用とする
;カレントメモリ
;2バイト
 add A, A
 push AF
 ld C, A
 ld B, 0
 ld DE, g_count_1
 ld HL, g_count_crnt
 call car_action_cache_value_s

 ld DE, car_pos_x
 ld HL, car_pos_x_crnt
 call car_action_cache_value_s

 ld DE, car_pos_y
 ld HL, car_pos_y_crnt
 call car_action_cache_value_s

 ld DE, CARMOVE_MOVEPOS_VEC
 ld HL, CARMOVE_MOVEPOS_VEC_crnt
 call car_action_cache_value_s

 ld DE, CARMOVE_ADD_DIRECTION_VALUE_CACHE
 ld HL, CARMOVE_ADD_DIRECTION_VC_crnt
 call car_action_cache_value_s

 ld DE, CARMOVE_HUNDLE_V
 ld HL, CARMOVE_HUNDLE_V_crnt
 call car_action_cache_value_s

 ld DE, CARMOVE_AUTO_PITIN_WAIT_TIMERS
 ld HL, CARMOVE_AUTO_PITIN_W_T_crnt
 call car_action_cache_value_s
 
 ;4バイト
 pop AF
 add A, A
 ld C, A
 ld B, 0
 ld DE, car_pit_pos
 ld HL, car_pit_pos_crnt
 call car_action_cache_value_s

 ld DE, CARMOVE_COMPUTER_NEXT
 ld HL, CARMOVE_COMPUTER_NEXT_crnt
 call car_action_cache_value_s

 ;push AF
 call CARMOVE	;ここの中は敵の車も動かすので、IXレジスタをbaseのアドレスにして対応
 ;ld A, 1
 ;call log_A_kwait
 call BGMOVE
 ;ld A, 2
 ;call log_A_kwait
 call BGMOVE_CORRECTION
 ;ld A, 3
 ;call log_A_kwait
 call SCROLL
 ;ld A, 4
 ;call log_A_kwait
 call SET_BG
 ;ld A, 5
 ;call log_A_kwait
 ld A, 6
 ;call log_A_kwait
 call SET_SPRITE
 ;call cource_info
 ;pop AF
 ;call log_A
 ;inc A
 inc IX
 jp objfetch_loop_end

endpoint:
 ld HL, (retstack)
 ld SP, HL
 ret
retstack:
 dw 0

car_action_cache_value_s:;BC=(objno)*2 DE=展開元 例:g_count_1 HL=展開先 例:g_count_crnt
 ld (car_action_cache_value_s_HL1 + 1), HL	;アドレス
 inc HL
 inc HL
 ld (car_action_cache_value_s_HL2 + 1), HL	;値
 ex DE, HL
 
 add HL, BC	;展開元
car_action_cache_value_s_HL1:
 ld (overwrite), HL	;展開先1 アドレス
 ld E, (HL)
 inc HL
 ld D, (HL)
 ex DE, HL
 cp 1
 jr z, car_action_cache_value_s_HL3
car_action_cache_value_s_HL2:
 ld (overwrite), HL	;展開先2 値
 ret
car_action_cache_value_s_HL3:
 ld HL, (car_action_cache_value_s_HL2 + 1)
 dec HL
 ld (HL), E
 ret
 
 
;------- コース情報展開 ---------
BGUNZIP:
 ;コースグラフィックアドレスを求める
 ld A, (cource_current)
 ld C, A
 ld HL, BGUNZIP_COURCE_TABLE
 ld DE, 2
 call XSAN
 ld E, (HL)
 inc HL
 ld D, (HL)
 ex DE, HL
 ld (BGMOVE_BGLDIS_2_HL + 1), HL	;以下背景スクロール用コースグラフィックテーブル先頭アドレス
 ld (BGMOVE_BGLDIS_3_HL + 1), HL
 ld (BGMOVE_BGLDIS_4_HL + 1), HL
 ld (BGMOVE_BGLDIS_5_HL + 1), HL
 
 ld A, C
 
 ld HL, BGUNZIP_SIZE_TABLE
 ld DE, 2
 call XSAN
 ;call log_HL_kwait
 
 ld E, (HL)
 inc HL
 ld D, (HL)
 ex DE, HL
 
 ld A, (HL)					;コースサイズ
 ;call log_A_kwait
 ld (cource_size), A		;y
 ld E, A
 inc HL
 ld A, (HL)
 ;call log_A_kwait
 ld (cource_size + 1), A	;x
 
 ld D, 0
 ld HL, 0
 call XSAN
 ld A, L					;この条件だとx*Y=255サイズまでのマップが作れる
 ld (bg_sizetotal), A
 
 ld A, C
 ld HL, BGUNZIP_ARRANGEMENT_TABLE
 ld DE, 2
 call XSAN
 ;call log_HL_kwait
 
 ld E, (HL)
 inc HL
 ld D, (HL)
 ex DE, HL
 
 ld (cource_arrangement_address), HL;コースの並び先頭アドレス
 ;初描画は自機のメインループで
 ret

;BGアドレス取得 in (A=グラフィックNo) (HL=BGUNZIP_COURCE_N_TABLE) XMAX=(cource_maxsize + 1) YMAX=(cource_maxsize) xmax=(cource_size + 1) ymax=(cource_size)  ret HL=
BGUNZIP_GET_BG:
 or A
 ret z
 push AF
 
 exx						;裏レジスタへ
 ld A, (bg_sizetotal)
 ld D, A
 
 pop AF
 push AF
 ex AF, AF'					;A'レジスタへ
BGUNZIP_GET_BG_X:
 ld A, D					;A' = courcesize x * y
 exx						;表レジスタへ
BGUNZIP_GET_BG_X_1:
 dec A
 jr z, BGUNZIP_GET_BG_RET	;courcesize x * y = 0
 
 inc HL
 inc HL
 
 ex AF, AF'					;A表レジスタ(入力値)へ
 dec A
 jr z, BGUNZIP_GET_BG_RET	;入力値 = 0
 ex AF, AF'					;A'レジスタへ
 
 jr BGUNZIP_GET_BG_X_1
 
BGUNZIP_GET_BG_RET:
 pop AF
 ret

;何画面目か指定して、展開先FRAMを指定
BGUNZIP_GET_FRAM:	;A=指定bg_index ret DE=FRAM転送先アドレステーブルを取得
 push HL
 push AF

 ld C, A                    ;C=指定bg_index C >= A
 ld A, (cource_size + 1)
 ld B, A
 ld A, C                    ;A=指定bg_index
 cp B
 jr c, BGUNZIP_GET_FRAM_2   ;xmaxB > select_bg_indexA
 sub B
 add A, 2                   ;1行飛ばしにさせるため

BGUNZIP_GET_FRAM_2:
 ld HL, BGUNZIP_FRAM_TABLE
 ld C, A                    ;C=指定bg_index C >= A
 ld A, (current_bg_index)   ;A=current_bg_index
 ld B, A
 ld A, C
 sub B
 ld DE, 2
 call XSAN                  ;HL = FRAM0~FRAM3を示すアドレス

 ex DE, HL
 pop AF
 pop HL
 ret

;コースグラフィック
BGUNZIP_COURCE_TABLE:
 dw BGUNZIP_COURCE_0_TABLE
 dw BGUNZIP_COURCE_1_TABLE
 dw BGUNZIP_COURCE_2_TABLE
 ;dw BGUNZIP_COURCE_3_TABLE

;コースグラフィック コース毎
;スピードウェイ
BGUNZIP_COURCE_0_TABLE:
 dw hiway_0, hiway_1, hiway_7
 dw hiway_8, hiway_9, hiway_15
 dw cource0_0, courcebl
 dw pit_upper, hiway_25, hiway_26, hiway_27, hiway_28
 dw pit_center, hiway_33, hiway_34, hiway_35, hiway_36, hiway_37
 dw pit_bottom, hiway_41, hiway_44, hiway_45, mini_1, cource0_4
 dw hiway_49, hiway_50
 dw hiway_56, hiway_57, hiway_58
 dw hiway_64
;モント シティ
BGUNZIP_COURCE_1_TABLE:
 dw mini_1, tech_1, tech_3
 dw courcebl, tech_7
 dw cource0_1, tech_13
 dw cource0_0
 dw pit_upper, tech_31, tech_34, hiway_7
 dw pit_center, tech_37, tech_40, hiway_15
 dw pit_bottom
 dw tech_48, tech_49, tech_51
 dw tech_54, tech_55, tech_57, cource0_4
;ヘアピンパーク
BGUNZIP_COURCE_2_TABLE:
 dw cource0_0, cource0_1, cource0_2, mini_1, cource0_4, cource0_5, courcebl
 dw pit_upper, pit_center, pit_bottom
;テクバ
;BGUNZIP_COURCE_3_TABLE:
 ;dw cource0_0, cource0_1, cource0_2, cource0_0, cource0_4, cource0_5
;ラップ数
BGUNZIP_LAP_TABLE:
 dw cource0_lap, cource1_lap, cource2_lap;, cource3_lap

;コースサイズ
BGUNZIP_SIZE_TABLE:
 dw cource0_size, cource1_size, cource2_size;, cource3_size

;コース初期位置
BGUNZIP_CARPOS_TABLE:
 dw cource0_carpos, cource1_carpos, cource2_carpos;, cource3_carpos

;ピットイン位置
BGUNZIP_PITPOS_TABLE:
 dw cource0_pitpos, cource1_pitpos, cource2_pitpos;, cource3_pitpos

;コースの並び
BGUNZIP_ARRANGEMENT_TABLE:
 dw cource0_arrangement, cource1_arrangement, cource2_arrangement;, cource3_arrangement

BGUNZIP_OBJLIST_TABLE:
 dw cource0_objlist, cource1_objlist, cource2_objlist;, cource2_objlist
;RAM
BGUNZIP_FRAM_TABLE
 dw fram_0, fram_1, fram_2, fram_3

;------- コース設定の初期化 ---------
COURCE_SETUP:;A = コースNo
 ;オブジェクト情報を展開
 xor A
 ld HL, objlist
 ld (HL), A
 ld DE, objlist + 1
 ld BC, 47
 ldir
 ld A, 80h
 ld (objlist + 1), A

 ld HL, objlist_pos_rect
 ld (BGUNZIP_OBJSTR_DE1 + 1), HL
 ld HL, objlist
 ld (BGUNZIP_OBJSTR_LP0 + 1), HL

 ld A, (cource_current)
 add A, A
 ;ld E, A
 ;ld D, 0
 ld HL, BGUNZIP_OBJLIST_TABLE
 ;add HL, DE
 call HL_ADD_A
 ld E, (HL)
 inc HL
 ld D, (HL)
 ex DE, HL	;HL = courcex_objlist
BGUNZIP_OBJSTR_LP0:
 ld DE, overwrite
 ld A, (HL)
 inc HL
 cp 80h
 jp z, BGUNZIP_OBJSTR_END
 ld C, A	;C = 種類
 ld A, (HL)
 inc HL
 ld B, A	;B = 回数
 push BC
 ld A, C
BGUNZIP_OBJSTR_LP1:
 ld (DE), A	;種類
 inc DE
 djnz BGUNZIP_OBJSTR_LP1
 ex DE, HL
 ld (BGUNZIP_OBJSTR_LP0 + 1), HL
 ex DE, HL
 pop BC
 ld A, B
BGUNZIP_OBJSTR_DE1:
 ld DE, overwrite		;DE = objlist_pos_rect
BGUNZIP_OBJSTR_LP2:
 ;call log_DE_kwait
 ldi
 ldi
 ldi
 ldi
 ldi
 ldi
 ldi
 ldi
 dec A
 jr nz, BGUNZIP_OBJSTR_LP2
 ex DE, HL
 ld (BGUNZIP_OBJSTR_DE1 + 1), HL
 ex DE, HL
 jp BGUNZIP_OBJSTR_LP0
BGUNZIP_OBJSTR_END:
 ld (DE), A	;80h

;ポイントを置く
 ld A, (HL)	;point counts
 ld (objlist_point_num), A
 ld B, A
 add A, A	;A = Ao * 2
 add A, A	;A = Ao * 4
 add A, B	;A = Ao * 4 + Ao
 ld B, A	;B = Ao * 5
 inc HL
 ld DE, objlist_points
BGUNZIP_PNTSTR_LP:
 ld A, (HL)
 ld (DE), A
 inc HL
 inc DE
 djnz BGUNZIP_PNTSTR_LP
 ld A, 0ffh	;endsign
 ld (DE), A

BGUNZIP_PNTSTR_LAPE:
;初期座標の設定
 ld A, (cource_current)
 add A, A
 ld HL, BGUNZIP_CARPOS_TABLE	;y
 ;ld E, A
 ;ld D, 0
 ;add HL, DE
 call HL_ADD_A
 ld E, (HL)
 inc HL
 ld D, (HL)
 ex DE, HL  ;アドレス
 
 ld E, (HL)
 inc HL
 ld D, (HL)
 inc HL
 ex DE, HL
 ld (car_pos_y), HL
 push DE
 ld DE, 18
 add HL, DE
 ld (car_pos_y + 2), HL
 add HL, DE
 ld (car_pos_y + 4), HL
 add HL, DE
 ld (car_pos_y + 6), HL
 
 pop DE
 ex DE, HL
 ld E, (HL)
 inc HL
 ld D, (HL)
 inc HL
 ex DE, HL
 ld (car_pos_x), HL
 ld (car_pos_x + 4), HL
 ld DE, 30
 add HL, DE
 ld (car_pos_x + 2), HL
 ld (car_pos_x + 6), HL

;ピット位置の設定
 ld A, (cource_current)
 add A, A
 ld HL, BGUNZIP_PITPOS_TABLE
 ;ld E, A
 ;ld D, 0
 ;add HL, DE
 call HL_ADD_A
 ld E, (HL)
 inc HL
 ld D, (HL)
 ex DE, HL  ;先頭アドレス
 
 ld E, (HL)
 inc HL
 ld D, (HL)
 inc HL
 ex DE, HL
 ld (car_pit_pos + 2), HL ;y
 push DE
 ld DE, 18
 add HL, DE
 ld (car_pit_pos + 6), HL ;y
 add HL, DE
 ld (car_pit_pos + 10), HL ;y
 add HL, DE
 ld (car_pit_pos + 14), HL ;y
 
 pop DE
 ex DE, HL
 ld E, (HL)
 inc HL
 ld D, (HL)
 inc HL
 ex DE, HL
 ld (car_pit_pos + 0), HL
 ld (car_pit_pos + 4), HL
 ld (car_pit_pos + 8), HL
 ld (car_pit_pos + 12), HL
 
;目標速度の設定
 ld A, 255
 ld HL, CARMOVE_COMPUTER_SPD + 1
 ld (HL), A
 inc HL
 ld (HL), A
 inc HL
 ld (HL), A

;目標地点の設定
 ld HL, (objlist_points)
 ld (CARMOVE_COMPUTER_NEXT + 4), HL
 ld (CARMOVE_COMPUTER_NEXT + 8), HL
 ld (CARMOVE_COMPUTER_NEXT + 12), HL
 ld HL, (objlist_points + 2)
 ld (CARMOVE_COMPUTER_NEXT + 6), HL
 ld (CARMOVE_COMPUTER_NEXT + 10), HL
 ld (CARMOVE_COMPUTER_NEXT + 14), HL

 ld A, (cource_size + 1)	;x
 dec A						;0以下にならない
 ld HL, 0
 ld DE, xdots
 call XSAN					;HL = x限界値
 ld (BGMOVE_X_MAX + 1), HL
 
 ld A, (cource_size)		;y
 dec A
 ld HL, 0
 ld DE, ydots
 call XSAN					;HL = y限界値
 ld (BGMOVE_Y_MAX + 1), HL
 ;call log_HL_kwait
 
 ret

;ラップ数の設定
LAP_SETUP:
 ld A, (game_mode)
 dec A
 ld B, A
 ld HL, lap_max
 ld (HL), A ;lap_max フリー走行用にリセット 0
 inc HL
 ld (HL), A ;lap_remind フリー走行用にリセット 0
 or A
 ret z
 ld A, (cource_current)
 add A, A
 ld HL, BGUNZIP_LAP_TABLE
 call HL_ADD_A
 ld E, (HL)
 inc HL
 ld D, (HL)
 ex DE, HL
 ld A, B
 cp 2
 jr c, BGUNZIP_PNTSTR_LAP2
 inc HL
BGUNZIP_PNTSTR_LAP2:
 ld A, (HL) ;スプリント タイキュウの周回数
 ld HL, lap_max
 ld (HL), A ;lap_max
 inc HL
 ld (HL), A ;lap_remind
 inc HL
 ld (HL), A
 inc HL
 ld (HL), A
 inc HL
 ld (HL), A
 ret

;フリー走行で敵車を消す
remove_enemys:
 ld HL, objlist
 ld A, 0fh
 inc HL
 ld (HL), A
 inc HL
 ld (HL), A
 inc HL
 ld (HL), A
 ret

;------- コース背景描画 ---------
SET_BG:			;(A = 現在の左上描画画面NO HL = 開始アドレス B = 描画範囲x D = 描画開始行 E = 描画開始x)
 ;ld A, (g_count)
 ;and 1
 ;ret z
 ;;SET_BG_DRAW_0で描画するための準備
 ;右上
 ld A, (objno)
 or A
 ret nz
 ld A, (current_bg_index)
 inc A
 ld (SET_BG_NOW), A
 ld HL, fram_1
 call SET_BG_0
 ld HL, (SET_BG_STARTPOS)
 ld (SET_BG_AFT_SETUP_1 + 1), HL
 ld A, (SET_BG_0_7_3 + 1)
 ld (SET_BG_AFT_SETUP + 1), A
 
 ;右下
 ld A, (current_bg_index)
 ld B, A
 ld A, (cource_size + 1)
 add A, B
 inc A
 ld (SET_BG_NOW), A
 ld HL, fram_3
 call SET_BG_0
 
 ;左上
 ld A, (current_bg_index)
 ld (SET_BG_NOW), A
 ld HL, fram_0
 call SET_BG_0
 
 ;左下
 ld A, (current_bg_index)
 ld B, A
 ld A, (cource_size + 1)
 add A, B
 ld (SET_BG_NOW), A
 ld HL, fram_2
 call SET_BG_0
 
 ;横隙間埋め１
SET_BG_AFT_SETUP:
 ld A, overwrite
 or A
 jr z, SET_BG_LOOP_E
SET_BG_AFT_SETUP_1:
 ld HL, overwrite
 dec H
 ld A, H
 add A, A
 ld E, A
 add A, A
 add A, E
 ld E, A		;x
 ld D, 0
 ld A, 7
 ld HL, SET_BG_SEAM
SET_BG_LOOP:
 ex AF, AF'
 ld B, 6
 push DE
 push HL
 ;call log_DE_kwait
 call write_lcd_exec
 pop HL
 ld DE, 6
 add HL, DE
 pop DE
 inc D
 ex AF, AF'
 dec A
 jr nz, SET_BG_LOOP
SET_BG_LOOP_E:
 call SET_PIT_SIGN
 call CARMOVE_TACHOMETER
 ret
 
 
SET_BG_0:
 ;HL=fram_X 描画BGのアドレス
 push HL					;BGが書かれたFRAM
 call SET_BG_CHECK_BGPOS
 jr nz, SET_BG_0_5	;割り切れないのは同列ではないから
 
 ;同列同行 0 (同列 違う行  1)
 or A
 jr z, SET_BG_0_4_1
 ld A, 1
SET_BG_0_4_1:
 ld (SET_BG_0_6_0 + 1), A
 ld A, (scroll_pos_x)
 ld B, A
 ld A, xdots
 sub B
 ld (SET_BG_DRAWDOTS), A	;現在の左上のカレントBG、または同列BGならxdots - (scroll_pos_x)値分描画 
 ld C, B
 ld B, 0
 jr SET_BG_0_6
 
SET_BG_0_5:
 or A
 jr z, SET_BG_0_5_1		;同行ならジャンプ
 
 ;違う列違う行
 ld A, 3
 ld (SET_BG_0_6_0 + 1), A
 jr SET_BG_0_5_2
SET_BG_0_5_1:
 ;違う列同じ行
 ld A, 2
 ld (SET_BG_0_6_0 + 1), A
SET_BG_0_5_2:
 ld A, (scroll_pos_x)
 ld (SET_BG_DRAWDOTS), A	;現在の左上のカレントBGと同列以外なら(scroll_pos_x)の値分描画
 ld BC, 0
 
SET_BG_0_6:
 ;call log_A_kwait
 ld E, A		;E = (SET_BG_DRAWDOTS)
 ld L, A
 ld H, 0
 ld A, 6
 call WSAN
 ld A, E		;A = (SET_BG_DRAWDOTS)
 sub L
 ld D, L		;D = (SET_BG_DRAWDOTS) % 6
 ld E, A		;E = (SET_BG_DRAWDOTS) - (SET_BG_DRAWDOTS) % 6

 pop HL						;BGが書かれたFRAM
 add HL, BC					;スクロール位置を足す
 ld (SET_BG_FRAM), HL		;描画開始アドレス
 
 push DE
 push HL
 call SET_BG_0_8			;描画
 pop HL
 pop DE
 
 ;----------- 横隙間をメモリに転送 -----------
 
 ld A, D		;転送量 A = (SET_BG_DRAWDOTS) % 6
 or A

 ld (SET_BG_0_7_3 + 1), A	;C設定値 (SET_BG_DRAWDOTS) % 6
 ld C, A
 
 ret z
 
 ;call log_A_kwait
 ;push BC
 ;push AF
 ;call TIM255
 ;pop AF
 ;pop BC
 ;call log_A_kwait
 ld A, 6
 sub D
SET_BG_0_6_SET_SECTION:
 ;call log_A_kwait
 ld (SET_BG_0_7_2 + 1), A	;DE加算量 (SET_BG_DRAWDOTS) % 6 同列同行では加算値が6 - Aでないといけない
 
 ld A, (SET_BG_DRAWLINES)	;描画行数
 cp 6
 jr z, SET_BG_0_6_SET_SECTION_A
 inc A
SET_BG_0_6_SET_SECTION_A:
 push AF
 
 xor A
 ld (SET_BG_0_7_1 + 2), A	;HLの加算量 H=0
 ld A, xdots
 sub D
 ld (SET_BG_0_7_1 + 1), A	;HLの加算量 xdots - (SET_BG_DRAWDOTS) % 6

SET_BG_0_6_0:
 ld A, overwrite		;(同列同行の場合0) (同列違う行 1) (同列以外 同行2) (同列以外 同業以外3)
 ;call log_A_kwait
 cp 2
 jr c, SET_BG_0_6_1		;左(同列同行 同列違う行) の場合ジャンプ
 
 cp 3
 push AF
 						;上(同列以外 同行)
 
 ld B, D				;B = (SET_BG_DRAWDOTS) % 6
 ld DE, SET_BG_SEAM + 6
 ld A, E
 sub B
 ld E, A
 jr nc, SET_BG_0_6_0_1	;DE = SET_BG_SEAM + 6 - (SET_BG_DRAWDOTS) % 6
 dec D
SET_BG_0_6_0_1:
 ;call log_DE_kwait
 pop AF
 jr z, SET_BG_0_6_1_1	;右下(同列以外 同行以外) の場合ジャンプ
 jr SET_BG_0_6_1_2


SET_BG_0_6_1:	;同列同行 同列違う行 の場合
 push AF
 ld A, D		;カウント A = (SET_BG_DRAWDOTS) % 6
 ;call log_A_kwait
 ld D, 0		;E = (SET_BG_DRAWDOTS) - (SET_BG_DRAWDOTS) % 6
 add HL, DE		;HL = (SET_BG_FRAM) + { (SET_BG_DRAWDOTS) - (SET_BG_DRAWDOTS) % 6 } 同列BGではブロックの始まりから
 ld DE, SET_BG_SEAM

 pop AF
 or A
 jr z, SET_BG_0_6_1_2
 ;jr nz, SET_BG_0_6_1_1_F;違う行

SET_BG_0_6_1_1_F:;違う列違う行
 ld DE, SET_BG_SEAM
SET_BG_0_6_1_1:	 ;同列違う行 の場合 ;DE = SET_BG_SEAMの位置調整
 push HL
 push DE
 ld DE, xdots
 ld A, (scroll_block)
 call XSAN
 
 ld A, (scroll_block)
 ld E, A
 ld A, 6
 sub E
 pop HL
 ld DE, 6
 call XSAN
 ex DE, HL
 pop HL
 jr SET_BG_0_6_9

SET_BG_0_6_1_2:
 push DE
 ld A, (SET_BG_DRAWLINES)
 ld E, A
 ld A, 6
 sub E
 ld DE, xdots
 call XSAN
 pop DE
 jr SET_BG_0_6_9

SET_BG_0_6_9:
 ;call log_DE_kwait
 ;call log_HL_kwait
 pop AF			;カウンタ 描画行数
 ;call log_A_kwait
SET_BG_0_7:
 push AF		;カウンタ 描画行数
 ;push BC
 ;push AF
 ;call TIM255
 ;pop AF
 ;pop BC
 ;call log_A_kwait
SET_BG_0_7_0:
 ld A, (HL)		;(SET_BG_FRAM + a)
 ld (DE), A		;(SET_BG_SEAM + a)
 ;call log_0_kwait
 ;call log_HL_kwait
 ;call log_DE_kwait
 inc HL
 inc DE
 dec C
 ld A, C
 ;call log_A_kwait
 jr nz, SET_BG_0_7_0
 
SET_BG_0_7_1:
 ld BC, overwrite
 add HL, BC			;一行飛ばす xdots - (SET_BG_DRAWDOTS) % 6
SET_BG_0_7_2:
 ld A, overwrite	;1 ~ 5増やす
 add A, E
 ld E, A
 jr nc, SET_BG_0_7_3
 inc D
SET_BG_0_7_3:
 ld C, overwrite	;1 ~ 5カウンタ修正
 pop AF
 
 ;call log_0_kwait
 ;call log_HL_kwait
 ;call log_DE_kwait
 
 dec A
 ;call log_A_kwait
 jr nz, SET_BG_0_7
 ret
 
SET_BG_0_8:
 xor A
 ld (SET_BG_0_COLUMN_LINEOTHER_H + 1), A
 ld (SET_BG_0_COLUMN_LINESAME_HL + 1), A
 ld (SET_BG_0_COLUMN_LINESAME_HL + 3), A
 call SET_BG_CHECK_BGPOS	;現在の左上BGからの位置 Z...同列 A...0で同行
 call z, SET_BG_0_COLUMN	;同列での描画開始位置を求める
 call nz, SET_BG_0_OTHER	;違う列での描画開始位置を求める
 
 call SET_BG_DRAW_0
 ;call log_0_kwait
 ;call log_SP_kwait
 ret
 
SET_BG_0_COLUMN:	;同列での描画行数、描画開始位置を求める A...ずれてる行数
 or A
 ld A, (scroll_block)		;y描画開始行(BGデータから)
 jr nz, SET_BG_0_COLUMN_LINEOTHER	;行違いのとき場合ジャンプ
 
SET_BG_0_COLUMN_LINESAME:	;同列同行
 ld E, A
 ld D, 0
 
 ld A, 11
 ;call log_A_kwait
 
 ld A, 6
 sub E			;A = 6 - (scroll_block) ... 何行描画するか
 ld (SET_BG_DRAWLINES), A
SET_BG_0_COLUMN_LINESAME_HL:
 ld H, overwrite			;描画開始列
 ld L, 0					;描画開始行
 ld (SET_BG_STARTPOS), HL
 xor A		;Z
 ret

SET_BG_0_COLUMN_LINEOTHER:	;同列違う行 描画開始行を求める
 ld (SET_BG_DRAWLINES), A
 ld E, A
 ld D, 0
 
 ld A, 10
 ;call log_A_kwait
 
 ld A, 6
 sub E
 ld L, A
SET_BG_0_COLUMN_LINEOTHER_H
 ld H, overwrite			;描画開始列
 ld (SET_BG_STARTPOS), HL
 xor A		;Z
 ret

SET_BG_0_OTHER:	;違う列での描画行数、描画開始位置を求める A...ずれてる行数
 push AF
 ld A, (SET_BG_DRAWDOTS)
 ld HL, 24
SET_BG_0_OTHER_1:
 cp 6
 jr c, SET_BG_0_OTHER_2
 dec HL
 sub 6
 jr nz, SET_BG_0_OTHER_1
SET_BG_0_OTHER_2:
 exx
 ld HL, (SET_BG_FRAM)
 ;ld D, 0
 ;ld E, A
 ;add HL, DE
 call HL_ADD_A
 ld (SET_BG_FRAM), HL
 exx
 pop AF
 or A
 jr z, SET_BG_0_OTHER_LINESAME
 
SET_BG_0_OTHER_LINEOTHER	;違う列違う行 L = 描画開始列 A= (SET_BG_DRAWDOTS) / 6
 ld A, L		;描画開始列
 ld (SET_BG_0_COLUMN_LINEOTHER_H + 1), A
 
 ;ld A, 99
 ;call log_A_kwait
 
 ld A, (scroll_block)		;y描画開始行(BGデータから)
 call SET_BG_0_COLUMN_LINEOTHER
 ret
 
SET_BG_0_OTHER_LINESAME:	;違う列同じ行
 ld A, 91
 ;call log_A_kwait
 
 ld A, L
 ld (SET_BG_0_COLUMN_LINESAME_HL + 1), A	;x
 xor A						;描画開始行 = 0
 ld (SET_BG_0_COLUMN_LINESAME_HL + 3), A	;y
 ld A, (scroll_block)
 call SET_BG_0_COLUMN_LINESAME
 ret
 
 
SET_BG_DRAW_0:
 ld HL, (SET_BG_FRAM)
 ld A, (SET_BG_DRAWDOTS)
 ld B, A
 or A
 ret z
 
SET_BG_DRAW_0_LINES:
 exx
 call SET_BG_CHECK_BGPOS
 exx
 or A
 ld A, (SET_BG_DRAWLINES)
 jr z, SET_BG_DRAW_0_1	;同じ行
 inc A
 push AF
 xor A
 jr SET_BG_DRAW_0_2		;違う行
SET_BG_DRAW_0_1:
 push AF				;カウント用
 ld E, A
 ld A, 6
 sub E			;6 - (SET_BG_DRAWLINES)
SET_BG_DRAW_0_2:
 ld E, A
 ld D, 0
 ld A, xdots
 call XSAN		;HL = { (SET_BG_FRAM) + (scroll_pos_x) } + xdots * { {6 - (SET_BG_DRAWLINES)} or 0}
 ;call log_HL_kwait
 
 ld A, (SET_BG_STARTPOS)		;y
 ld D, A
 ld A, (SET_BG_STARTPOS + 1)	;x
 add A, A
 ld E, A
 add A, A
 add A, E ;*6
 ld E, A
 ;call log_DE
 xor A
 ld (write_lcd_loop + 1), A
 pop AF

SET_BG_DRAW_1:
 push AF
 push DE
 push HL
 push BC
 call write_lcd_exec
 pop BC
 pop HL
 ld DE, xdots
 add HL, DE
 pop DE
 inc D
 pop AF
 dec A
 jr nz, SET_BG_DRAW_1

 ret


SET_BG_CHECK_BGPOS:		;カレントBGと選択しているBGがどの位置にあるか返す 返り値 A...(現在選択 - (current_bg_index))/(cource_size + 1) Z...同列  {現在選択 >= (current_bg_index)
 ld A, (SET_BG_NOW)
 ld L, A
 ld H, 0
 ld A, (cource_size + 1)
 call WSAN
 ld (SET_BG_CHECK_BGPOS_1 + 1), A	;現在選択 / (cource_size + 1)
 ld A, L
 push AF
 
 ld A, (current_bg_index)
 ld L, A
 ld H, 0
 ld A, (cource_size + 1)
 call WSAN
 ld H, A							;(current_bg_index) / (cource_size + 1)
SET_BG_CHECK_BGPOS_1:
 ld A, overwrite					;現在選択 / (cource_size + 1)
 sub H								;(現在選択 - (current_bg_index)) / (cource_size + 1)
 ld (SET_BG_CHECK_BGPOS_END + 1), A
 pop AF
 cp L								;Z...同列 Cy...それ以外
SET_BG_CHECK_BGPOS_END:
 ld A, overwrite					;(現在選択 - (current_bg_index)) / (cource_size + 1) Aが0で同行
 ;ld HL, 9999
 ;call log_HL_kwait
 ;call log_A_kwait
 ret
 
 pop HL
 pop AF
 ret
 
SET_BG_NOW:			;現在選択されているBG
 db overwrite
SET_BG_FRAM:		;描画開始FRAM(+分も含む)
 dw overwrite
SET_BG_DRAWDOTS:	;描画横ドット数
 db overwrite
SET_BG_DRAWLINES:	;描画行数
 db overwrite
SET_BG_STARTPOS:	;描画開始行y 列x
 db overwrite, overwrite
SET_BG_SEAM:		;つなぎ目メモリ (6 + 1)行 * 横6ドット分
 ds 42

;----------- レース開始カウント ------------
CARMOVE_RACE_START:
 dec (HL)
 ld L, A
 in A,(1Fh)
 rla		
 jp c, endpoint	;ゲーム終了
 ld H, 0
 ld A, fps
 call WSAN
 call get_num_chara_drct
 ld BC, 6
 ld DE, fram_0 + 44h
 ldir
 ret
START_TIM:
 db overwrite
;----------- 移動アルゴリズム --------------
CARMOVE:		;アクセル ブレーキ エンジンブレーキ 左右移動(速度による)
CARMOVE_AUTO_ADD:
 ld A, (game_mode)
 cp 1
 jr z, CARMOVE_1

 ld HL, START_TIM
 ld A, (objno)
 or A
 jr nz, CARMOVE_COMPUTER
 
 ld A, (HL)
 or A
 jr nz, CARMOVE_RACE_START
 ;cp 2
 ;jr c, CARMOVE_COMPUTER
CARMOVE_1:
 in A,(1Fh)
 rla		
 jp c, endpoint	;ゲーム終了
 ld A, (car_speed)
 ;call log_A
 call keysc
 jp CARMOVE_A

;---------- 敵車の操作エミュレート ------------
;重そう
CARMOVE_COMPUTER:
 ld A, (HL) ;START_TIM
 or A
 ret nz

 xor A
 ld (keysc_key), A

 ld A, (IX + speed)
 ld D, A
 ld A, (IX + computer_spd)
 ;難易度別速度調整
 ld C, A
 ld A, (game_mode)
 cp 3
 ld B, 15	;タイキュウ
 jr z, CARMOVE_COMPUTER_SET_SPD_E
 ld A, (game_class)
 cp 1
 ld B, 0	;ワールド
 jr c, CARMOVE_COMPUTER_SET_SPD_E
 ld B, 18	;コクナイ
 jr z, CARMOVE_COMPUTER_SET_SPD_E
 ld B, 40	;ノービス
CARMOVE_COMPUTER_SET_SPD_E:
 ld A, C
 sub B

 sub D	;gotoSpeed - nowSpeed cyでスピード超過
 jr c, CARMOVE_COMPUTER_BSP
CARMOVE_COMPUTER_ACS
 ld HL, keysc_key
 set 3, (HL)	;アクセル
 jr CARMOVE_COMPUTER_BA
CARMOVE_COMPUTER_BSP:;A=-x
 ex AF, AF'
 ld HL, car_coms_corner_level - 1
 ld A, (objno)
 call HL_ADD_A
 ld E, (HL)
 ex AF, AF'
 add A, E
 ;ld C, A
 ;srl B
 ;srl B
 ;ld A, B
 ;add A, C
 jr c, CARMOVE_COMPUTER_ACS ;コーナーレベルが大きいとあまり減速しない
 ld HL, keysc_key
 set 2, (HL)  ;ブレーキ
CARMOVE_COMPUTER_BA:

;向き計算してハンドルを適切な方向へ
;point - car_pos_x
 ld HL, (CARMOVE_COMPUTER_NEXT_crnt)	; x, y, x, y...の並び
 ;add HL, BC	;+ (objno) * 2
 ;add HL, BC	;+ (objno) * 4
 ld E, (HL)
 inc HL
 ld D, (HL)
 ;call log_DE
 push DE

;xの差分
 ld HL, (car_pos_x_crnt_v)				;x, x, x, x, y, y, y, yの並び
 ex DE, HL
 ;add HL, BC	;+ (objno) * 2
 ;ld E, (HL)
 ;inc HL
 ;ld D, (HL)
 ;inc DE	;補正値
 ;inc DE
 ;inc DE
 ;inc DE
 pop HL
 or A
 sbc HL, DE	;目指す座標 - 自機座標 (CARMOVE_COMPUTER_NEXT + (objno) * 2) - (car_pos_x + (objno) * 2)
 ld DE, 1
 call cp_hl_de
 jp c, CARMOVE_AUTO
 ;整数化する
 ld A, 1
 ld (CARMOVE_COMPUTER_QUADRANT + 1), A	;第一象限で使う前提だと0 第4象限で使う前提だと1
 bit 7, H
 jr z, CARMOVE_COMPUTER_X_SCR	;(-)サインなし
 ld A, L
 cpl
 ld L, A
 ld A, H
 cpl
 ld H, A
 inc HL
 ;ld DE, 16
 call cp_hl_de
 jp c, CARMOVE_AUTO
 ld A, 2;第2, 3象限(-側)なので8 * 2以上の向きを加えるのは確定
 ld (CARMOVE_COMPUTER_QUADRANT + 1), A
CARMOVE_COMPUTER_X_SCR:
 ld (CARMOVE_COMPUTER_X + 1), HL

;yの差分
 ld HL, (CARMOVE_COMPUTER_NEXT_crnt) CARMOVE_COMPUTER_NEXT + 2	; x, y, x, y...の並び
 ;add HL, BC	;+ (objno) * 2
 ;add HL, BC	;+ (objno) * 4
 inc HL
 inc HL
 ld E, (HL)
 inc HL
 ld D, (HL)
 push DE

 ld HL, (car_pos_y_crnt_v)				;x, x, x, x, y, y, y, yの並び
 ex DE, HL
 ;add HL, BC	;+ (objno) * 2
 ;ld E, (HL)
 ;inc HL
 ;ld D, (HL)
 ;inc DE	;補正値
 ;inc DE
 ;inc DE
 ;inc DE
 pop HL
 or A
 sbc HL, DE	;目指す座標 - 自機座標 (CARMOVE_COMPUTER_NEXT + 2 + (objno) * 2) - (car_pos_y + (objno) * 2)
 ld DE, 1
 call cp_hl_de
 jp c, CARMOVE_AUTO;整数化する
 bit 7, H
 jr z, CARMOVE_COMPUTER_Y_SCR
 ld A, L
 cpl
 ld L, A
 ld A, H
 cpl
 ld H, A
 inc HL
 ;ld DE, 16
 call cp_hl_de
 jp c, CARMOVE_AUTO
 ;ld A, 2;第3, 4象限なので8 * 2を加えるのは確定
 ld A, (CARMOVE_COMPUTER_QUADRANT + 1)
 xor 1
 ld (CARMOVE_COMPUTER_QUADRANT + 1), A
CARMOVE_COMPUTER_Y_SCR:
 ld (CARMOVE_COMPUTER_Y + 1), HL
;割る数割られる数を、小さい方が割る数、大きいほうが割られる数に入れ替え
;B = 0 になる条件はdirが0~3 8~11 15~18 23~27
 ld HL, (CARMOVE_COMPUTER_X + 1)
 ex DE, HL
 ld HL, (CARMOVE_COMPUTER_Y + 1)
 call cp_hl_de
 ;ld B, 4	;Xのほうが大きいと、dir数は4より大きい
 jr c, CARMOVE_COMPUTER_HLDE	;CARMOVE_COMPUTER_X > CARMOVE_COMPUTER_Y	割る数の方が大きい
 ;ld B, 0
 ld (CARMOVE_COMPUTER_X + 1), HL
 ex DE, HL
 ld (CARMOVE_COMPUTER_Y + 1), HL
CARMOVE_COMPUTER_HLDE:
 push AF	;cyフラグは0ビット ;x > y = 1, y >= x = 0
 pop DE
 ld A, (CARMOVE_COMPUTER_QUADRANT + 1)	;0, 1, 0, 1
 xor E
 ;1でB=4と後半の7-Aが必要
 bit 0, A
 ld B, 0
 jr z, CARMOVE_COMPUTER_X
 ld B, 4
CARMOVE_COMPUTER_X:
 ld DE, overwrite	;CARMOVE_COMPUTER_NEXT++ - car_pos_x++
 or A
 rr D
 rr E
 or A
 rr D
 rr E
 or A
 rr D
 rr E	;DE / 8 ... 割る数を8で割って、分数演算をする
 ;ここで0なら目指す方向はcar_dir = 0
 ld A, D
 or A
 jr nz, CARMOVE_COMPUTER_Y
 ld A, E
 or A
 jr z, CARMOVE_COMPUTER_DIRCHK	;割る数が0なら arctan (y/x) ≒ 90°のため、dir数=0
CARMOVE_COMPUTER_Y:	;Xの値を出して、X/8を計算
 ld HL, overwrite	;割られる数 CARMOVE_COMPUTER_NEXT + 2 ++ - car_pos_y++
 xor A
CARMOVE_COMPUTER_Y_LP:
 sbc HL, DE
 inc A	;A++ / 8
 jr nc, CARMOVE_COMPUTER_Y_LP
 dec A
 ;call log_A
 srl A	;0 ~ 3 0.25ずつ
 bit 2, B	;4 or 0
 jr z, CARMOVE_COMPUTER_Y_AADD

 ld C, A
 ld A, 4
 sub C
CARMOVE_COMPUTER_Y_AADD:
 add A, B	;0 ~ 7
CARMOVE_COMPUTER_QUADRANT:	;時計回りに1, 4, 3, 2象限ごとに加算
 ld B, overwrite
 ld C, 8
CARMOVE_COMPUTER_QUADLP:
 add A, C
 djnz CARMOVE_COMPUTER_QUADLP

CARMOVE_COMPUTER_DIRCHK:;in A = 目指す方向 基礎値
 ;領域チェックして0 ~ 3に分け、x, yを整数化して
 ;目的角度に8*0~3足したもので比較する
 ld B, A
 ld A, (IX + dir)
 sub B
 ;push AF
 ;call log_A
 ;pop AF
 jr z, CARMOVE_AUTO
 ld HL, keysc_key
 jr nc, CARMOVE_COMPUTER_DIRLEFT
CARMOVE_COMPUTER_DIRRIGHT:
 neg
 cp 16
 jr nc, CARMOVE_COMPUTER_DIRLEFT_2
CARMOVE_COMPUTER_DIRRIGHT2:
 set 1, (HL)	;右
 jp CARMOVE_AUTO
CARMOVE_COMPUTER_DIRLEFT:
 cp 16
 jr nc, CARMOVE_COMPUTER_DIRRIGHT2
CARMOVE_COMPUTER_DIRLEFT_2:
 set 0, (HL)	;左
 jp CARMOVE_AUTO

;--------- 敵車専用ルーチン ------------
CARMOVE_AUTO:
 ld A, (IX + auto_pitin_flag)
 or A
 jr nz, CARMOVE_AUTO_PITIN
 call CARMOVE_AUTO_ACCEL
 call CARMOVE_AUTO_BRAKE
 call CARMOVE_AUTO_HUNDLE
 call CARMOVE_MOVEPOS
 call CARMOVE_POINT
 ret

;敵のピットイン処理
CARMOVE_AUTO_PITIN_WAIT_TIMERS:
 dw overwrite, overwrite, overwrite, overwrite
CARMOVE_AUTO_PITIN:
 cp 2
 jr c, CARMOVE_AUTO_PITIN_MOVE  ;ピット位置まで移動
 jr z, CARMOVE_AUTO_PITIN_WAIT  ;ピット位置で待機
CARMOVE_AUTO_PITIN_OUT:         ;先頭のピット位置まで移動
 ld DE, (car_pit_pos + 2)
 ld HL, (car_pos_y_crnt_v)
 ;call log_HL_kwait
 or A
 sbc HL, DE
 ld HL, (car_pos_y_crnt)
 jr z, CARMOVE_AUTO_PITIN_OUT_E
 dec (HL)
 ret nz
 inc HL
 dec (HL)
 ret
CARMOVE_AUTO_PITIN_OUT_E:
;g_countリセット
 ld HL, (g_count_crnt)
 xor A
 ld (HL), A
 inc HL
 ld (HL), A
;周回
 call CARMOVE_POINT_LAP_EXEC
 jr CARMOVE_AUTO_PITIN_COM_E
CARMOVE_AUTO_PITIN_MOVE:
CARMOVE_AUTO_PITIN_MOVE_X:
 ld DE, (car_pos_x_crnt_v)
 ;call log_HL_kwait
 ld HL, (car_pit_pos_crnt_v)
 ;call log_HL_kwait
 or A
 sbc HL, DE
 ld HL, (car_pos_x_crnt)
 jr z, CARMOVE_AUTO_PITIN_MOVE_Y
 
 inc (HL)
 jr nz, CARMOVE_AUTO_PITIN_MOVE_Y
 inc HL
 inc (HL)
CARMOVE_AUTO_PITIN_MOVE_Y:
 ;call log_HL_kwait
 ld HL, (car_pit_pos_crnt)
 inc HL
 inc HL
 ld E, (HL)
 inc HL
 ld D, (HL)
 ld HL, (car_pos_y_crnt_v)
 ;call log_HL_kwait
 or A
 sbc HL, DE
 ld HL, (car_pos_y_crnt)
 jr z, CARMOVE_AUTO_PITIN_MOVE_E
 dec (HL)
 ret nz
 inc HL
 dec (HL)
 ret
CARMOVE_AUTO_PITIN_MOVE_E:
;ピット待機タイマーセット
 ld HL, (CARMOVE_AUTO_PITIN_W_T_crnt)
 call rand
 or 80h
 rl A
 ld D, 0
 jr nc, CARMOVE_AUTO_PITIN_MOVE_E2
 inc D
CARMOVE_AUTO_PITIN_MOVE_E2:
 ld (HL), A
 inc HL
 ld (HL), D
CARMOVE_AUTO_PITIN_COM_E:
;フラグを1すすめる
 ld A, (IX + auto_pitin_flag)
 inc A
 and 3
 ld (IX + auto_pitin_flag), A
 ret
CARMOVE_AUTO_PITIN_WAIT:
;ピット待機処理
 ld HL, (CARMOVE_AUTO_PITIN_W_T_crnt)
 ld E, (HL)
 inc HL
 ld D, (HL)
 dec DE
 ld A, E
 or D
 jr z, CARMOVE_AUTO_PITIN_COM_E
 ld (HL), D
 dec HL
 ld (HL), E
 ret

CARMOVE_AUTO_ACCEL:
 ;ld A, (car_slip_x)
 ;and 10000000b
 ;ret nz

 ld A, (keysc_key)
 bit 3, A
 ret z

 ld A, (objno)
 ld HL, car_coms_speedlevel - 1
 ;ld E, A
 ;ld D, 0
 ;add HL, DE
 call HL_ADD_A
 ld A, (HL) ;車固有の加速度を足す
 add A, 124 ;ベース最高加速度
 ld C, A

 ld A, (IX + speed)
 ld B, A
 ld A, cpu_max_speed  ;CPU最高速度
 sub B  ;236 - speed
 ret c
 cp C
 jr c, CARMOVE_AUTO_ACCEL_2
 ld A, C
CARMOVE_AUTO_ACCEL_2:
 ld E, A
 ld D, 0
 ld A, (IX + acceleration)
 ld L, A
 ld H, 0
 add HL, DE
 ld DE, 192 ;1キロ加速するのに必要な加速度
 or A
 ld B, 255
CARMOVE_AUTO_ACCEL_LP:
 inc B
 sbc HL, DE
 jr nc, CARMOVE_AUTO_ACCEL_LP
 ld A, B
 or A
 ld A, L
 jr z, CARMOVE_AUTO_ACCEL_E
 add HL, DE

 ld A, (IX + speed)
 add A, B
 ld (IX + speed), A
 ld A, L
CARMOVE_AUTO_ACCEL_E:
 ld (IX + acceleration), A
 ret

CARMOVE_AUTO_BRAKE:
 ld A, (keysc_key)
 bit 2, A
 ret z

 ld A, (IX + speed)
 or A
 ret z
 dec A
 ld (IX + speed), A
 ret

CARMOVE_AUTO_HUNDLE:
 ld A, (keysc_key)
 ld B, A
 
 ld HL, (CARMOVE_HUNDLE_V_crnt)
 inc HL
 ld A, (HL)

 bit 0, B
 jr nz, CARMOVE_AUTO_HUNDLE_L
 bit 1, B
 jr nz, CARMOVE_AUTO_HUNDLE_R
 
 ret

CARMOVE_AUTO_HUNDLE_L:
 or A
 ld A, 0
 ld (HL), A
 ret z
 
 ld A, (IX + dir)
 dec A
 jp p, CARMOVE_AUTO_HUNDLE_LL
 ld A, 31
CARMOVE_AUTO_HUNDLE_LL:
 ld (IX + dir), A
 ret

CARMOVE_AUTO_HUNDLE_R:
 or A
 ld A, 0
 ld (HL), A
 ret z
 
 ld A, (IX + dir)
 inc A
 cp 32
 jr c, CARMOVE_AUTO_HUNDLE_RR
 xor A
CARMOVE_AUTO_HUNDLE_RR:
 ld (IX + dir), A
 ret

CARMOVE_A_RACEEND:
 cp 2
 call z, result_check
 ld A, (IX + speed)
 inc A
 ret z
 ld (IX + speed), A
 ret

CARMOVE_A:
 ld A, (lap_end)
 or A
 jr nz, CARMOVE_A_RACEEND

 ld A, (on_sand)
 cp 2
 call z, sand_action

 ld HL, lap_time
 inc (HL)
 inc HL
 jr nz, CARMOVE_A_TIMEINC
 ld A, (HL)
 cp 0ch
 jr z, CARMOVE_A_TIMEINC
 inc (HL)
CARMOVE_A_TIMEINC:
;ガソリン入りの重さを更新
 ld A, (car_fuel_st + 1)
 rra  ;127
 rra  ;63
 rra  ;31
 rra  ;15
 rra  ;7
 and 00000111b
 ld B, A
 ld A, (car_weight_st)
 add A, B
 ld (car_weight_and_fuel_st), A

 ld A, (car_crash_fl)
 cp 32	;クラッシュ!
 jr nz, CARMOVE_PITCHECK
 ld HL, ENDTIMER_COUNT
 dec (HL)
 jp z, CARMOVE_RETIRE
 ret
CARMOVE_PITCHECK:
 ld A, (car_pitin)
 or A
 jp nz, CARMOVE_PITIN ;ピットイン中

;自走不能の処理
 ld A, (car_speed)
 or A
 jr nz, CARMOVE_TIRE_BURST
 ld HL, car_engine_st
 bit 7, (HL)
 ld HL, CANNOT_MES1
 jr nz, CANNOT_RUN_RETIRE
 ld HL, car_fuel_st + 1
 xor A
 or (HL)
 ld HL, CANNOT_MES2
 jr z, CANNOT_RUN_RETIRE
 jr CARMOVE_TIRE_BURST
CANNOT_RUN_RETIRE:
 push HL
 ld DE, 0201h
 call sub_window_draw
 pop HL
 ld DE, 0308h
 ld B, 9
 call msp
 call TIM255
 call TIM255
 call kwait
 jp CARMOVE_RETIRE
CANNOT_MES1:
 db "ｴﾝｼﾞﾝﾌﾞﾛｰ"
CANNOT_MES2:
 db "ﾈﾝﾘｮｳｷﾞﾚ "
CARMOVE_TIRE_BURST:
 ;タイヤバースト時の処理
 ld A, (car_tire_rest_st + 1)
 or A
 call z, TIRE_BURST

 ld A, (keysc_key)
 push AF
 bit 5, A
 jr z, CARMOVE_PITINss
 call pause
CARMOVE_PITINss:
 pop AF
 bit 3, A
 call nz, CARMOVE_ACCEL
 ld A, (keysc_key)
 bit 2, A
 call nz, CARMOVE_BRAKE
 call CARMOVE_RPM
 call CARMOVE_RESISTANCE;自車のみ
 call CARMOVE_TRANSMISSION
 xor A
 ld (CARMOVE_PREVPOS_STR_A + 1), A
 call CARMOVE_MOVEPOS
 call CARMOVE_HUNDLE
 call CARMOVE_GRAVITY	;自車のみ
 call CARMOVE_RADIATOR

 call CARMOVE_BREAKCHECK
 ;call BRAKE_ENGINE
 ;call BRAKE_RADIATOR
 ;ウィングは衝突時に壊れる
 ;タイヤは曲がり操作、スリップ時のときに消耗する
 ;トランスミッションはミッション操作時に壊れる
 ;サスペンションは衝突時、ダート進入時に壊れやすくなる
 ;call BRAKE_SUSPENSION

 call RAND  ;最低1つは乱数を回す

;レンジ設定のウェイト
 ld A, (speed_wait)
 ;call TIMER

 ret
 
jjj:
 db 0
 
;アクセルを押したときの速度増減
;チューン設定を読み込みアクセル加速度を加える(速度は大方の場合増える)
CARMOVE_ACCEL:
;エンジン回転数チェック
 ;ld A, (car_rpm_st)
 ;cp 160
 ;ret nc
;オーバーヒートの処理
 call CARMOVE_OVERHEET
;エンジン故障チェック
 ld A, (car_engine_st)
 bit 7, A
 ret nz
;燃料チェック
 ld HL, (car_fuel_st)
 ld A, H
 or L
 ret z
CARMOVE_ACCEL_FUELOK:
;電気系チェック
 ld A, (car_elect_st)
 bit 7, A
 jr z, CARMOVE_ACCEL_ELECOK
;4回に3回しか加速しない
 inc A
 and 10000011b
 ld (car_elect_st), A
 and 00000011b
 ret z
CARMOVE_ACCEL_ELECOK:
;バックチェックc
 ld A, (IX + speed)
 or A
 jr nz, CARMOVE_ACCEL_CMN
 ld A, (car_transmission_st)				;速度0でバックフラグリセット
 and 11111000b
 or 1
 ld (car_transmission_st), A
 ld A, (keysc_key)
 bit 6, A		;速度0 & アクセル & 下キー
 jr z, CARMOVE_ACCEL_CMN
 ld A, (car_transmission_st)
 and 11111000b
 ld (car_transmission_st), A
CARMOVE_ACCEL_CMN:
 ld A, (car_rpm_st)      ;IXに入れ替える
 ld B, A
 ld A, 159  ;リミット15900rpmで固定
 cp B	;car_transmission_limit_st - car_rpm_st < 0 = limit over
 ret c
 ld E, B
 ld D, 0
 ld HL, car_acceleration_table
 add HL, DE
 ld A, (HL)		;加える加速度
 ld E, A		;E = 基本トルク	
 ld HL, CARMOVE_TUNE_ACCEL
 ld A, (car_tune_st)
 ld C, A
 ld B, 0
 add HL, BC
 ld A, (HL)		;チューン開始回転数
 ld B, A		;B = チューン開始回転数
 ld A, (car_rpm_st)
 sub B
 jr c, CARMOVE_ACCEL_TL		;チューントルクバンド外
 cp 67
 jr nc, CARMOVE_ACCEL_TL	;チューントルクバンド外
 ld HL, (car_fuel_st)
 ld BC, 1
 or A
 sbc HL, BC
 jr nc, CARMOVE_TUNEFUEL:
 ld HL, 0
CARMOVE_TUNEFUEL:
 ld (car_fuel_st), HL
 ld HL, car_acceleration_tune_table
 ld C, A
 ld B, 0
 add HL, BC
 ld A, (HL)		;チューントルク
 add A, E		;チューン加速度に基本トルクを加える
 ld E, A		;E = 加速度
CARMOVE_WING:
 ld A, (car_wing_st)  ;ウィングの加速度ボーナス
 cp 8
 jr c, CARMOVE_WING2
 xor A                ;ウィングの故障で抵抗が増え、ボーナスなし
CARMOVE_WING2:
 add A, E
 ld E, A
CARMOVE_ACCEL_TL:
;ここで重さの分を加速度から引いていく
 ld A, (car_weight_and_fuel_st)
 ld B, A
 ld A, E
 sub B	;加速度 - 重さ
 jr nc, CARMOVE_ACCEL_TL2
 xor A
CARMOVE_ACCEL_TL2:
 ld E, A
;オーバーヒート、電気系 加速鈍り
 ;ld A, (ENGINE_BLOW + 1)  ;Hibyte
;コンピュータ加速鈍り
 ld A, (ENGINE_OVERHEET + 1)
 ;call log_A
 sub 20h   ;32度以上で加速にぶる
 ld B, A  ;オーバーヒート数
 ld A, E  ;加速度
 jr c, CARMOVE_ACCEL_TL3
 sub B
 jr nc, CARMOVE_ACCEL_TL3
 xor A
CARMOVE_ACCEL_TL3:
 ld E, A

 ld A, (car_speed)
 cp 100
 jr c, CARMOVE_ACCEL_CALCU
;ダートに乗っている
 ld A, (on_sand)
 cp 2 ;2で芝生の上
 jr nz, CARMOVE_ACCEL_CALCU
 srl E
 srl E
 srl E  ;ダートで加速度1/8
CARMOVE_ACCEL_CALCU:  ;加速度計算
 ld D, 0
 ld A, (car_acceleration)
 ;call loG_A
 ld L, A
 ld H, 0
 add HL, DE
 ld DE, 127		;1キロ加速するのに必要な加速度
 ld B, 0
CARMOVE_ACCEL_L:
 ld A, H
 or A
 jr nz, CARMOVE_ACCEL_L2
 ld A, L
 cp 128
 jr c, CARMOVE_ACCEL_L3
CARMOVE_ACCEL_L2:
 sbc HL, DE
 inc B		;加速
 jr CARMOVE_ACCEL_L
CARMOVE_ACCEL_L3:
 ld (car_acceleration), A
CARMOVE_ACCEL_L4:
 ld A, (car_speed)
 add A, B
 jr nc, CARMOVE_ACCEL_L5
 ld A, 255
CARMOVE_ACCEL_L5:
 ld (car_speed), A      ;IXに入れ替える
CARMOVE_FUEL:
 ld HL, (car_fuel_st)
 ld DE, 1
 or A
 sbc HL, DE
 jr nc, CARMOVE_FUEL_P
 ld HL, 0
CARMOVE_FUEL_P:
 ld (car_fuel_st), HL
 ;call log_HL
 ret

;チューン加速度の始まる回転数
CARMOVE_TUNE_ACCEL:
 db 14,28,43,57,72,87,102,117
 
;ブレーキを押したときの速度増減
;チューン設定を読み込みブレーキ加速度を加える(速度は減る)
CARMOVE_BRAKE:
 ld A, (car_speed)
 or A
 ret z
 ld B, A
 ld A, (car_brake_st)
 bit 7, A
 jr z, CARMOVE_BRAKE_L
 rra;ブレーキ故障
CARMOVE_BRAKE_L:
 and 00000111b
 inc A
 ld C, A
 ld A, B
 sub C
 jr nc, CARMOVE_BRAKE_END
 xor A
CARMOVE_BRAKE_END:
 ld (car_speed), A      ;IXに入れ替える
 ;call log_A
 ret
 
;回転数を速度に合わせる
CARMOVE_RPM:
 ;最適化したい
 ld HL, car_gear_speed_table_st + 11	;IX objno
 ;ld A, 0	;car_no
 ;ld DE, 160 * 6
 ;call xsan
 ld A, (car_transmission_st);1~6
 and 7
 or A
 jr z, CARMOVE_RPM_Z
 dec A
CARMOVE_RPM_Z:
 ld DE, 160
 call xsan
 ld A, (car_speed)
 ld B, A
 ld C, 12
CARMOVE_RPM_LP:
 ld A, (HL)
 inc HL
 inc C
 cp B
 jr z, CARMOVE_RPM_END2
 jr nc, CARMOVE_RPM_END
 jr CARMOVE_RPM_LP
CARMOVE_RPM_END:
 dec C
CARMOVE_RPM_END2:
 ld A, C
 cp 161
 jr c, CARMOVE_RPM_END3
 ld A, 160
CARMOVE_RPM_END3:
 ld (car_rpm_st), A
 
 ;エンジン負荷計算
 ld HL, (ENGINE_BLOW)
 ;call log_HL

 ld A, (car_rpm_st)
 ld B, A
 ld C, 129	;(+4~32)
 ld A, (car_engine_st)
 and 00000111b
 inc A
 rla
 rla
 add A, C
 ld C, A
 ld A, B
 sub C        ;負荷がかかるゾーン エンジン強さで変化する
 jr c, CARMOVE_RPM_RADIATOR
 ld C, 8
 ld B, 0
 ld HL, (ENGINE_BLOW)
 add HL, BC
 jr nc, CARMOVE_RPM_ENGINE
 ld HL, 0ffffh
CARMOVE_RPM_ENGINE:
 ld (ENGINE_BLOW), HL   ;Hibyteの値がb0hを超えたらこわれる。70hから確率で壊れる。
 ld A, H
 ;call log_A
 cp 0f0h
 ret c
 call RAND
 cp 97;ランダム値
 jr CARMOVE_RPM_ENGINE_BR
 ;jr z, CARMOVE_RPM_ENGINE_BR
 ld A, H
 cp 0ffh
 ret nz
CARMOVE_RPM_ENGINE_BR:
 ld A, (car_engine_st)
 set 7, A  ;完全故障サイン
 ld (car_engine_st), A
 ret
CARMOVE_RPM_RADIATOR:
 ld A, (car_radiator_st)
 bit 7, A
 ret nz   ;ラジエータ故障中 エンジンを冷やせない 焼付き過進行
 ld HL, (ENGINE_BLOW)
 inc A
 ld C, A
 ld B, 0
 or A
 sbc HL, BC
 jr nc, CARMOVE_RPM_RADIATOR2
 ld HL, 0
CARMOVE_RPM_RADIATOR2:
 ;ld A, H
 ;call log_A
 ld (ENGINE_BLOW), HL
 ;call log_HL
 ;ld HL, (ENGINE_OVERHEET)
 ;ld DE, 0300h
 ;call log_POS_HL
 ret

;走行抵抗 主に空気抵抗(車体が軽いのでほぼ線形に)
CARMOVE_RESISTANCE:
 ld A, (car_speed)	;IX
 or A
 ret z
 ld A, (car_wing_st)
 bit 4, A	;ウィングの故障で空気抵抗が増える
 ld C, 0
 jr z, CARMOVE_RESISTANCE_WING
 ld C, 50
 jr CARMOVE_RESISTANCE_LP
CARMOVE_RESISTANCE_WING:
 and 00000111b
 sla A
 sla A
 ld B, A
 ld A, 32
 sub B
 ld C, A	;ウィング抵抗 = 29 - (0~7)*4 or 50
CARMOVE_RESISTANCE_LP:
 ld A, (car_acceleration)	;IX
 sub C
 ld (car_acceleration), A
 jr nc, CARMOVE_RESISTANCE_SPD
 ld A, (car_speed)	;IX
 dec A
 ld (car_speed), A
CARMOVE_RESISTANCE_SPD:
 ld A, (car_speed)
 rra
 rra
 rra
 rra
 and 00001111b
 ld C, A
 ld A, (car_acceleration)	;IX
 sub C
 ld (car_acceleration), A
 jr nc, CARMOVE_RESISTANCE_E
 ld A, (car_speed)	;IX
 dec A
 ld (car_speed), A
CARMOVE_RESISTANCE_E:
 call CARMOVE_ENGINE_BRAKE
 ret

;エンジンブレーキ
 ;アクセルが踏まれていないときの速度増減
 ;チューン設定を読み込みエンジンブレーキ加速度を加える(速度は減る)
CARMOVE_ENGINE_BRAKE:
 ld A, (car_slip_x)
 and 10000000b
 jr nz, CARMOVE_ENGINE_BRAKE_2

 ld A, (keysc_key)
 bit 3, A
 ret nz

 ld A, (car_speed)      ;IXに入れ替える
 or A
 ret z

CARMOVE_ENGINE_BRAKE_2:
 ld A, (car_engine_brake_st)
 and 00000111b
 inc A
 ld L, 192	;基本強さ

 ld A, (car_rpm_st)
 ld C, 20
 ld B, 1
 sub 56
 jr c, CARMOVE_ENGINE_BRAKE_E
 ;jr z, CARMOVE_ENGINE_BRAKE_E
CARMOVE_ENGINE_BRAKE_P:
 sub C
 jr c, CARMOVE_ENGINE_BRAKE_M
 ;jr z, CARMOVE_ENGINE_BRAKE_M
 ex AF, AF'
 ld A, B
 add A, 2
 ld B, A
 ex AF, AF'
 jr CARMOVE_ENGINE_BRAKE_P
CARMOVE_ENGINE_BRAKE_M:
 ld H, 0
 ld A, B
 or A
 jr z, CARMOVE_ENGINE_BRAKE_E
CARMOVE_ENGINE_BRAKE_M2:
 ld E, L
 ld H, 0
 ld L, B
 ld A, (car_engine_brake_st)
 cpl	;7 -> 0 0 -> 7 2 -> 5
 and 00000111b
 inc A
 call WSAN
 ld B, A
 ld L, E
CARMOVE_ENGINE_BRAKE_LP:
 ld A, (car_acceleration)	;IX
 sub L
 ld (car_acceleration), A
 jr nc, CARMOVE_ENGINE_BRAKE_E
 ld A, (car_speed)	;IX
 dec A
 ld (car_speed), A
 or A
 jr z,CARMOVE_ENGINE_BRAKE_E
CARMOVE_ENGINE_BRAKE_LP2:
 djnz CARMOVE_ENGINE_BRAKE_LP
CARMOVE_ENGINE_BRAKE_E:
 ret

;シフトチェンジなど
CARMOVE_TRANSMISSION:
 ld A, (car_transmission_st)
 and 00000111b
 or A
 ret z  ;バック時は変速しない

CARMOVE_TRANSMISSION_AT:
 ;シフトアップ
 ld A, (car_rpm_st)
 ld B, A
 ld A, (car_transmission_shiftup_st)
 cp B	;car_transmission_shiftup_st - car_rpm_st <= 0 シフトアップ
 jr c, CARMOVE_TRANSMISSION_SHIFTUP
 jr z, CARMOVE_TRANSMISSION_SHIFTUP
 ;シフトダウン
 ld HL, car_transmission_shiftdown_st
 ld DE, 6
 ld A, (objno)
 call xsan
 ld A, (car_transmission_st)
 and 7
 dec A
 jp z, CARMOVE_TRANSMISSION_END
 ;ld E, A
 ;ld D, 0
 ;add HL, DE
 call HL_ADD_A
 ld A, (HL)
 dec A	;念の為閾値を下げる
 dec A
 cp B	;car_transmission_shiftdown_st - car_rpm_st > 0
 jr nc, CARMOVE_TRANSMISSION_SHIFTDOWN
 jr CARMOVE_TRANSMISSION_END
CARMOVE_TRANSMISSION_SHIFTUP:
 ld A, (keysc_key)
 bit 3, A
 jp z, CARMOVE_TRANSMISSION_END ;アクセルオフで変速しない

 ld A, (car_transmission_st)
 ld C, 1  ;1速あとのギアのジョウタイ
 call check_mission
 jr z, CARMOVE_TRANSMISSION_END ;0 or 6速 or 故障
 and 00000111b
 cp 6
 jr z, CARMOVE_TRANSMISSION_END

 ld A, (car_transmission_st)
 inc A
 ld (car_transmission_st), A
CARMOVE_TRANSMISSION_END:
 ret

CARMOVE_TRANSMISSION_SHIFTDOWN:
 ex AF, AF'
 ld A, (keysc_key)
 bit 3, A
 jp nz, CARMOVE_TRANSMISSION_END
 ex AF, AF'
 
 ;call log_A_kwait;

 ld A, (car_transmission_st)
 ld C, 255  ;1速前のギアのジョウタイ
 call check_mission
 jr z, CARMOVE_TRANSMISSION_END
 and 00000111b
 dec A
 jr z, CARMOVE_TRANSMISSION_END
 ld A, (car_transmission_st)
 dec A
 ld (car_transmission_st), A
 jr CARMOVE_TRANSMISSION_END

CARMOVE_TACHOMETER:
 ;ld A, (objno)
 ;or A
 ;ret nz

 ld A, (car_transmission_st)
 and 7
 ld HL, num_charas
 jr nz, CARMOVE_TACHOMETER_NUM
 ld HL, chara_gear_R:
 jr CARMOVE_TACHOMETER_GP
CARMOVE_TACHOMETER_NUM:
 add A, A
 ld E, A
 add A, A
 add A, E
 ld E, A
 ld D, 0
 add HL, DE
CARMOVE_TACHOMETER_GP:
 ld DE, 2859h
 ld A, (car_sprite_fix + 1)
 add A, D
 ld D, A
 push DE
 ld B, 6
 call grp_overwrite

 ld A, (car_rpm_st)
 ld L, A
 ld H, 0
 add HL, HL
 add HL, HL
 add HL, HL
 add HL, HL;160*16 -> 2560
 ld DE, 530	;3*16 ≒ 333*0.16 53.28 精度誤差がいいかんじ
 ld A, 0	;3,4 ~ 48
 ld B, 10
CARMOVE_TACHOMETER_LP:
 or A
 sbc HL, DE
 jr c, CARMOVE_TACHOMETER_2
 add A, B
 jr CARMOVE_TACHOMETER_LP
CARMOVE_TACHOMETER_2:
 add HL, DE
 ld DE, 53
 ld B, A
CARMOVE_TACHOMETER_LP3:
 or A
 sbc HL, DE
 jr c, CARMOVE_TACHOMETER_E
 inc B
 jr CARMOVE_TACHOMETER_LP3
CARMOVE_TACHOMETER_E:
 ld HL, tachometa_graphics_l
 pop DE
 push DE
 ld E, 5fh
 call grp_overwrite
 
 ld HL, tachometa_graphics_r
 ld B, 6
 pop DE
 ld E, 137
 ld A, 3
 call grp_set_A
 ret

check_mission:;A=car_transmission_st C=1 or -1 後のギアの状態
 ld B, A
 and 00000111b
 add A, C
 or A     ;見るギアが0
 ret z
 cp 7     ;見るギアが7
 ret z
 ld C, A  ;C=チェックするギア
 ld A, B
 rra
 rra
 rra
 rra
 and 00000111b  ;A=故障しているギア
 cp C
 ld A, B  ;値復帰
 ret
 
tachometa_graphics_l:
 db 0fch,0fch,030h,030h,030h,030h
 db 030h,030h,030h,030h,030h,030h
 db 030h,030h,030h,030h,030h,030h
 db 030h,030h,030h,030h,030h,030h
 db 030h,030h,030h,030h,030h,030h
 db 030h,030h,030h,030h,030h,030h
 db 030h,030h,030h,030h,030h,030h
 db 030h,030h,030h,030h,030h,030h
tachometa_graphics_r:
 db 0fch,0fch,0,0,0,0
chara_gear_R::;R
 db 0FEh,0FEh,12h,12h,32h,0CCh
num_charas:
 db 38h,7Ch,82h,82h,7Ch,38h
 db 80h,84h,0FEh,0FEh,80h,80h
 db 84h,0C6h,0E2h,0B2h,9Eh,8Ch
 db 40h,0D2h,9Ah,9Eh,0F6h,62h
 db 30h,38h,2Ch,26h,0FEh,020h
 db 4Eh,0CEh,8Ah,8Ah,0FAh,70h
 db 78h,0FCh,96h,92h,0F2h,60h
 db 06h,06h,0F2h,0FAh,0Eh,06
 db 6Ch,0FEh,92h,92h,0FEh,6Ch
 db 0Ch,1Eh,92h,0D2h,7Eh,3Ch

;ハンドル操作 A= 0bit (left,right,z,x,tab,space,down,up) 7bit
;チューン設定、スピードと荷重をみてハンドルの方向替えを重くしたり軽くしたり
CARMOVE_HUNDLE:
;スリップ中
 ld A, (car_slip_x)
 or A
 jp nz, CARMOVE_HUNDLE_DRIFT
;スリップチェック
 ld A, (car_gravity + 1)
 ld L, A
 ld H, 0
 ld A, (car_gravity)
 ld E, A
 ld D, 0
 bit 7, A
 jr z, CARMOVE_HUNDLE_P
 neg
 ld E, A
 or A
 sbc HL, BC
 jr c, CARMOVE_HUNDLE_L
 jr CARMOVE_HUNDLE_C
CARMOVE_HUNDLE_P:
 add HL, DE
 ld A, H
 or A
 jp nz, CARMOVE_HUNDLE_SLIP
CARMOVE_HUNDLE_C:
 ld A, H
 or A
 jr z, CARMOVE_HUNDLE_L
 ld A, L
 cp 13		;スリップ閾値 267
 jp nc, CARMOVE_HUNDLE_SLIP
 
CARMOVE_HUNDLE_L:
 ld HL, (CARMOVE_HUNDLE_V_crnt)
 ;ld HL, CARMOVE_HUNDLE_V
 ;ld A, (objno)
 ;add A, A
 ;ld E, A
 ;ld D, 0
 ;add HL, DE
 
 ld E, (HL)
 inc HL
 ld D, (HL)

 ld A, D
 or A
 ret z	;曲がるのはDが1になってから
 xor A
 ld (HL), A

 ld A, (car_transmission_st)
 and 00000111b
 jr nz, CARMOVE_HUNDLE_F
CARMOVE_HUNDLE_R:
 ld A, (keysc_key)
 bit 0, A
 jp nz, CARMOVE_HUNDLE_TORIGHT
 bit 1, A
 jp nz, CARMOVE_HUNDLE_TOLEFT
 jr CARMOVE_HUNDLE_RFEND
CARMOVE_HUNDLE_F:
 ld A, (keysc_key)
 bit 0, A
 jp nz, CARMOVE_HUNDLE_TOLEFT
 bit 1, A
 jp nz, CARMOVE_HUNDLE_TORIGHT
CARMOVE_HUNDLE_RFEND:
 xor A	;center
 ld (car_temp_values + hundle_slipdir), A

 ret

;滑り処理 ドリフト　横荷重がほぼなくなるまでスピン 90度を超えてスピンすると制御を失う
CARMOVE_HUNDLE_SLIP:  ;開始トリガー
 ld A, 1
 ld (car_slip_x), A
CARMOVE_HUNDLE_DRIFT:	;ドリフト 連続的に滑るの意味
 ld A, (car_tire_st)
 ld E, A
 ld D, 0
 ld HL, (car_tire_rest_st)
 or A
 sbc HL, DE
 jr nc, CARMOVE_HUNDLE_DRIFT_TIRE
 ld HL, 0
CARMOVE_HUNDLE_DRIFT_TIRE:
 ld (car_tire_rest_st), HL
 
 ld A, (car_temp_values + hundle_slipdir)
 or A
 jp z, CARMOVE_HUNDLE_SLIP_END
 cp 2
 jp c, CARMOVE_DRIFT_TOLEFT
 jp CARMOVE_DRIFT_TORIGHT

CARMOVE_HUNDLE_SLIP_END:
 ld A, (car_slip_x)
 bit 7, A
 jr z, CARMOVE_HUNDLE_SLIP_END_L
 ld A, (car_speed)
 or A
 jr z, CARMOVE_HUNDLE_SLIP_END2
 ret

CARMOVE_HUNDLE_SLIP_END_L:
 ld A, (car_gravity)
 bit 7, A
 jr nz, CARMOVE_HUNDLE_SLIP_END2
 
 cp 60
 jr c, CARMOVE_HUNDLE_SLIP_END2
 
 ld A, (car_gravity)
 cp 127
 ret nc

CARMOVE_HUNDLE_SLIP_END2:
 xor A
 ld (car_slip_x), A
 ld A, (car_degree + 1)
 ld B, A
 ld A, (IX + dir)
 add A, B
 jp p, CARMOVE_HUNDLE_SLIP_ENDP
 add A, 32
CARMOVE_HUNDLE_SLIP_ENDP:
 cp 32
 jr c, CARMOVE_HUNDLE_SLIP_END32
 sub 32
CARMOVE_HUNDLE_SLIP_END32:
 ld (IX + dir), A
 xor A
 ld (car_gravity), A
 ld (car_slip_x), A
 ld (car_degree), A
 ld (car_degree + 1), A
 ret

CARMOVE_HUNDLE_ADDDEG:	;cyで回転させる
 ld A, (car_suspension_st)
 bit 4, A ;サスペンションの故障
 jr z, CARMOVE_HUNDLE_ADDDEG_1
 and 00000111b
 srl A
CARMOVE_HUNDLE_ADDDEG_1:
 ld C, A

 ld A, (car_speed)
 srl A					;1/2 (0~127)	;グリップ計算用
 ld D, A
 srl A					;1/4 (0~63)		;グリップ計算用
 ld E, A
 srl A					;1/8 (0~31)		;G計算用
 ld L, A
 ld A, D
 add A, E				;3/4 (0~190)
 add A, L       ;7/8 (0~223)
 ld B, A					;スピードごとに基本ハンドルグリップが減る

 ld A, (car_engine_st)
 and 00000111b
 or 00000100b
 ld D, A
 add A, L				;エンジンの重さを足す
 sub C					;0 ~ 31 - suspension(0 ~ 7) + エンジン(4 ~ 7)
 jr nc, CARMOVE_HUNDLE_ADDDEG_2
 xor A
CARMOVE_HUNDLE_ADDDEG_2:
 ld C, A
 ld A, (car_gravity + 1)	;スピードごとに横荷重が増えやすい
 add A, C
 jr nc, CARMOVE_HUNDLE_ADDDEG_GRAV
 ld A, 255
CARMOVE_HUNDLE_ADDDEG_GRAV:
 ld (car_gravity + 1), A

 ld L, A
 ld H, 0
 ld A, (car_gravity)
 bit 7, A
 jr nz, CARMOVE_HUNDLE_ADDDEG_SUS
CARMOVE_HUNDLE_GRAVPM:
 ld E, A
 ld D, 0
 add HL, DE
 ld DE, 160	;ロールし始める合計荷重
 call cp_hl_de
 jr c, CARMOVE_HUNDLE_ADDDEG_SUS

;ロール
 ld A, (car_gravity + 1)
 srl A
 srl A
 add A, B
 ld B, A
 jr nc, CARMOVE_HUNDLE_ADDDEG_SUS
 ld B, 255

CARMOVE_HUNDLE_ADDDEG_SUS:
 ld A, (car_tire_rest_st + 1)
 ;call log_A
 sub tire_consumption ;タイヤが約40%以下 28h / 6Dh
 ld E, 0
 jr nc, CARMOVE_HUNDLE_ADDDEG_TIR
 neg
 ld E, A
CARMOVE_HUNDLE_ADDDEG_TIR:
 ld A, (car_tire_st)
 inc A
 ld C, A
 sla C
 sla C					;*4 (4 ~ 32)
 ld A, 32
 sub C		;grip_minus_1 = 32-(4~32)
 add A, E ;タイヤ消耗度を足す
 add A, B	;grip_minus_2 = (grip_minus_1 + タイヤ消耗度) + car_speed/2
 jr nc, CARMOVE_HUNDLE_GRIP
 ld A, 255
CARMOVE_HUNDLE_GRIP:
 ld B, A
 xor A
 sub B
 ld B, A	;grip = 256 - grip_minus_x
 ld HL, car_temp_values + add_deg_value_cache
 ld A, (HL)
 add A, B	;ここでcyが起こるとハンドル角度が加算される
 ld (HL), A
 ex AF, AF'

 ;ld A, (objno)
 ;or A
 ;ret nz

 ld A, (car_tire_st)
 and 00000111b
 rla	;A = 0 ~ 14
 ld B, A
 ld A, (car_suspension_st)
 and 00000111b
 srl A	;A = 0 ~ 3
 add A, B
 ld B, A  ;B = (タイヤ + サスペンション)
 ld A, (car_weight_and_fuel_st)
 srl A
 srl A
 add A, B
 ld E, A	;タイヤ消費=(タイヤ + サスペンション / 2) + (car_weight_and_fuel_st) / 4
 ld D, 0
 ld HL, (car_tire_rest_st)
 or A
 sbc HL, DE
 jr nc, CARMOVE_HUNDLE_GRIP_TIRE
 ld HL, 0
CARMOVE_HUNDLE_GRIP_TIRE:
 ld (car_tire_rest_st), HL
 ld A, H
 ;call log_A

 ex AF, AF'
 ret

CARMOVE_HUNDLE_TOLEFT:
 ld A, 1	;left
 ld (car_temp_values + hundle_slipdir), A

 call CARMOVE_HUNDLE_ADDDEG
 ret nc
 ld A, (IX + dir)            ;IXに入れ替える
 ;call log_A_kwait
 dec A
 jp p, CARMOVE_HUNDLE_TOLEFT_2
 ld A, 31
CARMOVE_HUNDLE_TOLEFT_2:
 ld (IX + dir), A            ;IXに入れ替える
 ;call log_A_kwait
 jp CARMOVE_HUNDLE_SLIP_END

CARMOVE_HUNDLE_TORIGHT:
 ld A, 2	;right
 ld (car_temp_values + hundle_slipdir), A

 call CARMOVE_HUNDLE_ADDDEG
 ret nc
 ld A, (IX + dir)            ;IXに入れ替える
 ;call log_A_kwait
 inc A
 cp 32
 jr nz, CARMOVE_HUNDLE_TORIGHT_2
 xor A
CARMOVE_HUNDLE_TORIGHT_2:
 ld (IX + dir), A            ;IXに入れ替える
 ;call log_A_kwait 
 jp CARMOVE_HUNDLE_SLIP_END

CARMOVE_DRIFT_TOLEFT:
 ld HL, (car_degree)
 ld DE, 38
 or A
 sbc HL, DE
 ld A, H
 cp 32
 jp c, CARMOVE_DRIFT_TOLEFT_P
 ld H, 31
CARMOVE_DRIFT_TOLEFT_P:
 ld A, H
 cp 24
 jr nc, CARMOVE_DRIFT_TOLEFT_C	;90度は許容
 ld A, (car_slip_x)
 or 10000000b
 ld (car_slip_x), A
CARMOVE_DRIFT_TOLEFT_C:
 ld (car_degree), HL

 ld A, (keysc_key)
 bit 1, A
 jr z, CARMOVE_DRIFT_TOLEFT_NC
 ;カウンターでGをへらす
 ;call log_A
 ld A, (car_gravity + 1)
 sub 37
 jr nc, CARMOVE_DRIFT_TOLEFT_PLS
 xor A
CARMOVE_DRIFT_TOLEFT_PLS:
 ld (car_gravity + 1), A
 jp CARMOVE_HUNDLE_SLIP_END

CARMOVE_DRIFT_TOLEFT_NC:
 ld A, (car_slip_x)
 and 10000000b
 jp z, CARMOVE_HUNDLE_TOLEFT
 jp CARMOVE_HUNDLE_SLIP_END

CARMOVE_DRIFT_TORIGHT:
 ld HL, (car_degree)
 ld DE, 38
 add HL, DE
 ld A, H
 cp 32
 jr c, CARMOVE_DRIFT_TORIGHT_C
 ld H, 0
CARMOVE_DRIFT_TORIGHT_C:
 ld A, H
 cp 9
 jr c, CARMOVE_DRIFT_TORIGHT_CC	;90度は許容
 ld A, (car_slip_x)
 or 10000000b
 ld (car_slip_x), A
CARMOVE_DRIFT_TORIGHT_CC:
 ld (car_degree), HL
 
 ld A, (keysc_key)
 bit 0, A
 jr z, CARMOVE_DRIFT_TORIGHT_NC
 ;call log_A
 ;カウンターでGをへらす
 ld A, (car_gravity + 1)
 sub 37
 jr nc, CARMOVE_DRIFT_TORIGHT_P
 xor A
CARMOVE_DRIFT_TORIGHT_P:
 ld (car_gravity + 1), A
 jp CARMOVE_HUNDLE_SLIP_END

CARMOVE_DRIFT_TORIGHT_NC:
 ld A, (car_slip_x)
 and 10000000b
 jp z, CARMOVE_HUNDLE_TORIGHT
 jp CARMOVE_HUNDLE_SLIP_END

;Gボールの動きによるサスペンション操作
;自車のみ
CARMOVE_GRAVITY:
;自然にもとに戻る荷重
CARMOVE_GRAVITY_SUS:
 ld A, (car_gravity + 1)	;x
 sub 16			;サスペンションの強さは関係ない
 jr nc, CARMOVE_GRAVITY_A
 xor A
CARMOVE_GRAVITY_A:
 ld (car_gravity + 1), A
 
 ld A, (car_gravity)	;y
 bit 7, A
 jr z, CARMOVE_GRAVITY_SUB
CARMOVE_GRAVITY_ADD:
 add A, 16		;サスペンションの強さは関係ない
 jp m, CARMOVE_GRAVITY_E
 xor A
 jr CARMOVE_GRAVITY_E
CARMOVE_GRAVITY_SUB:
 sub 8		;サスペンションの強さは関係ない
 jp p, CARMOVE_GRAVITY_E
 xor A
CARMOVE_GRAVITY_E:
 ld (car_gravity), A
 
CARMOVE_GRAV_SPDDIFF:
;速度変化による荷重変化
 ld HL, CARMOVE_PREV_SPEED
 ld A, (HL)
 ld B, A
 ld A, (car_speed)			;IX
 sub B		;now - prev
 jr z, CARMOVE_GRAVITY_END
 jr nc, CARMOVE_GRAV_REAR
 jr CARMOVE_GRAV_FRONT
CARMOVE_GRAVITY_END:
 ld A, (car_speed)			;IX
 ld (HL), A ;IX

 call CARMOVE_GRAV_SPD
 ret
 
;G計算用(y)
CARMOVE_PREV_SPEED:
 db overwrite 	;objno = 0

CARMOVE_GRAV_FRONT:
 ld E, 12		;サスペンションの強さによる
 ld D, 0
 ld HL, 0
 neg
 call xsan
 ld A, H
 or A
 jr nz, CARMOVE_GRAV_FRONT_1
CARMOVE_GRAV_FRONT_P:
 ld A, L
 cp 31
 jr c, CARMOVE_GRAV_FRONT_2
CARMOVE_GRAV_FRONT_1:
 ld L, 31	;max値
CARMOVE_GRAV_FRONT_2:
 ex DE, HL
 ld A, (car_gravity)
 ld H, 0
 add A, H
 push AF
 ld L, A
 or A
 sbc HL, DE
 pop AF
 jp p, CARMOVE_GRAV_FRONT_3
 ld A, L
 ld B, 0
 add A, B
 jp m, CARMOVE_GRAV_FRONT_3
 ld L, 128
CARMOVE_GRAV_FRONT_3:
 ld A, L
CARMOVE_GRAV_FRONT_E:
 ld (car_gravity), A
 ld A, (car_speed)			;IX
 ld (CARMOVE_PREV_SPEED), A ;IX
 ret 

CARMOVE_GRAV_REAR:
 ld E, 16		;サスペンションの強さによる
 ld D, 0
 ld HL, 0
 call xsan
 ld A, H
 or A
 jr nz, CARMOVE_GRAV_REAR_1
CARMOVE_GRAV_REAR_P:
 ld A, L
 cp 31
 jr c, CARMOVE_GRAV_REAR_2
CARMOVE_GRAV_REAR_1:
 ld H, 0
 ld L, 31	;max値
CARMOVE_GRAV_REAR_2:
 ld A, (car_gravity)
 ld B, 0
 add A, B
 push AF
 ld C, A
 add HL, BC
 pop AF
 jp m, CARMOVE_GRAV_REAR_3
 ld A, L
 ld B, 0
 add A, B
 jp p, CARMOVE_GRAV_REAR_E
 ld L, 127
CARMOVE_GRAV_REAR_3:
 ld A, L
CARMOVE_GRAV_REAR_E:
 ld (car_gravity), A
 ld A, (car_speed)
 ld (CARMOVE_PREV_SPEED), A
 ret 

CARMOVE_GRAV_SPD:
 ld A, (keysc_key)
 bit 3, A
 ret z

 ld A, (car_speed)
 cp 128
 jr c, CARMOVE_GRAV_SPD_F
 rra
 rra
 rra
 rra
 and 00001111b
 add A, 18 ;基礎値

 ld B, A
 ld A, (car_wing_st)
 and 00000111b
 ld C, A
 ld A, 7
 sub C				;ウィング抵抗軽減値 設定値7で0
 cpl				;	-1 ~ -8
 add A, B			;サスペンション機能が強すぎるのでスピード補正 ウィングの故障は影響しない
 
 ;push AF
 ;ld L, A
 ;ld H, 0
 ;call ASCN
 ;ld HL, NMEMRY
 ;ld DE, 0300h
 ;ld B, 5
 ;call msp	;ウィング軽減値
 ;pop AF
 
 ld L, A
 ld H, 0
 call CARMOVE_GRAV_REAR_P
 ret

CARMOVE_GRAV_SPD_F:
 ld L, A
 ;push AF
 ;ld L, A
 ;ld H, 0
 ;call ASCN
 ;ld HL, NMEMRY
 ;ld DE, 0300h
 ;ld B, 5
 ;call msp
 ;pop AF
 
 ld H, 0
 call CARMOVE_GRAV_FRONT_P
 ret

CARMOVE_MOVEPOS:
 ;方向ごとに加算 方向ごとにfpsを足して基準値を超えると動く
 ;方向ごとに分岐して、基準値の値を取得する。
 call CARMOVE_GET_DIR_VALUE			;方向ごとの基準フレーム数取得
 ld (CARMOVE_MOVEPOS_VEC_DE + 1), A
 xor A
 ld (CARMOVE_MOVEPOS_VEC_DE + 2), A	;移動の基準フレーム数をセット

CARMOVE_MOVEPOS_HL:	;あとハンドルの切り加減も変えないといけない。
 ;ld HL, overwrite		
 ld HL, (CARMOVE_MOVEPOS_VEC_crnt);0から始まり、基準値を超えているかチェックするための値
 ld (CARMOVE_MOVEPOS_STR + 1), HL;最後にもう一度値を更新するためにアドレスを書き込む
 ;;;;call log_HL_kwait
 ld HL, (CARMOVE_MOVEPOS_VEC_crnt_v)
 ;HL = (CARMOVE_MOVEPOS_VEC + carno*2)
 ld A, (IX + speed)     ;IXに入れ替える
 ld D, 0				;速度の値を加える 加えるDEの値でフレーム調整をうまいことする
 ld E, A

 add HL, DE
CARMOVE_MOVEPOS_VEC_DE:
 ld DE, overwrite		;各方向ごとの基準値 直角角度でfps 54 これ以下は計算誤差のため下げられない
 or A
 sbc HL, DE				;((CARMOVE_MOVEPOS_VEC++) + (car_speed)) > DE:方向ごとの基準フレーム数 のときのみ次に進める
 jr c, CARMOVE_MOVEPOS_END
 jr z, CARMOVE_MOVEPOS_END
CARMOVE_MOVEPOS_ADD:
 ;call log_HL_kwait
 ;call log_DE_kwait
 exx
 call CARMOVE_PREVPOS_STR	;はじめの一回のみ
 ;各方向ごとに決まった座標を加える
 call CARMOVE_ADD_DIRECTION_VALUE	;何回呼ばれてもハンドルが切られるのは一回のみ
 exx

 or A
 sbc HL, DE				;(HL + (car_speed)) > DE:方向ごとの基準フレーム数 のときのみ次に進める
 jr nc, CARMOVE_MOVEPOS_ADD
CARMOVE_MOVEPOS_END:
 add HL, DE
CARMOVE_MOVEPOS_STR:
 ld (overwrite), HL	;スピードを加えた値をストアし、連続的に値を変化させる。
 
 ret
 
;前回座標記憶
CARMOVE_PREVPOS_STR:
 ld A, (objno)
 or A
 ret nz

CARMOVE_PREVPOS_STR_A:
 ld A, overwrite	;一回のみ
 or A
 ret nz

 push HL
CARMOVE_PREVPOS_STR_X:
 ld HL, (car_pos_x)
 ld DE, (block_prevx + 1)
 call cp_hl_de
 jr z, CARMOVE_PREVPOS_STR_Y
 ld (block_prevx + 1), HL
CARMOVE_PREVPOS_STR_Y:
 ld HL, (car_pos_y)
 ld DE, (block_prevy + 1)
 call cp_hl_de
 jr z, CARMOVE_PREVPOS_STR_E
 ld (block_prevy + 1), HL
CARMOVE_PREVPOS_STR_E:
 pop HL
 ld A, 1
 ld (CARMOVE_PREVPOS_STR_A + 1), A	;一回のみ
 ret

;方向ごとの座標加算判定用の基準値を取得する。
CARMOVE_GET_DIR_VALUE:
 ld A, (IX + dir)	;A=0~31            ;IXに入れ替える
 ld HL, CARMOVE_GET_HUNDLEVALUE_TABLE
 ld E, A
 ld D, 0
 add HL, DE
 ld A, (HL)
 ret

CARMOVE_GET_HUNDLEVALUE_TABLE:
 ;dir 0 基準値=fps = 54
 ;dir 1 基準値=fps * 1 / cos(22.5) = 58
 ;dir 2 基準値=fps * 1.414 = 54 * 1.414 = 76
 ;dir 3 基準値=fps * 1 / cos(22.5) = 58
 ;dir 4 基準値=fps = 54 54
 ;dir 5 基準値=fps * 1 / cos(22.5) = 58
 ;dir 6 基準値=fps * 1.414 = 54 * 1.414 = 76
 ;dir 7 基準値=fps * 1 / cos(22.5) = 58
 ;dir 8 基準値=fps = 54
 ;dir 9 基準値=fps * 1 / cos(22.5) = 58
 ;dir 10 基準値=fps * 1.414 = 54 * 1.414 = 76
 ;dir 11 基準値=fps * 1 / cos(22.5) = 58
 ;dir 12 基準値=fps = 54
 ;dir 13 基準値=fps * 1 / cos(22.5) = 58
 ;dir 14 基準値=fps * 1.414 = 54 * 1.414 = 76
 ;dir 15 基準値=fps * 1 / cos(22.5) = 58
 db 54,55,58,65,76,65,58,55,54,55,58,65,76,65,58,55,54,55,58,65,76,65,58,55,54,55,58,65,76,65,58,55
; db 27,29,38,29,27,29,38,29,27,29,38,29,27,29,38,29
;db 27+13,29+14,38+19,29+14,27+13,29+14,38+19,29+14,27+13,29+14,38+19,29+14,27+13,29+14,38+19,29+14

;ハンドル角度ごとに決まった座標ベクトルを加える
CARMOVE_HUNDLE_ST:	;自車ハンドルのセッティング or 敵車のハンドルのセティング
 db overwrite
CARMOVE_ADD_DIRECTION_VALUE:
 ld A, (objno)
 or A
 ld A, 4
 jr nz, CARMOVE_ADD_DIRECTION_HNDSTR;enemy
 ld A, (car_hundle_st)
 and 00000111b
CARMOVE_ADD_DIRECTION_HNDSTR:
 ld (CARMOVE_HUNDLE_ST), A

 ld A, (keysc_key)
 and 00000011b
 jr z, CARMOVE_QUICK_SUB	;ハンドル操作は関係ないね
 
 ;毎フレーム実行されるという理由でハンドル操作の
 ;重みを再現
 ld HL, (CARMOVE_HUNDLE_V_crnt)
 ;;;;call log_HL_kwait
 push HL	;->*;HL = CARMOVE_HUNDLE_V++
 ld E, (HL)
 inc HL
 ld D, (HL)
 push DE	;->**;DE = (HUNDLE_V)
 
 ld A, (IX + hundle_quick)
 ld B, A	;ハンドル切れ角now

 ld A, (CARMOVE_HUNDLE_ST)
 inc A
 add A, B
 cp 9
 jr c, CARMOVE_ADD_DIRECTION_QUICK
 ld A, 8
CARMOVE_ADD_DIRECTION_QUICK:
 ld (IX + hundle_quick), A	;ハンドル切れ角 = 1 ~ 8	ハンドルクイック値を足す

 ld E, A
 ld D, 0
 ld A, 21
 ld HL, 0
 call xsan	;HL = 21 * (1 ~ 8)

 ld E, L
 ld D, 0
 pop HL		;->*;HL = (HUNDLE_V)
 add HL, DE	;ハンドル値に ハンドルクイック数*定数(21) を足す
 ex DE, HL
 pop HL		;->0;HL = CARMOVE_HUNDLE_V++
 ld (HL), E	;(HUNDLE_V)
 inc HL
 ld (HL), D	;(HUNDLE_V + 1)
 jr CARMOVE_QUICK_ADDED


CARMOVE_QUICK_SUB:
 ld A, (IX + hundle_quick)	;A = (CARMOVE_HUNDLE_QUICK++)
 ld B, A
 ld A, (CARMOVE_HUNDLE_ST)
 inc A
 ld C, A
 ld A, B
 sub C	;(CARMOVE_HUNDLE_QUICK++) - (CARMOVE_HUNDLE_ST)
 jr nc, CARMOVE_ADD_DIRECTION_VALUE_1C
 xor A
CARMOVE_ADD_DIRECTION_VALUE_1C:
 ld (IX + hundle_quick), A	;ハンドルクイック値を減らす
CARMOVE_QUICK_ADDED:
 ld A, (IX + dir)			;0 ~ 31            ;IXに入れ替える
 ;call log_A
 ld B, A

 ld A, (objno)
 or A
 jr nz, CARMOVE_ADD_DIRECTION_VALUE_NR

 ld A, (car_transmission_st)
 and 10000111b
 or A
 jr nz, CARMOVE_ADD_DIRECTION_VALUE_NR
 ;自機 and バックギア
 ld A, 16
 add A, B	;バックするときに進行方向を180°ターンさせる
 and 00011111b
 ld B, A

CARMOVE_ADD_DIRECTION_VALUE_NR:
 ld E, B
 ld D, 0
 ld HL, CARMOVE_ADD_DIRECTION_VALUE_TABLE
 add HL, DE
 add HL, DE
 ;call log_HL 正しいアドレス y
 ld D, (HL)
 inc HL
 ld E, (HL);D=addX E=addY

;こっから面倒くさい
CARMOVE_ADD_DIRECTION_VALUE_Y:
 ld A, E
 or A
 jr z, CARMOVE_ADD_DIRECTION_VALUE_X
 ;ld BC, CARMOVE_ADD_DIRECTION_VALUE_CACHE	;objnoを考慮
 ld HL, (CARMOVE_ADD_DIRECTION_VC_crnt)
 ;add HL, BC
 ;;;;call log_HL_kwait
 ld A, (HL)
 bit 7, E
 jr z, CARMOVE_ADD_DIRECTION_VALUE_Y2
 add A, E
 bit 7, A
 jr z, CARMOVE_ADD_DIRECTION_VALUE_XB
 cp 0fdh
 jr nc, CARMOVE_ADD_DIRECTION_VALUE_XB
 xor A
 ld (HL), A
 push DE
 ;ld DE, car_pos_y	;objnoを考慮
 ld HL, (car_pos_y_crnt)
 ;add HL, DE
 ld E, (HL)
 inc HL
 ld D, (HL)
 ex DE, HL
 dec HL	;-1
CARMOVE_ADD_DIRECTION_VALUE_Y22:
 jr nc, CARMOVE_ADD_DIRECTION_VALUE_Y23
 ld HL, 0
CARMOVE_ADD_DIRECTION_VALUE_Y23:
 ex DE, HL
 ld (HL), D
 dec HL
 ld (HL), E
 pop DE
 jr CARMOVE_ADD_DIRECTION_VALUE_X
CARMOVE_ADD_DIRECTION_VALUE_Y2:
 add A, E
 cp 4
 jr c, CARMOVE_ADD_DIRECTION_VALUE_XB
 xor A
 ld (HL), A
 push DE
 ;ld DE, car_pos_y
 ld HL, (car_pos_y_crnt)
 ;add HL, DE
 ld E, (HL)
 inc HL
 ld D, (HL)
 ex DE, HL
 inc HL	;+1
 jr CARMOVE_ADD_DIRECTION_VALUE_Y22
 
CARMOVE_ADD_DIRECTION_VALUE_XB:
 ld (HL), A
CARMOVE_ADD_DIRECTION_VALUE_X:
 ld A, D
 or A
 jr z, CARMOVE_ADD_DIRECTION_VALUE_END
 ;ld BC, CARMOVE_ADD_DIRECTION_VALUE_CACHE + 1	;objnoを考慮
 ld HL, (CARMOVE_ADD_DIRECTION_VC_crnt)
 inc HL
 ;add HL, BC
 ;;;;call log_HL_kwait
 ld A, (HL)
 bit 7, D
 jr z, CARMOVE_ADD_DIRECTION_VALUE_X2
 add A, D
 bit 7, A
 jr z, CARMOVE_ADD_DIRECTION_VALUE_ENB
 cp 0fdh
 jr nc, CARMOVE_ADD_DIRECTION_VALUE_ENB
 xor A
 ld (HL), A
 push DE
 ;ld DE, car_pos_x	;objnoを考慮
 ld HL, (car_pos_x_crnt)
 ;add HL, DE
 ld E, (HL)
 inc HL
 ld D, (HL)
 ex DE, HL
 dec HL	;-1
CARMOVE_ADD_DIRECTION_VALUE_X22:
 jr nc, CARMOVE_ADD_DIRECTION_VALUE_X23
 ld HL, 0
CARMOVE_ADD_DIRECTION_VALUE_X23:
 ex DE, HL
 ld (HL), D
 dec HL
 ld (HL), E
 pop DE
 jr CARMOVE_ADD_DIRECTION_VALUE_END
CARMOVE_ADD_DIRECTION_VALUE_X2:
 add A, D
 cp 4
 jr c, CARMOVE_ADD_DIRECTION_VALUE_ENB
 xor A
 ld (HL), A
 push DE
 ;ld DE, car_pos_x	;objnoを考慮
 ld HL, (car_pos_x_crnt)
 ;add HL, DE
 ld E, (HL)
 inc HL
 ld D, (HL)
 ex DE, HL
 inc HL	;+1
 jr CARMOVE_ADD_DIRECTION_VALUE_X22

CARMOVE_ADD_DIRECTION_VALUE_ENB:
 ld (HL), A
CARMOVE_ADD_DIRECTION_VALUE_END:
 ret

;タイヤバースト時の挙動 とりあえず左に回転するようにする。
TIRE_BURST:
 ld HL, keysc_key
 ld A, (HL)
 bit 1, A ;右キーが押されていたら何もしない
 ret nz
 set 0, (HL)  ;左に回転
 ld A, (HL)
 ret

CARMOVE_OVERHEET:
;オーバーヒートの処理
 ld HL, (ENGINE_OVERHEET)
 ld A, (car_engine_st)
 and 00000111b
 ld B, A
 ld A, 8
 sub B
 add A, 8 ;底値   A=8~16
 ld E, A
 ld D, 0
 add HL, DE
 jr nc, CARMOVE_OVERHEET2
 ld HL, 0ffffh
CARMOVE_OVERHEET2:
 ld (ENGINE_OVERHEET), HL
 ld A, H
 sub 20h
 ret c
 ld HL, (ENGINE_BLOW)
 call HL_ADD_A  ;オーバーヒート時、エンジンブローも進行させる
 ld (ENGINE_BLOW), HL
 ret

CARMOVE_RADIATOR:
 ld HL, (ENGINE_OVERHEET)
 ld A, H
 or A
 ret z
 ld A, (car_speed)
 rra
 rra
 rra
 rra  ;1/16
 and 00001111b
 ld B, A
 srl A  ;1/32
 add A, B ;3/32
 ld B, A
 ld A, (car_radiator_st)
 bit 7, A
 ret nz   ;ラジエータ故障中 エンジンを冷やせない
 ld C, A
 srl A
 add A, C
 add A, B
 ld E, A
 ld D, 0
 or A
 sbc HL, DE
 jr nc, CARMOVE_OVERHEET3
 ld HL, 0
CARMOVE_OVERHEET3:
 ld (ENGINE_OVERHEET), HL
 ret

;自然に確率で壊れる
CARMOVE_BREAKCHECK:
 ld A, (g_count_1 + 1)
 cp 80h
 ret c

 call RAND
 cp 4
 ret nc
 ;1/64
 call RAND
 ;1/16384 の確率で自然故障
 cp 72
 jp z, suspension_break
 cp 24
 jp z, radiator_break
 cp 157
 jp z, transmission_break
 cp 251
 jp z, electron_break
 cp 97
 jp z, brake_break
 ret

;------------- ピットイン 自車のみ -------------
CARMOVE_PITIN:;(car_pitin)
 cp 3
 jp z, CARMOVE_PITOUT_REP

 ld A, 11
 ld (car_rpm_st), A
 xor A
 ld (car_slip_x), A
 ld HL, (car_pos_x)
 ex DE, HL
 ld HL, (car_pit_pos)
 call cp_hl_de ;(car_pit_pos_x++) - (car_pos_x++) xベクトル プラスになると思う
 jr z, CARMOVE_PITIN_Y
 jr c, CARMOVE_PITIN_Y
CARMOVE_PITIN_X_2:
 ex DE, HL
 inc HL
 ld (car_pos_x), HL
CARMOVE_PITIN_Y:
 ld HL, (car_pos_y)
 ex DE, HL
 ld HL, (car_pit_pos + 2)
 call cp_hl_de ;(car_pit_pos_y++) - (car_pos_y++) yベクトル マイナスになると思う
 ex DE, HL
 dec HL
 ld (car_pos_y), HL
 ret c
 ld A, 1
 ld (CARMOVE_PITIN_MENU_MOVE + 1), A ;移動完了で1
CARMOVE_PITIN_MENU:
;自車メニュー
;できること
;・ポジション確認・残り周回数
;・損傷度確認と修理メニュー
CARMOVE_PITIN_MENU_MOVE:
 ld A, overwrite  ;0でピットイン途中
 or A
 ret z

;ラップの処理 レース終了することもある
 call lap_exec
;メニューに入ったらリセット
 xor A
 ld (car_pitin), A
;給油前のガソリン量
 ld A, (car_fuel_st + 1)
 ld (CARMOVE_FUEL_REP_SEC), A
;temp_hiの故障をセット
 ld A, (ENGINE_OVERHEET + 1)
 cp 20h
 jr c, CARMOVE_PITIN_MENU_SHOW
 ld HL, car_engine_st
 set 6, (HL)  ;オーバーヒートの不調

 ld A, (lap_end)
 or A
 jp nz, CARMOVE_PITIN_RACEEND
CARMOVE_PITIN_MENU_SHOW:
;メニュー表示
;修理することを示すアドレスをリセット
 ld HL, CARMOVE_REPAIR
 xor A
 ld (HL), A
 ld DE, CARMOVE_REPAIR + 1
 ld BC, 8
 ldir

CARMOVE_PITIN_MENU_DRAW:
 ld A, 0a7h
 out (40h), A
 ld A, 40h
 out (40h), A
 ld DE, 0004h
 call sub_window_draw

 ld A, (lap_remind)
 ld L, A
 ld H, 0
 call ASCN
 ld HL, NMEMRY + 2
 ld DE, CARMOVE_PITIN_MES + 4
 ldi
 ldi
 ldi

 ld A, (rank)
 add A, 30h
 ld (CARMOVE_PITIN_MES + 17), A

 ld HL, CARMOVE_PITIN_MES
 ld DE, 0103h
 ld A, 5
 ld C, 5
 call show_block_mes
 
 ld A, (game_mode)
 cp 1
 jr nz, CARMOVE_PITIN_MENU_DRAW_2

 ld HL, CARMOVE_PITIN_MES2
 ld DE, 0103h
 ld B, 19
 call msp
CARMOVE_PITIN_MENU_DRAW_2:
 xor A
 ld (CARMOVE_PITIN_INDEX), A
 ld (CARMOVE_PITIN_PREV), A
CARMOVE_PITIN_PROMPT:
 ld A, (CARMOVE_PITIN_PREV)
 add A, 2
 ld D, A
 ld E, 3
 call prompt_delete

 ld A, (CARMOVE_PITIN_INDEX)
 add A, 2
 ld D, A
 ld E, 3
 call prompt_r_view
CARMOVE_PITIN_MENU_MLP:
 call kscan_ex
 jr z, CARMOVE_PITIN_MENU_MLP
 call keysc
 ld B, A
 ld HL, CARMOVE_PITIN_INDEX
 ld A, (HL)
 ld (CARMOVE_PITIN_PREV), A
 bit 6, B
 jr nz, CARMOVE_PITIN_DOWN
 bit 7, B
 jr nz, CARMOVE_PITIN_UP
 bit 3, B
 jr nz, CARMOVE_PITIN_DONE
 jr CARMOVE_PITIN_MENU_MLP

CARMOVE_PITIN_DOWN:
 cp 2
 jr z, CARMOVE_PITIN_MENU_MLP
 inc A
 jp CARMOVE_PITIN_STR
CARMOVE_PITIN_UP:
 or A
 jr z, CARMOVE_PITIN_MENU_MLP
 dec A
 jp CARMOVE_PITIN_STR
CARMOVE_PITIN_DONE:
 or A
 jr z, CARMOVE_PITOUT  ;0:ピットアウト
 ;1:コンディション / シュウリ
 ;2:リアイア
 cp 2
 jp c, CARMOVE_CONDITION
 jp z, CARMOVE_RETIRE
 jr CARMOVE_PITIN_PROMPT
CARMOVE_PITOUT:
 ld A, 255
 ld (SCROLL_A + 1), A
 call SCROLL
;x位置強制補正
 ld HL, (car_pit_pos)
 ld (car_pos_x), HL
;BG初期化
 xor A
 ld (cource_inited), A
 dec A
 ld (current_bg_index), A
 call BGMOVE
;画面反転解除
 ld A, 0a6h
 out (40h), A
;ピットイン時アニメ再生
 ld A, 3
 ld (car_pitin), A
;修理画像アニメ再生秒数値設定
 ld A, (car_fuel_st + 1)
 ld B, A
 ld HL, CARMOVE_FUEL_REP_SEC
 ld C, (HL)
 xor A
 ld (HL), A ;給油なし
 ld A, C
 sub B
 jr z, CARMOVE_PITOUT_NOFUEL
 neg
 ld BC, 25
CARMOVE_PITOUT_FUELLP:
 inc B  ;Bの秒数だけ給油入れる
 sub C
 jr nc, CARMOVE_PITOUT_FUELLP
 ld (HL), B   ;;1 ~ 6秒
 ld A, B
CARMOVE_PITOUT_NOFUEL:
;B=エンジン系 C=ウィング D=ブレーキサス E=タイヤ
 ld BC, 0
 ld DE, 0
 ld HL, CARMOVE_REPAIR
 ld A, 255 ;インデックス
 ld (CARMOVE_PITOUT_REPIDX), A
CARMOVE_PITOUT_REPLP:
 ld A, (CARMOVE_PITOUT_REPIDX)
 inc A
 ld (CARMOVE_PITOUT_REPIDX), A
 or A
 call z, engine_value_repair  ;オーバーヒート、エンジンブロー係数の軽減
 cp 8
 jr z, CARMOVE_PITOUT_REPGUS
 ld A, (HL)
 inc HL
 or A
 jr z, CARMOVE_PITOUT_REPLP
 cp 80h
 jr z, CARMOVE_PITOUT_REPLP
 push HL
 ld A, (CARMOVE_PITOUT_REPIDX)
 ld HL, CARMOVE_REPAIR_TIME
 call HL_ADD_A
 or A
 jr z, CARMOVE_PITOUT_B
 cp 2
 jr c, CARMOVE_PITOUT_C
 jr z, CARMOVE_PITOUT_B
 cp 4
 jr c, CARMOVE_PITOUT_B
 jr z, CARMOVE_PITOUT_D
 cp 6
 jr c, CARMOVE_PITOUT_B
 jr z, CARMOVE_PITOUT_E
 cp 8
 jr c, CARMOVE_PITOUT_D
 pop HL
 ;最後ガソリン 80h
CARMOVE_PITOUT_REPGUS:
 xor A
 ld HL, CARMOVE_PITOUT_REP_INDEX
 ld (HL), A ;インデックス初期化
 inc HL
 ld (HL), B ;B=エンジン系
 inc HL
 ld (HL), C ;C=ウィング
 inc HL
 ld (HL), D ;D=ブレーキサス
 inc HL
 ld (HL), E ;E=タイヤ
 ret  ;CARMOVE_Aに戻る

CARMOVE_PITIN_RACEEND:
 ld A, 100
 ld (car_speed), A
 ret

;オーバーヒート、エンジンブロー係数の軽減
engine_value_repair:
 push AF
 exx
 ld A, (car_engine_st)
 bit 7, A
 jr z, engine_value_repair_rd
 ld HL, 0
 ld (ENGINE_BLOW), HL   ;エンジンブロー時のみ係数を0にする
engine_value_repair_rd:
 bit 6, A
 jr z, engine_value_repair_e
 ld HL, ENGINE_OVERHEET + 1
 ld A, (HL)
 and 00000111b  ;20hからオーバーヒート 7h以下まで冷却
 ld (HL), A             ;エンジンオーバーヒート時のみ係数をHi=7以下にする
engine_value_repair_e
 exx
 pop AF
 ret

CARMOVE_PITOUT_REPIDX:
 db overwrite
CARMOVE_PITOUT_B:;A=インデックス
 ld A, (HL)
 call RandChange
 add A, B
 ld B, A
 jr CARMOVE_PITOUT_REPLPCM
CARMOVE_PITOUT_C:
 ld A, (HL)
 call RandChange
 add A, C
 ld C, A
 jr CARMOVE_PITOUT_REPLPCM
CARMOVE_PITOUT_D:
 ld A, (HL)
 call RandChange
 add A, D
 ld D, A
 jr CARMOVE_PITOUT_REPLPCM
CARMOVE_PITOUT_E:
 ld A, (HL)
 call RandChange
 add A, E
 ld E, A
CARMOVE_PITOUT_REPLPCM:
;故障を修理
 push DE
 ld A, (CARMOVE_PITOUT_REPIDX)
 add A, A
 ld HL, CARMOVE_BREAK_ADDRESS
 ld E, A
 ld D, 0
 add HL, DE
 ld E, (HL)
 inc HL
 ld D, (HL)
 ex DE, HL
 ld A, (HL)
 and 00000111b
 ld (HL), A
 pop DE
 pop HL
 jp CARMOVE_PITOUT_REPLP

HL_ADD_A:
 or A
 ret z
 push AF
 add A, L
 ld L, A
 jr nc, HL_ADD_A2
 inc H
HL_ADD_A2:
 pop AF
 ret
RandChange:
 ld L, A
 call RAND4
 add A, L
 ret
 
;-------- 修理作業アニメ -----------
;CARMOVE_PITOUT_REP_SECの値により分岐。最後に(car_pitin)を0にして終了
CARMOVE_PITOUT_REP:
 ld HL, CARMOVE_PITOUT_REP_INDEX
 ld A, (HL)
 inc HL
 ld B, 6  ;grp描画用
 cp 1
 jr c, CARMOVE_PITOUT_ENGREP
 inc HL
 jr z, CARMOVE_PITOUT_WINGREP
 inc HL
 cp 3
 jr c, CARMOVE_PITOUT_BRSUREP
 inc HL
 jp z, CARMOVE_PITOUT_BRSUREP
 inc HL
CARMOVE_PITOUT_FUELREP:
 ld A, (HL)
 or A
 jp z, CARMOVE_PITOUT_REPNEXT
 call sub_pit_count
 ld HL, pit_fuel1
 ld DE, 1c4bh
 ld B, 6
 ld A, 2
 call grp_set_A
 ld HL, pit_fuel2
 ld DE, 244bh
 ld B, 6
 ld A, 2
 call grp_set_A
 jp CARMOVE_PITOUT_REP_END
CARMOVE_PITOUT_ENGREP:
 ld A, (HL)
 or A
 jp z, CARMOVE_PITOUT_REPNEXT
 call sub_pit_count
 ld DE, 1e49h
 ld HL, pit_engine_r
 ld A, 2
 call grp_set_A
 jp CARMOVE_PITOUT_REP_END
CARMOVE_PITOUT_WINGREP:
 ld A, (HL)
 or A
 jp z, CARMOVE_PITOUT_REPNEXT
 push AF
 call sub_pit_count
 pop AF
 ld HL, 1845h
 ld D, A
 ld E, 0
 sbc HL, DE
 ex DE, HL
 ld HL, pit_wing_u
 ld A, 2
 call grp_set_A
 ;jr CARMOVE_PITOUT_REPNEXT
 jp CARMOVE_PITOUT_REP_END
CARMOVE_PITOUT_BRSUREP:
 ld (CARMOVE_PITOUT_NEXTCH_HL + 1), HL
 ;call sub_pit_count

 ld HL, set_pit_rand_values
 ld A, (HL)
 or A
 jr z, CARMOVE_PITOUT_BRSUREP2
 dec (HL)
 ld HL, pit_engine_l
 ld DE, 1c40h
 ld B, 6
 ld A, 2
 call grp_set_A

CARMOVE_PITOUT_BRSUREP2:
 ld HL, set_pit_rand_values + 1
 ld A, (HL)
 or A
 jr z, CARMOVE_PITOUT_BRSUREP3
 dec (HL)
 ld HL, pit_engine_r
 ld DE, 1c4ah
 ld B, 6
 ld A, 2
 call grp_set_A

CARMOVE_PITOUT_BRSUREP3:
 ld HL, set_pit_rand_values + 2
 ld A, (HL)
 or A
 jr z, CARMOVE_PITOUT_BRSUREP4
 dec (HL)
 ld HL, pit_engine_l
 ld DE, 2140h
 ld B, 6
 ld A, 2
 call grp_set_A

CARMOVE_PITOUT_BRSUREP4:
 ld HL, set_pit_rand_values + 3
 ld A, (HL)
 or A
 jr z, CARMOVE_PITOUT_NEXTCH
 dec (HL)

 ld HL, pit_engine_r
 ld DE, 214ah
 ld B, 6
 ld A, 2
 call grp_set_A
 jp CARMOVE_PITOUT_REP_END

;タイヤ、足回り変数チェック
CARMOVE_PITOUT_NEXTCH:
 ld HL, set_pit_rand_values
 ld E, (HL)
 inc HL
 ld D, (HL)
 inc HL
 ld C, (HL)
 inc HL
 ld B, (HL)
 ex DE, HL
 add HL, BC
 jp nz, CARMOVE_PITOUT_REP_END
 xor A
CARMOVE_PITOUT_NEXTCH_HL:
 ld HL, overwrite
 ld (HL), A
 ;jp CARMOVE_PITOUT_REPNEXT

CARMOVE_PITOUT_REPNEXT:
 ld A, (CARMOVE_PITOUT_REP_INDEX)
 inc A
 call set_pit_rand_value
 cp 5
 jr c, CARMOVE_PITOUT_REPNEXT_2
 xor A
 ld (car_pitin), A
CARMOVE_PITOUT_REPNEXT_2:
 ld (CARMOVE_PITOUT_REP_INDEX), A
CARMOVE_PITOUT_REP_END:
 call TIM3
 ret

set_pit_rand_values:
 db overwrite
 db overwrite
 db overwrite
 db overwrite
set_pit_rand_value:
 cp 2
 ret c
 cp 4
 ret nc
 push AF
 inc HL
 ld A, (HL) ;基本タイヤ、足回りピット時間
 or A
 jr z, set_pit_rand_value_E
 ld HL, set_pit_rand_values
 ld C, A
 ld E, 4  ;繰り返し人数
set_pit_rand_value_lp:
 call RAND4
 add A, C ;基本タイヤ、足回りピット時間 + RAND4
 ld B, A
 xor A
set_pit_rand_value_lp2:
 add A, 16  ;追加秒数
 djnz set_pit_rand_value_lp2
 ld (HL), A
 inc HL
 dec E
 jr nz, set_pit_rand_value_lp
set_pit_rand_value_E:
 pop AF
 ret

sub_pit_count:;HL=CARMOVE_PITOUT_REP_SEC++ 
 ld A, (CARMOVE_PITOUT_REP_A)
 inc A
 ld (CARMOVE_PITOUT_REP_A), A
 cp 30
 ret c
 dec (HL) ;ルーチン外部で0判定する
 xor A
 ld (CARMOVE_PITOUT_REP_A), A
 ret

CARMOVE_PITOUT_REP_INDEX:
 db overwrite
CARMOVE_PITOUT_REP_SEC:
; エンジン系 ウィング ブレーキサス タイヤ
 db overwrite, overwrite, overwrite, overwrite
CARMOVE_FUEL_REP_SEC:
 db overwrite
CARMOVE_PITOUT_REP_A:
 db overwrite
;pit_engine_l:
; db 10h, 14h, 1eh, 07h, 0bh, 14h
;pit_fuel1:
; db 8, 8, 8, 7fh, 0ffh, 0
;pit_fuel2:
; db 0, 0, 1eh, 1eh, 1fh, 0
;pit_wing_u:
; db 40h, 60h, 7fh, 67h, 60h, 40h
;pit_wing_d:
; db 1, 3, 3, 7fh, 73h, 1
;pit_tire_l_1:
; db 0, 20h, 3Fh, 3fh, 14h, 14h
;pit_tire_l_2:
; db 0, 20h, 3Fh, 3fh, 6, 0
;pit_tire_l_3:
; db 0, 3ch, 3Fh, 3fh, 0, 0
;pit_tire_r_1: ;反転してもいいけれど...
; db 14h, 14h, 3fh, 3fh, 20h, 0
;pit_tire_r_2:
; db 0, 6, 3fh, 3fh, 20h, 0
;pit_tire_r_3:
; db 0, 0, 3fh, 3fh, 3ch, 0
;--------- コンディション/シュウリ
CARMOVE_CONDITION:
 call cls
 ld HL, CARMOVE_PITIN_COND_MES
 ld DE, 0001h
 ld A, 2
 ld C, 6
 call show_block_mes

 ;故障表示
CARMOVE_CONDITION_BREAKVW:
 xor A ;インデックス
 ld HL, CARMOVE_BREAK_ADDRESS ;故障をチェックするアドレス
CARMOVE_CONDITION_BREAKLB:
 ld E, (HL)
 inc HL
 ld D, (HL)
 inc HL
 push HL
 ex DE, HL
 ld B, (HL) ;B = ステータス
 push AF    ;AF = インデックス
 ld A, B
 and 11111000b
 ld HL, CARMOVE_PITIN_COND_OK
 jr z, CARMOVE_CONDITION_BREAKOK
 ld HL, CARMOVE_PITIN_COND_NG
 jr CARMOVE_CONDITION_BREAKON
CARMOVE_CONDITION_BREAKOK:
 pop AF
 push AF
 ld DE, CARMOVE_REPAIR  ;修理することを示すアドレス
 ex DE, HL
 ld C, A
 ld B, 0
 add HL, BC
 set 7, (HL)
 ex DE, HL
CARMOVE_CONDITION_BREAKON:
 pop AF
 ld C, A
 push AF
 push HL
 ex AF, AF'
 ld HL, CARMOVE_REPAIR
 ld B, 0
 add HL, BC
 ld A, (HL)
 bit 0, A
 pop HL
 jr z, CARMOVE_CONDITION_BREAKCH
 ld HL, CARMOVE_PITIN_COND_RP
CARMOVE_CONDITION_BREAKCH:
 ex AF, AF'
 call get_condDE
 inc E
 ld B, 2
 call msp
 pop AF
 pop HL
 inc A
 cp 6
 jr z, CARMOVE_CONDITION_SKIP
 cp 8
 jr nz, CARMOVE_CONDITION_BREAKLB
 jr CARMOVE_CONDITION_TIRE
CARMOVE_CONDITION_SKIP:
 inc HL
 inc HL
 inc A
 jr CARMOVE_CONDITION_BREAKLB

CARMOVE_CONDITION_TIRE:
;タイヤ
 ld HL, (car_tire_rest_st)
 ld DE, tire_rest_010
 or A
 ld B, 255
CARMOVE_CONDITION_TIRELP:
 inc B
 sbc HL, DE
 jr nc, CARMOVE_CONDITION_TIRELP
 ld A, B
 cp 10
 jp c, CARMOVE_CONDITION_TIRE2
 ld A, 9
CARMOVE_CONDITION_TIRE2:
 add A, 30h
 ld DE, 0402h
 call 0be62h  ;◯ワリ

 ld A, (car_fuel_st + 1)
 ld L, A
 ld H, 0
 call ASCN
 ld HL, NMEMRY + 2
 ld B, 3
 ld DE, 0502h
 call msp     ;〇〇〇

CARMOVE_CONDITION_LP:
 ld A, (CARMOVE_CONDITION_PREV)
 call get_condDE
 call prompt_delete

 ld A, (CARMOVE_CONDITION_INDEX)
 call get_condDE
 call prompt_r_view
CARMOVE_CONDITION_MLP:
 call kscan_ex
 jr z, CARMOVE_CONDITION_MLP
 call keysc
 ld B, A
 ld A, (CARMOVE_CONDITION_INDEX)
 ld (CARMOVE_CONDITION_PREV), A
 bit 0, B
 jr nz, CARMOVE_CONDITION_LEFT
 bit 1, B
 jr nz, CARMOVE_CONDITION_RIGHT
 bit 3, B
 jr nz, CARMOVE_CONDITION_DONE
 bit 6, B
 jr nz, CARMOVE_CONDITION_DOWN
 bit 7, B
 jr nz, CARMOVE_CONDITION_UP
 jr CARMOVE_CONDITION_MLP

CARMOVE_CONDITION_DONE:
 cp 9
 jp z, CARMOVE_PITIN_MENU_DRAW
 cp 6
 jp z, CARMOVE_CONDITION_TIRECH
 cp 8
 jp z, CARMOVE_CONDITION_FUELCH
CARMOVE_CONDITION_DONE_COM:
 ld HL, CARMOVE_REPAIR
 ld C, A
 ld B, 0
 add HL, BC
 ld A, (HL)
 and 10000001b
 jp m, CARMOVE_CONDITION_BREAKVW
 xor 1
 ld (HL), A
 jp CARMOVE_CONDITION_BREAKVW
CARMOVE_CONDITION_TIRECH:
;car_tire_st
 call tire_setting
 jp z, CARMOVE_CONDITION  ;キャンセル
 ld (car_tire_st), A
 ld HL, tire_restmax
 ld (car_tire_rest_st), HL
 call show_done_setting
 ld A, 1
 ld (CARMOVE_REPAIR + 6), A
 jp CARMOVE_CONDITION

CARMOVE_CONDITION_FUELCH:
 ld A, (car_fuel_st + 1)
 cp 255
 jp z, CARMOVE_CONDITION  ;満タン
 ld C, A
 call chage_fuel
 cp 255
 jp z, CARMOVE_CONDITION  ;キャンセル
 call show_done_setting
 ld A, 1
 ld (CARMOVE_REPAIR + 8), A
 jp CARMOVE_CONDITION

CARMOVE_CONDITION_LEFT:
 dec A
 jp m, CARMOVE_CONDITION_MLP
 jr CARMOVE_CONDITION_CME
CARMOVE_CONDITION_RIGHT:
 inc A
 cp 10
 jp z, CARMOVE_CONDITION_MLP
 jr CARMOVE_CONDITION_CME
CARMOVE_CONDITION_DOWN:
 inc A
 inc A
 cp 10
 jp c, CARMOVE_CONDITION_CME
 jp CARMOVE_CONDITION_MLP
CARMOVE_CONDITION_UP:
 dec A
 dec A
 jp p, CARMOVE_CONDITION_CME
 jp CARMOVE_CONDITION_MLP

CARMOVE_CONDITION_CME:
 ld (CARMOVE_CONDITION_INDEX), A
 jp CARMOVE_CONDITION_LP
 
CARMOVE_CONDITION_INDEX:
 db overwrite
CARMOVE_CONDITION_PREV:
 db overwrite

get_condDE:
 push AF
 ld B, A
 and 1
 jr z, get_condDE_E
 add A, 9
get_condDE_E:
 inc A
 ld E, A  ;x
 ld A, B
 srl A
 inc A
 ld D, A  ;y
 pop AF
 ret
CARMOVE_PITIN_COND_MES:
 db 20, "ｺﾝﾃﾞｨｼｮﾝ ｶｸﾆﾝ & ｼｭｳﾘ"
 db 18, "OK:ｴﾝｼﾞﾝ  OK:ｳｨﾝｸﾞ"
 db 19, "OK:ﾗｼﾞｴｰﾀ OK:ﾃﾞﾝｷｹｲ"
 db 18, "OK:ﾌﾞﾚｰｷ  OK:ﾐｯｼｮﾝ"
 db 21, "9ﾜﾘ:ﾀｲﾔ   OK:ｻｽﾍﾟﾝｼｮﾝ"
 db 13, "   :ｶﾞｿﾘﾝ ｵﾜﾙ"
CARMOVE_PITIN_COND_NG:
 db "NG"
CARMOVE_PITIN_COND_OK:
 db "OK"
CARMOVE_PITIN_COND_RP:
 db "RP"
 
CARMOVE_BREAK_ADDRESS:
 dw car_engine_st, car_wing_st, car_radiator_st, car_elect_st, car_brake_st
 dw car_transmission_st, car_tire_rest_st, car_suspension_st, car_fuel_st
CARMOVE_REPAIR:
;修理するかしないか 並びは上の故障と同じ
;直す場合、修理アニメの長さをを下の数(下位5ビット)*2*fps分で乱数を出す。
;上位3ビットは故障作業アニメの種類
;故障作業アニメグループ
;0：エンジン、ラジエータ、ミッション、デンキケイ
;20h：ウィング
;40h：ブレーキ、サスペンション
;60h：タイヤ
;80h：ガソリン
 db overwrite, overwrite, overwrite, overwrite, overwrite
 db overwrite, overwrite, overwrite, overwrite
CARMOVE_REPAIR_TIME:
 db 18h, 3, 5h, 5, 4
 db 8h, 6, 4, 80h;80hのガソリンはFUEL_REPの値を読み込む
;-----　リタイア -----
CARMOVE_RETIRE:
 ld HL, (retstack)
 ld SP, HL
 ld A, (game_mode)
 cp 1
 jr nz, CARMOVE_RITIRE2 ;スプリント ;タイキュウ
 jp start ;フリー走行

CARMOVE_RITIRE2:
 ld A, 4
 ld HL, rank
 ld (HL), A
 inc HL
 push HL
 xor A
 ld (HL), A
 inc HL
 ld (HL), A
 inc HL
 ld (HL), A
 pop HL
 ld B, 3
CARMOVE_RITIRE2_rnd:
 call RAND4
 cp 3
 jr z, CARMOVE_RITIRE2_rnd
 inc A
 call CARMOVE_RETIRE_RANKCKECK
 jr z, CARMOVE_RITIRE2_rnd
 ld (HL), A
 inc HL
 djnz CARMOVE_RITIRE2_rnd
 jp result_stack

CARMOVE_RETIRE_RANKCKECK:
 push HL
 push BC
 ld HL, rank + 1
 ld BC, 3
 cpir
 pop BC
 pop HL
 ret

CARMOVE_PITIN_STR:
 ld (HL), A
 jp CARMOVE_PITIN_PROMPT

CARMOVE_PITIN_INDEX:
 db overwrite
CARMOVE_PITIN_PREV:
 db overwrite
CARMOVE_PITIN_MES:
 db 19, "ﾉｺﾘ   LAP ｼﾞｭﾝｲ   ｲ"
 db 8, "ﾋﾟｯﾄ ｱｳﾄ"
 db 15, "ｺﾝﾃﾞｨｼｮﾝ / ｼｭｳﾘ"
 db 4, "ﾘﾀｲｱ"
CARMOVE_PITIN_MES2:
 db " ﾌﾘｰｿｳｺｳ  ﾋﾟｯﾄｲﾝ   "

;ブロック文字列化メッセージ表示ループ
show_block_mes:;HL=文字ブロック先頭アドレス A=指定列 C=終了行
 ld (show_block_mes_E + 1), A
 ld A, C
 ld (show_block_mes_C + 1), A
show_block_mes_lp:
 ld B, (HL)
 inc HL
 call msp
 inc HL
 inc D
show_block_mes_E:
 ld E, overwrite
 ld A, D
show_block_mes_C:
 cp overwrite
 jr nz, show_block_mes_lp
 ret
 

;------------ 敵車とポイントの接触判定 -------------
;接触している番号をCARMOVE_NOW_POINTに落とす
;目指すスピードを落とす
;次に目指す座標を落とす
CARMOVE_PNT_INDEX:	;一時的に使う 判定中のインデックス
 db overwrite
CARMOVE_POINT:
 ;ld HL, CARMOVE_NOW_POINT
 ;add HL, BC
 ld A, (IX + now_point)
 ld (CARMOVE_PNT_INDEX), A
 ld E, A
 ld D, 0
 ld HL, objlist_points
 ld A, 5
 call xsan
 push HL
 
 exx
 ld HL, 0
 ld (check_collision_BC + 1), HL
 exx

 ld A, 7	;補正値
 ;call log_A_kwait
CARMOVE_POINT_DE1:
 ld DE, (car_pos_x_crnt)
 call check_collision	;現在ポジション - ポイントポジション
 jr c, CARMOVE_POINT_E
 exx
 ld HL, (block_action_posDE + 1)
 ld (block_action_x), HL
 exx

 ld A, 10	;補正値
 ;call log_A_kwait
CARMOVE_POINT_DE2:
 ld DE, (car_pos_y_crnt)
 call check_collision
 jr c, CARMOVE_POINT_E
 exx
 ld HL, (block_action_posDE + 1)
 ld (block_action_y), HL

 ld HL, (block_action_x)
 ld (check_collision_BC + 1), HL
 exx
 ld A, 4	;補正値
 ;call log_A_kwait
 ld HL, CARMOVE_POINT_RECT
CARMOVE_POINT_DE3:
 ld DE, (car_pos_x_crnt)
 call check_collision
 jr nc, CARMOVE_POINT_E
 
 exx
 ld HL, (block_action_y)
 ld (check_collision_BC + 1), HL
 exx
 ld A, 4	;補正値
 ;call log_A_kwait
CARMOVE_POINT_DE4:
 ld DE, (car_pos_y_crnt)
 call check_collision
 jr c, CARMOVE_POINT_COLLIDED

;--------- ループ時の処理 ------------
CARMOVE_POINT_E:
 pop HL
 ret

;---------- 接触時の処理 ------------
CARMOVE_POINT_COLLIDED:
 ;ld A, (objno)
 ;ld E, A
 ;ld D, 0

 ;call log_A
 pop HL
 inc HL
 inc HL
 inc HL
 inc HL
 ld A, (HL)	;Speed
 push HL
 ;call log_A_kwait
 ;ld HL, CARMOVE_COMPUTER_SPD
 ;add HL, DE
 ;call log_HL_kwait
 ld (IX + computer_spd), A
 
 ld A, (objlist_point_num)
 ld C, A
 ld A, (CARMOVE_PNT_INDEX)
 inc A
 cp C
 jr z, CARMOVE_POINT_LAP  ;最後のポイントにふれると周回処理
 call CARMOVE_POINT_PITIN ;最後から3番目のポイントにふれているか見て、ピットイン開始評価->ポイント変更を行う
 jr CARMOVE_POINT_STRIDX ;それ以外
;最後のポイントに触れると周回処理をする。
CARMOVE_POINT_LAP:
 call CARMOVE_POINT_LAP_EXEC
CARMOVE_POINT_LAPE:
 xor A
CARMOVE_POINT_STRIDX:
 ld (IX + now_point), A		;次のポイントインデックスを保存

 pop HL
 ;ld A, E
 ;add A, A
 ;add A, A
 ;ld E, A
 ;push DE	;* 4

 inc HL		;次のポイントアドレス
 ld A, (HL)
 cp 0ffh	;endsign
 jr nz, CARMOVE_POINT_NENDSIGN
CARMOVE_POINT_PITIN_JP:
 ld HL, objlist_points
CARMOVE_POINT_NENDSIGN:
 ld E, (HL)
 inc HL
 ld D, (HL)
 inc HL
 ex DE, HL
 ld (CARMOVE_POINT_DE5 + 1), HL
 ex DE, HL

 ld E, (HL)
 inc HL
 ld D, (HL)
 inc HL
 ex DE, HL
 ld (CARMOVE_POINT_DE6 + 1), HL
 ex DE, HL

 ld HL, (CARMOVE_COMPUTER_NEXT_crnt)
 ;pop DE
 ;add HL, DE	;4byte 次の座標をストアする先
CARMOVE_POINT_DE5:
 ld DE, overwrite	;x次の目指すポイント
 ld (HL), E
 inc HL
 ld (HL), D
 inc HL
CARMOVE_POINT_DE6:
 ld DE, overwrite	;y次の目指すポイント
 ld (HL), E
 inc HL
 ld (HL), D
 ret

CARMOVE_POINT_RECT:
 dw 13, 13

CARMOVE_POINT_LAP_EXEC:
 ld A, (IX + lp_end)
 or A
 ret nz

 ld HL, lap_remind
 ld A, (objno)
 call HL_ADD_A
 ;ld (CARMOVE_POINT_LAP_EXEC_HL + 1), HL
 ld A, (HL)
 or A
 ld A, 0ffh;	(ゴール済み)
 jr z, CARMOVE_POINT_LAP_EXEC_RACEEND
 dec (HL)
 ld A, (objno)
 call regist_position
CARMOVE_POINT_LAP_EXEC_RACEEND:
 ld A, (lap_end)
 or A
 ret z
;CARMOVE_POINT_LAP_EXEC_HL:
 ;ld HL, overwrite
 ld (IX + lp_end), A
 ret

CARMOVE_POINT_PITIN:;A=CARMOVE_PNT_INDEX + 1 HL=CARMOVE_NOW_POINT++ C=(objlist_point_num)
 push AF
 inc A
 inc A
 cp C
 jr nz, CARMOVE_POINT_PITIN_E;最後から3番目以外
 ld HL, lap_remind
 ld A, (objno)
 call HL_ADD_A
 ld A, (HL)
 cp 2
 jr c, CARMOVE_POINT_PITIN_E;ラスト1周またはウィニングランではピットインしない
;CARMOVE_NOW_POINTを先頭のポイントにして、CPUのピットインフラグをセットする。
;g_count_nをチェック
 ld HL, (g_count_crnt)
 inc HL
 ld A, (HL) ;HL=g_count_n + 1
 cp cpu_pit_g_count ;g_countがここれ以上だとCPUのタイヤが減っていることにする
 jr c, CARMOVE_POINT_PITIN_E
 xor A
 ld (HL), A ;(g_count_n + 1)リセット

 ld A, 1
 ld (IX + auto_pitin_flag), A ;0以外の値をセット
 pop AF
 pop HL
 pop HL

 xor A
 ld (IX + now_point), A ;次のポイントをコース先頭のものにする
 ld (IX + dir), A ;向きリセット
 ld (IX + speed), A ;スピードリセット
 ld (IX + acceleration), A ;加速度リセット
 ld A, E
 add A, A
 add A, A
 ld E, A
 jp CARMOVE_POINT_PITIN_JP
CARMOVE_POINT_PITIN_E:
 pop AF
 ret

;2(y, x)の並び。奇数の値は2回に一回加算される。
CARMOVE_ADD_DIRECTION_VALUE_TABLE:
 dw 0fc00h,0fc01h,0fc02h,0fc03h,0fc04h,0fd04h,0fe04h,0ff04h,0004h,0104h,0204h,0304h,0404h,0403h,0402h,0401h
 dw 0400h,04ffh,04feh,04fdh,04fch,03fch,02fch,01fch,00fch,0fffch,0fefch,0fdfch,0fcfch,0fcfdh,0fcfeh,0fcffh


;lap_end: ;fps分ループさせて、fps>=(lap_end)かつ敵車がラップする、でレース終了 1:ゴール 2:画面外に自車が消える
; db overwrite
race_end:
 call regist_position
 call lap_info
 ld A, 1
; ld (race_ended), A
 ld (lap_end), A
;
 ret

;順位確定 全車共通 各車ラップ時に呼ばれる
regist_position:;A=carno HL=address
 push HL
 push DE
 ld E, A
 ld D, 0
 push DE
 ld A, (HL)
 ld HL, lap_remind
 ld E, 0  ;E=前に何台居るか 自車も含む
 ld B, 4
regist_position_lp:
 ld C, (HL)
 cp C	;A=自身の残り周回数 > C=比べる残り周回数 -> 順位加算
 jr c, regist_position_djnz
 inc E     ;A(自車残り) >= C(敵車残り)で前にいる台数を加算
regist_position_djnz:
 inc HL
 djnz regist_position_lp
 pop BC
 ld HL, rank
 add HL, BC
 ld (HL), E
 ld A, (HL)
 pop DE
 pop HL
 ret

;周回時の情報表示
lap_info:
 ld A, 190
 ld (SET_PIT_SIGN_TIMER), A
 call fram_clear_sign
 
 ld A, (game_mode)
 cp 1
 jr z, get_time_ldir
 
 ld A, (rank)
 call get_num_chara_drct
 ld BC, 6
 ld DE, sign_fram_0 + 7
 ldir
 ld A, (lap_remind)
 ld L, A
 ld H, 0
 call ASCN
 ld A, (NMEMRY + 3)
 call get_num_chara
 ld BC, 6
 ld DE, sign_fram_0 + 7 * 3
 ldir
 ld A, (NMEMRY + 4)
 call get_num_chara
 ld BC, 6
 ld DE, sign_fram_0 + 7 * 4
 ldir

get_time_ldir:
 call calc_lap_time

 ld HL, SAVE_RECORD
 ld A, (cource_current)
 sla A
 call HL_ADD_A
 ld (get_time_ldir_HL_RECORD + 1), HL
 ld E, (HL)
 inc HL
 ld D, (HL)	;DE = (SAVE_RECORD ++)
 ld HL, (calc_lap_time_3b)
 or A
 call cp_hl_de
 jr nc, get_time_ldir2
get_time_ldir_HL_RECORD:
 ld (overwrite), HL	;レコード更新
get_time_ldir2:
 ld B, 4
 ld HL, NMEMRY + 1
 exx
 ld DE, sign_fram_1 ;;;
 exx
get_time_ldir_l:
 ld A, (HL)
 exx
 call get_num_chara ;;;
 ld BC, 6           ;;;
 ldir               ;;;
 inc DE
 exx
 inc HL
 djnz get_time_ldir_l
 
 ld A, (car_fuel_st + 1)
 cp 30  ;30L以下
 jr nc, get_time_ldir_t
 ld HL, lap_F
 ld BC, 6
 ld DE, sign_fram_1 + 28
 ldir
get_time_ldir_t:
 ld A, (car_tire_rest_st + 1)
 cp tire_consumption  ;タイヤ4割以下
 ret nc
 ld HL, lap_T
 ld BC, 6
 ld DE, sign_fram_1 + 28
 ldir
 ret

get_num_chara:;A=数字(ASCII)
 or A
 jr z, get_num_chara_drct
 sub 30h
get_num_chara_drct:;A=数字
 ld HL, num_charas
 or A
 ret z
 add A, A
 ld B, A
 add A, A
 add A, B ;A*6
 call HL_ADD_A
 ret


;ラップタイム取得
calc_lap_time_3b:
 dw overwrite
calc_lap_time:;
 ld HL, (lap_time)
 push HL
 add HL, HL
 pop DE
 add HL, DE ;HL * 3
 ld (calc_lap_time_3b), HL
 call ASCN
 ld HL, lap_time
 xor A
 ld (HL), A
 inc HL
 ld (HL), A
 ret

;------------ 背景移動 ------------
BGMOVE:	;CARMOVEの結果からスクロール幅を決める
 ld A, (objno)
 or A
 ret nz
 ld A, (lap_end)
 or A
 ret nz

 ld HL, (car_pos_x)
 ld DE, x0
 sbc HL, DE
 jr nc, BGMOVE_X_MAX
 ld A, L
 ld HL, 0
 jr BGMOVE_X
BGMOVE_X_MAX:
 ld DE, overwrite
 or A
 sbc HL, DE
 jr c, BGMOVE_X_3
 ld A, L
 ex DE, HL
 jr BGMOVE_X
BGMOVE_X_3:
 add HL, DE
BGMOVE_X:
 ld (car_pos_mod_x), A
 ld (scroll_vec + 2), HL
 
 ld HL, (car_pos_y)
 ld A, H
 cp 255
 jr z, BGMOVE_Y_FFXX
BGMOVE_YHCHECK:
 xor A
 ld DE, y0
 sbc HL, DE
 jr nc, BGMOVE_Y_MAX
 ld A, L			;y-補正
 ld HL, 0
 jr BGMOVE_Y
BGMOVE_Y_MAX:
 ld DE, overwrite
 or A
 sbc HL, DE
 jr c, BGMOVE_Y_3
 ld A, L
 ex DE, HL
 jr BGMOVE_Y
BGMOVE_Y_3:
 add HL, DE
BGMOVE_Y:
 ld (car_pos_mod_y), A
 ld (scroll_vec), HL
 ret

BGMOVE_Y_FFXX:
 or A
 ld DE, y0
 sbc HL, DE
 ld A, L
 ld HL, 0
 jr BGMOVE_Y

BGMOVE_CORRECTION:
 ld A, (lap_end)
 or A
 ret nz
 ld A, (objno)
 or A
 ret nz
 call BGMOVE_CORRECTION_Y
 call BGMOVE_CORRECTION_X
 ld A, (reserved_0)		;y分のBG位置進度
 ;call log_A_kwait
 ld C, A
 ld A, (reserved_1)		;x分のBG位置進度
 ;call log_A_kwait
 add A, C
 ld E, A					;BG進度 更新値
 ld A, (current_bg_index)	;BG進度 前回値
 ld B, A					;B=前回値
 ld A, E					;A=更新値
 ld (current_bg_index), A	;更新値代入
 cp B
 ret z

 ld L, B
 ld H, 0
 ld A, (cource_size + 1)
 call WSAN
 ld B, L					;前回値あまり x
 ld C, A					;前回値割った結果 y
 
 ld L, E
 ld H, 0
 ld A, (cource_size + 1)
 call WSAN
 ld D, L					;更新値あまり x
 ld E, A					;更新値割った結果 y
 
 ld A, B
 sub D
 ld H, A
 ld A, C
 sub E
 ld L, A
 ld A, (cource_inited)
 or A
 jr nz, BGMOVE_CORRECTION_COM
 ld A, 1
 ld (cource_inited), A
 jp BGMOVE_BGLDIS		;current_bg_indexが右に移動

BGMOVE_CORRECTION_COM:
 inc H						;更新値が1大きい
 jp z, BGMOVE_BGLDIS		;current_bg_indexが右に移動
 dec H
 dec H						;更新値が1小さい
 jp z, BGMOVE_BGLDIS		;current_bg_indexが左に移動
 inc L
 jp z, BGMOVE_BGLDIS		;current_bg_indexが上に移動
 dec L
 dec L
 jp z, BGMOVE_BGLDIS		;current_bg_indexが下に移動
 ret

;BGMOVE_BGLDIS_left:
; ld A, 2
;call log_A_kwait
; jp BGMOVE_BGLDIS
;BGMOVE_BGLDIS_right:
; ld A, 3
;call log_A_kwait
; jp BGMOVE_BGLDIS
;BGMOVE_BGLDIS_up:
; ld A, 0
;call log_A_kwait
; jp BGMOVE_BGLDIS
;BGMOVE_BGLDIS_down:
; ld A, 1
;call log_A_kwait
; jp BGMOVE_BGLDIS

BGMOVE_CORRECTION_Y:
 ld A, (cource_size + 1)
 ld (BGMOVE_CORRECTION_Y_1_add + 1), A
 ex AF, AF'
 exx
 ld C, 0
 xor A
 exx
 ex AF, AF'
 ld HL, (scroll_vec)
 ld DE, ydots
BGMOVE_CORRECTION_Y_1:
 or A
 sbc HL, DE
 jr c, BGMOVE_CORRECTION_Y_2
 ex AF, AF'
 exx
 ld C, A
BGMOVE_CORRECTION_Y_1_add:
 add A, overwrite	;一画面下のFRAMは5つ進む。
 ld C, A         	;SET_BG系メソッドでの入力がcource_sizeに準ずるので
 exx
 ex AF, AF'      	;cource_size_x分は足す必要あり
 jr BGMOVE_CORRECTION_Y_1
BGMOVE_CORRECTION_Y_2:;HL = (scroll_vec) % ydots
 add HL, DE
 exx
 ld A, C			;現在のFRAM_BG位置を求めて補正
 ld (reserved_0), A	;y分のBG位置進度
 exx
 
 ld A, 8
 call WSAN              ;A = 0 ~ 5 HL =0 ~ 7
 ld C, L
BGMOVE_CORRECTION_Y_4_end:
 ld (scroll_block), A	;yのブロック補正位置
 ret

BGMOVE_CORRECTION_X:
 ld HL, (scroll_vec + 2)
 ld C, 0
 ld DE, 144
BGMOVE_CORRECTION_X_1:
 or A
 sbc HL, DE
 jr c, BGMOVE_CORRECTION_X_2
 inc C				;一画面横のFRAMは1つ進む
 jr BGMOVE_CORRECTION_X_1
BGMOVE_CORRECTION_X_2:
 add HL, DE
 					;HL = (scroll_vec + 2) % 144
 					;C = (scroll_vec + 2) / 144
 ld A, C			;現在のFRAM_BG位置を求めて補正
 ld (reserved_1), A	;x分のBG位置進度
 ld A, L
 ld (scroll_pos_x), A	;xの描画開始位置
 ret

BGMOVE_BGLDIS:;自機が右に移動して、BGを左に転送
 ld A, (current_bg_index)
 ld HL, (cource_arrangement_address)
 ld E, A
 ld D, 0
 add HL, DE
 ld A, (HL)
BGMOVE_BGLDIS_2_HL:
 ld HL, overwrite
 call BGUNZIP_GET_BG
 ld E, (HL)
 inc HL
 ld D, (HL)
 ex DE, HL
 ld DE, fram_0
 call unzip
 
 ld A, (current_bg_index)
 inc A
 ld E, A
 ld A, (bg_sizetotal)
 sub E
 jr z, BGMOVE_BGLDIS_4
 jr c, BGMOVE_BGLDIS_4

 ld HL, (cource_arrangement_address)
 ld D, 0
 add HL, DE
 ld A, (HL)
BGMOVE_BGLDIS_3_HL:
 ld HL, overwrite
 call BGUNZIP_GET_BG
 ld E, (HL)
 inc HL
 ld D, (HL)
 ex DE, HL
 ld DE, fram_1
 call unzip
BGMOVE_BGLDIS_4:
 ld A, (current_bg_index)
 ld C, A
 ld A, (cource_size + 1)
 add A, C
 ld E, A
 ld A, (bg_sizetotal)
 sub E
 jr z, BGMOVE_BGLDIS_5
 jr c, BGMOVE_BGLDIS_5

 ld HL, (cource_arrangement_address)
 ld D, 0
 add HL, DE
 ld A, (HL)
BGMOVE_BGLDIS_4_HL:
 ld HL, overwrite
 call BGUNZIP_GET_BG
 ld E, (HL)
 inc HL
 ld D, (HL)
 ex DE, HL
 ld DE, fram_2
 call unzip

BGMOVE_BGLDIS_5:
 ld A, (current_bg_index)
 inc A
 ld C, A
 ld A, (cource_size + 1)
 add A, C
 ld E, A
 ld A, (bg_sizetotal)
 sub E
 ret c
 ret z

 ld HL, (cource_arrangement_address)
 ld D, 0
 add HL, DE
 ld A, (HL)
BGMOVE_BGLDIS_5_HL:
 ld HL, overwrite
 call BGUNZIP_GET_BG
 ld E, (HL)
 inc HL
 ld D, (HL)
 ex DE, HL
 ld DE, fram_3
 call unzip
 ret

;------- グラフィックの描画 -------
SET_SPRITE:
 ld A, (objno)
 or A
 jp nz, SET_SPRITE_ENEMY
 ld A, (cross_be)
 cp 5
 ret z
SET_SPRITE_LP:
 ld A, 1
 ld (read_lcd_A + 1), A ;芝判定フラグリセット

 ld HL, car0
 ld DE, 24
 ld A, (car_crash_fl)
 cp 32	;クラッシュ
 jp z, SET_SPRITE_CRASH
 ld A, (car_dir)
 ld B, A
 ld A, (car_degree + 1)
 add A, B
 jp p, SET_SPRITE_DEGP
 add A, 32
SET_SPRITE_DEGP:
 cp 32
 jr c, SET_SPRITE_DEGC
 sub 32
SET_SPRITE_DEGC:
 srl A
 call XSAN
 
car_sprite_fix:
 ld A, overwrite					;スクロール余り幅 0 ~ 7
 ld D, A
 ld A, (car_pos_mod_y)
 add A, D
 add A, y0
 push AF
 ld D, A
 
 ld A, (car_pos_mod_x)
 ld E, x0
 add A, E
 ld E, A
 ld B, 12
 ld A, (on_sand)
 call grp_set_A		;HL_data,DE_(y,x),B_databit
 
 pop AF
 ld D, A
 push AF
 add A, 8
 ld D, A
 
 ld A, (car_pos_mod_x)            ;IXに入れ替える
 ld E, x0
 add A, E
 ld E, A
 ld B, 12
 ld A, (on_sand)
 call grp_set_A		;HL_data,DE_(y,x),B_databit

 xor A
 ld (read_lcd_A + 1), A ;芝判定フラグリセット

 pop DE ;D=上の座標

 ld A, (lap_end)
 or A
 jr nz, car_sprite_raceend
 cp 2 ;画面外に自車が消えている
 ret z

 ld A, (car_tire_rest_st + 1)
 cp tire_consumption
 jr nc, car_sprite_fix_slip
 ld A, (car_tire_rest_st)
 and 3
 jr nz, car_sprite_fix_slip
 ld A, (keysc_key)
 and 00000011b
 jr nz, car_sprite_fix_slip2
car_sprite_fix_slip:
 ld A, (car_slip_x)
 or A
 ret z

car_sprite_fix_slip2:
 ld A, (car_pos_mod_x)            ;IXに入れ替える
 ld E, x0
 add A, E
 ld E, A    ;左の座標
 push DE
 ld A, (car_dir)
 ;srl A
 ;sla A
 res 0, A
 ld HL, SET_SPRITE_SLIP_POSL
 ;ld E, A
 ;ld D, 0
 ;add HL, DE
 call HL_ADD_A
 ld D, (HL)
 inc HL
 ld E, (HL)
 ex DE, HL
 pop DE
 add HL, DE
 ex DE, HL
 ld HL, car_slip_gr
 ld B, 8
 ld A, 2
 call grp_set_A		;HL_data,DE_(y,x),B_databit
 ld A, 1
 ld (tire_beep_A + 1), A
 ret

car_sprite_raceend:
 xor A
 ld (car_dir), A
 ld HL, (car_speed)
 ld H, 0
 ld A, fps
 call WSAN
 ld B, A
 ld HL, car_pos_mod_y
 ld A, (HL)
 sub B
 cp 256 - y0 - 17
 jr c, car_sprite_raceend_2
 ld (HL), A
 ret
car_sprite_raceend_2:
 ld A, 2
 ld (lap_end), A ;画面外に自車が消える
 ret

SET_SPRITE_SLIP_POSL:
 db 8, 2  ;dir=0
 db 8, 1
 db 7, 0
 db 6, 0
 db 4, 0  ;dir=4
 db 2, 0
 db 1, 0
 db 0, 1
 db 0, 2  ;dir=8
 db 0, 3
 db 1, 4
 db 2, 4
 db 4, 4  ;dir=12
 db 6, 4
 db 7, 4
 db 8, 3

SET_SPRITE_ENEMY:
 ld A, (IX + computer_spd)
 cp 253 ;高架下
 ret z
;背景原点と車座標を比較
 ld HL, (car_pos_x_crnt_v)
 ld DE, (scroll_vec + 2)	;x
 push BC
 ld BC, 12
 add HL, BC
 pop BC
 or A
 sbc HL, DE	;(car_pos_x + 2*n) - (scroll_vec_X)
 ret c
 ld DE, 144+6
 call cp_hl_de
 ret nc

 ld A, 12
 ld (SET_SPRITE_ENEMY_B + 1), A ;横描画数
 ld A, L
 ld HL, 0
 ld (SET_SPRITE_ENEMY_ADD + 1), HL
 sub 12
 jr nc, SET_SPRITE_ENEMY_X
 neg
 ld L, A
 ld H, 0
 ld (SET_SPRITE_ENEMY_ADD + 1), HL
 ld A, 12
 sub L
 ret z
 ld (SET_SPRITE_ENEMY_B + 1), A ;横描画数
 xor A
SET_SPRITE_ENEMY_X:
 ld (SET_SPRITE_ENEMY_DE + 1), A

 ld HL, (car_pos_y_crnt_v)
 ld DE, (scroll_vec)	;y
SET_SPRITE_RACEEND_R:
 push BC
 ld BC, 12
 add HL, BC
 pop BC
 or A
 sbc HL, DE	;(car_pos_y + 2*n) - (scroll_vec_Y)
 ret c
 ld DE, 48+12
 call cp_hl_de
 ret nc
 ld A, (car_sprite_fix + 1)
 add A, L
 sub 12
 ld (SET_SPRITE_ENEMY_DE + 2), A

 ld A, (IX + dir)
 srl A
 ld HL, car0
 ld DE, 24
 call xsan

SET_SPRITE_ENEMY_ADD:
 ld DE, overwrite
 add HL, DE
SET_SPRITE_ENEMY_DE:
 ld DE, overwrite	;D=y E=x
SET_SPRITE_ENEMY_B:
 ld B, overwrite
 push BC
 push DE
 push HL
 call grp_set
 pop HL
 ld DE, 12
 add HL, DE
 pop DE
 pop BC
 ld A, D
 add A, 8
 ld D, A
 call grp_set
 ret

SET_SPRITE_CRASH:
 ld HL, car_crash
 jp car_sprite_fix

SET_PIT_SIGN_TIMER:
 db overwrite
SET_PIT_SIGN:
 ;ld A, (objno)
 ;or A
 ;ret nz
;ピット情報表示
 ld HL, SET_PIT_SIGN_TIMER
 ld A, (HL)
 or A
 ret z
 dec (HL)
 ld HL, sign_fram_0
 ld DE, 0
 ld A, (car_sprite_fix + 1)
 add A, D
 ld D, A
 ld B, 35
 call grp_overwrite
 ld HL, sign_fram_1
 ld DE, 1000h
 ld A, (car_sprite_fix + 1)
 add A, D
 ld D, A
 ld B, 35
 call grp_overwrite
 ret

;---------画面スクロール----------
SCROLL:
 ld A, (objno)
 or A
 ret nz
SCROLL_A:
 ld A, overwrite
 ld A, (scroll_vec);y
 and 7
 ld (car_sprite_fix + 1), A
 ld (SCROLL_A + 1), A
 add A, 40h
 OUT (40h), A
 ret

;リザルト
result_check:
 ld HL, lap_end
 ld B, 4
result_check_lp:
 ld A, (HL)
 inc HL
 or A
 ret z
 djnz result_check_lp
result_stack:
 ld HL, (retstack)
 ld SP, HL
result:
 ld A, 40h
 out (40h), A ;スクロール位置
 ld A, 0a7h

 ld A, (rank)
 cp 1
 jr nz, result_2
 call cls

 ld HL, result1_mes
 ld DE, 0005h
 ld B, 16
 call msp

 jr result_1st_grp
result_1st_lp:
 call kscan
 cp 12h ;X
 jr z, result_2
 dec B
 jr nz, result_1st_lp
result_1st_grp:
 ld HL, winner_0
 ld DE, 0109h
 ld A, 5
result_1st_grp_lp:
 push AF
 ld B, 37
 call grp
 inc HL
 inc D
 ld E, 09h
 pop AF
 dec A
 jr nz, result_1st_grp_lp
 ld B, 90
result_1st_grp_tim1_lp:
 call kscan
 cp 12h ;X
 jr z, result_2
 dec B
 jr nz, result_1st_grp_tim1_lp
 ld DE, 010bh
 ld HL, winner_1
 ld A, 3
result_1st_grp_lp2:
 push AF
 ld B, 12
 call grp
 inc HL
 inc D
 ld E, 0bh
 pop AF
 dec A
 jr nz, result_1st_grp_lp2
 ld B, 90
 jr result_1st_lp

result1_mes:
 db "**** ﾕｳｼｮｳ! ****"

result_2:
 ld A, 0a7h
 out (40h), A ;画面反転
 call cls
 ld DE, 0004h
 call sub_window_draw

 ld A, (game_mode)
 cp 3
 jr z, result_endrun
 ;次コースへ
 ld HL, SAVE_NEXT_COURCE
 inc (HL)

 ld HL, result_mes_2
 ld DE, 0110h
 ld A, 10h
 ld C, 5
 call show_block_mes

 ld HL, set_point_write_store
 ld (set_point_write_jp + 1), HL
 ld A, 2
 jr result_lp
result_endrun:
 ld HL, set_point_endrun
 ld (set_point_write_jp + 1), HL
 ld A, 1
result_lp:
 push AF
 ld HL, result_mes
 ld DE, 000ah
 ld A, 3
 ld C, 5
 call show_block_mes
 
 ld E, 255  ;現在の車no
 ld C, 1  ;現在探す順位
 ld B, 4  ;4rank分
result_rank:
 ld HL, rank
result_rank_lp:
 inc E
 ld A, (HL)
 inc HL
 cp C
 jr nz, result_rank_lp
 inc C
 ld A, E
 call set_rank_name ;ポイント加算も
 ld E, 255
 djnz result_rank
 call TIM100
 call TIM255
 call kwait
 ld HL, set_point_write_print
 ld (set_point_write_jp + 1), HL
 ld DE, 0004h
 call sub_window_draw
 pop AF
 dec A
 jr nz, result_lp
 ld A, (game_mode)
 cp 3
 jp c, sprint_continue
 ;タイキュウ
 ld A, (rank)
 cp 1
 jp nz, season_end_print
ending: ;エンディング
 ld HL, SAVE_NAME
 ld A, (game_mode)
 cp 3
 jr nz, ending_2
 ld HL, endurance_name
ending_2:
 ld DE, ending_meses + 1
 ld BC, 4
 ldir
 ld A, (game_mode)
 cp 3
 ld HL, title_menu_2 + 9
 jr z, ending_class
 ld A, (game_class)
 ld DE, 5
 ld HL, sprint_info_class
 call xsan
ending_class:
 ld DE, ending_meses + 35
 ld BC, 5
 ldir
 call cls
 ld B, 24
 ld HL, fram_0 + 24 * 6
ending_mes_memdel:
 ld (HL), 0
 inc HL
 djnz ending_mes_memdel

 ld HL, fram_0 + 24 * 5
 ld B, 24
ending_mes_del:
 ld (HL), 0
 inc HL
 djnz ending_mes_del

 ld HL, ending_meses + 25
 ld DE, fram_0
 ld B, 5
ending_mes_copy:
 push BC
 ld A, (HL)
 inc HL
 ld B, A
 ld C, 24
ending_mes_copy_lp:
 ld A, (HL)
 inc HL
 ld (DE), A
 inc DE
 dec C
 djnz ending_mes_copy_lp
 ld A, C
 or A
 jr z, ending_mes_copy2
 xor A
ending_mes_copy_lp2:
 ld (DE), A
 inc DE
 dec C
 jr nz, ending_mes_copy_lp2
ending_mes_copy2:
 pop BC
 djnz ending_mes_copy

 ld HL, ending_meses
 ld DE, 0
 xor A
 ld C, 6
 call show_block_mes

 ld A, (game_mode)
 cp 3
 jr z, ending_roll_b
 ld A, (game_class)
 or A
 jp nz, ending_break ;ﾉｰﾋﾞｽ ｺｸﾅｲ はスタッフロールなし
 ld DE, 0300h
 ld HL, ending_meses2
 ld B, 24
 call msp
 ld DE, fram_0 + 48
 ld HL, ending_meses2
 ld BC, 24
 ldir
ending_roll_b:
 call TIM255
 call kwait
 call TIM255
ending_roll:
 ld A, 0a7h
 out (40h), A
 ld HL, staff_role_mes
 ld D, 0  ;進めた行
 ld E, 0  ;スクロールドット
ending_roll_lp:
 push DE
 call TIM127
 pop DE
 inc E
 ld A, E
 and 7
 add A, 40h
 out (40h), A
 bit 3, E
 jr z, ending_roll_2
 call show_fram_mes
 ld E, 0
 inc D
 ld A, D
 cp 49	;スタッフロール行数
 jp z, kan
ending_roll_2:
 jr ending_roll_lp

kan:
 xor A
 ld (vram_view), A
 ld HL, kan_graph
 ld DE, 020ah
 ld B, 11
 call grp
 call TIM255
 call kwait
 jp start

kan_graph:
 db 0Eh,0A2h,0AAh,0AAh,6Bh,2Bh,0EAh,0AAh,0A2h,0AEh,06

ending_break:
 call TIM255
 call kwait
 jp start

show_fram_mes:  ;HL = 新しく付け加えるブロックメッセージ
 push DE
 push BC
 push HL
 ;下のメッセージを上に上げる
 ld HL, fram_0 + 24
 ld DE, fram_0
 ld BC, 24 * 6
 ldir
 pop HL
 ld A, 24
 ld C, (HL)
 sub C  ;A=空白部ノコリ
 inc HL
 ld B, 0
 ld DE, fram_0 + 24 * 6
 ldir
 push HL
 ld B, A
 xor A
 ex DE, HL
show_fram_mes_lp:
 ld (HL), A
 inc HL
 djnz show_fram_mes_lp
 call cls
 ld DE, 0
 ld HL, fram_0
 ld B, 24*6 - 1
 ld A, 1
 ld (vram_view), A
 call msp
 pop HL
 pop BC
 pop DE
 ret

ending_meses:
 db 24, "____ ｻﾝ ﾕｳｼｮｳ ｵﾒﾃﾞﾄｳ !!!"
 db 24, "ｱﾅﾀﾊ ｸﾗｽ _____ ﾉ ﾁｬﾝﾋﾟｵﾝ"
 db 7, "ﾆ ﾅﾘﾏｼﾀ"
 db 23, "ｻﾗﾅﾙ ｸﾗｽﾉ ｾｲﾊｦ ｷﾀｲｼﾃｲﾏｽ"
 db 6, "......"
 db 24, "THANK YOU FOR PLAYING!!!"
ending_meses2:
 db "ｻﾗﾅﾙ ｺｰｽﾚｺｰﾄﾞﾉ ｼﾞｭﾘﾂｦ..."

staff_role_mes:
 db 6, "ｼﾖｳﾂｰﾙ"
 db 16, " g800 / g800(ｶｲ)"
 db 5, " ZASM"
 db 7, " VSCode"
 db 14, " Visual Studio"
 db 8, " Terapad"
 db 6, " ﾍﾟｲﾝﾄ"
 db 5, " GIMP"
 db 8, " BMP2PGD"
 db 1, " "
 db 12, "ｿﾉﾎｶ ｼﾞｻｸﾂｰﾙ"
 db 6, " TILER"
 db 7, " ZIPPER"
 db 7, " RECTER"
 db 1, " "
 db 19, "ｽﾍﾟｼｬﾙｻﾝｸｽ ｹｲｼｮｳﾘｬｸ"
 db 9, " Maruhiro"
 db 11, " CharatSoft"
 db 9, " sekimegu"
 db 6, " ﾕﾘｸﾞﾐ"
 db 4, " ｺｱﾗ"
 db 6, " chame"
 db 18, " Twitter ｦ ﾐﾃｸﾚﾀｶﾀ"
 db 13, "      And You"
 db 1, " "
 db 1, " "
 db 20, "     ｾｲｻｸ TS.SOFT   "
 db 1, " "
 db 1, " "
 db 1, " "
 db 24, "Let", 27h
 db "s Challenge Records."
 db 15, " ｽﾋﾟｰﾄﾞｺｰｽ 2919"
 db 15, "  ﾓﾝﾄｼﾃｨ   1887"
 db 15, " ﾍｱﾋﾟﾝﾊﾟｰｸ 1575"
 db 1, " "
 db 1, " "
 db 1, " "
 db 19, "    ｺﾉｹﾞｰﾑﾉ ｺﾝﾃﾝﾂ ﾊ"
 db 23, " ｹﾞﾝｼﾞﾂﾄ ﾅﾝﾗ ｶﾝｹｲ ｱﾘﾏｾﾝ"
 db 23, "  ｺｳﾂｳ ﾙｰﾙｦ ﾏﾓｯﾃ ｱﾝｾﾞﾝﾅ"
 db 23, " ﾄﾞﾗｲﾋﾞﾝｸﾞ ｦ ｺｺﾛｶﾞｹﾏｼｮｳ"
 db 7, " ......"
 db 1, " "
 db 1, " "
 db 1, " "
 db 1, " "
 db 1, " "
 db 1, " "
 db 1, " "

result_mes:
 db 6, "ﾚｰｽｹｯｶ"
 db 4, " 1ST"
 db 4, " 2ND"
 db 4, " 3RD"
 db 4, " 4TH"

result_mes_2:
 db 4, " + 4"
 db 4, " + 3"
 db 4, " + 2"
 db 4, " + 1"

set_rank_name:  ;A=車ID C=順位
 push DE
 push BC
 or A
 ld (set_point_write_A + 1), A
 jr z, set_rank_name_me
 push AF
 ld A, (game_class)
 ld HL, car_coms_names_1
 ld DE, 12
 call xsan
 pop AF
 dec A
 ld DE, 4
 call xsan
 jr set_rank_name_write
set_rank_name_me:
 ld HL, SAVE_NAME
 ld A, (game_mode)
 cp 3
 jr nz, set_rank_name_write
 ld HL, endurance_name
set_rank_name_write:
 ld B, 4
 ld D, C
 dec D
 ld E, 9
 push DE
 call msp
 ld HL, SAVE_POINT
set_point_write_A:
 ld A, overwrite
 call HL_ADD_A
 ld A, (HL)
 push HL
 ld L, A
 ld H, 0
 call ASCN
 pop HL
 ld B, (HL)
 pop DE
 ld A, 5
 sub D
 add A, B
set_point_write_jp:
 jp overwrite
set_point_write_store:
 ld (HL), A
set_point_write_print:
 ld HL, NMEMRY + 3
 ld E, 0eh
 ld B, 2
 call msp
set_point_endrun:
 pop BC
 pop DE
 ret

;car_coms_names_3
;------------ ギアレシオからスピードテーブルを作成 ------------
init_speed_table:;HL=テーブル開始アドレス DE=ギアレシオ開始アドレス
 ld B, 6	;6速
init_speed_table_loop:
 push BC
 ld A, (DE)
 inc DE
 ld C, A
 ld A, (DE)
 inc DE
 ld B, A
 push DE	;::
 push HL	;
 ;BC=ギアレシオ
 ld HL, speed_level
 ld A, 255
init_speed_baselp:
 inc A
 or A
 sbc HL, BC
 jr nc, init_speed_baselp
 ex AF, AF'
 ld B, 12
 pop HL		;
init_speed_0lp:	;0~1100 rpmの速度は0
 xor A
 ld (HL), A
 inc HL
 djnz init_speed_0lp
 push HL	;

 ex AF, AF'
 ld B, 160-12	;loop count
 ld C, A		;base speed
 ld HL, 100
 ld (init_speed_l + 1), HL
init_speed_l:
 ld HL, overwrite	;now rpm - 1100 overwrite
 call get_speed_per_nowrpm
 ld HL, (init_speed_l + 1)
 ld DE, 100
 add HL, DE
 ld (init_speed_l + 1), HL
 pop HL		;
 ld (HL), A
 ;call log_HL
 inc HL
 push HL	;
 djnz init_speed_l
 pop HL		;
 pop DE		;::
 pop BC
 djnz init_speed_table_loop
 ret

get_speed_per_nowrpm:;C=base speed HL=now rpm - 1100
 push HL
 push BC
 ;C=base speed * (HL=(now rom -1100 * 100) / 14800)
 ld A, 148
 call WSAN	;(HL=(now rom -1100 * 100) / 14800)
 inc A
 ld HL, 0
 ld E, C
 ld D, 0
 call xsan	;base speed * A
 ld A, 100
 call WSAN	;HL / 100
 or A
 jr nz, get_speed_per_nowrpm_end
 inc A
get_speed_per_nowrpm_end:
 pop BC
 pop HL
 ret

;-------- キーボード入力 ---------
input_key:
 call kscan_ex
 jr z, input_key

 ld B, A
 ld HL, input_key_io
 ld C, 255
input_cp:
 inc C
 ld A, (HL)
 inc HL
 cp 255
 ret z ;(-1)該当なし
 cp B
 jr nz, input_cp

 ld HL, input_key_asc
 ld B, 0
 add HL, BC
 ld A, (HL) ;獲得文字
 ret

input_key_io:
 db 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15
 db 16, 17, 18, 19, 20, 21, 22, 23
 db 41, 49, 50, 57, 40, 58, 255
input_key_asc:
 ;db "qwertyuasdfghj"
 ;db "kzxcvbnm"
 ;db "liop"
 db "QWERTYUASDFGHJ"
 db "KZXCVBNM"
 db "LIOP"
 db 253, 254  ;return backspace

;------- beepウェイト --------
TIM_BEEP:
 ld A, (lap_end)
 or A
 jp nz, objfetch_loop_end
 ld A, (car_crash_fl)
 cp 32
 jr z, bomp_beep
 ld A, (car_slip_x)
 or A
 jr nz, drift_beep
tire_beep_A:
 ld A, overwrite
 or A
 ld A, 0
 ld (tire_beep_A + 1), A
 jr nz, drift_beep
 ld A, (car_rpm_st)
 ld B, A
 ld A, 255
 sub B
 ld B, A
 ld A, 20
 ld HL, engine_beep_data
 push HL
TIM_BEEP_LP:
 ld (HL), B
 inc HL
 dec A
 jr nz, TIM_BEEP_LP
 pop HL
TIM_BEEP_COM:
TIM_BEEP_WAIT:
 LD E,overwrite   ;モードで変化する音の単位長さ 2 or 7
TIM_BEEP_WAIT_A:
 LD A, 4;overwrite   ;設定で変化する音の単位長さ 0 ~ 8 初期値4
 add A, E
 ld E, A
ST:
 LD D, (HL)		;周波数
 inc HL
 LD A,80H
 OUT (18H),A
L1:
 DEC D
 DB 0, 0
 JR NZ,L1
 LD D, (HL)
 XOR A
 OUT (18H),A
L2:
 DEC D
 DB 0
 JR NZ,L2
 DEC E
 JR NZ,ST
 jp objfetch_loop_end
 
bomp_beep:
 ld HL, bomp_beep_data
 jr TIM_BEEP_COM

drift_beep:
 ld HL, drift_beep_data
 jr TIM_BEEP_COM

pause_prev:
 db overwrite
pause:
 call pause_sub
 ret z
 cp 1eh ;space
 ret nz
 ld HL, pause_mes
 ld B, 11
 ld DE, 0207h
 call msp
 call TIM30
pause_lp:
 in A, (1fh)
 rla		
 jp c, endpoint	;ゲーム終了
 call pause_sub
 jr z, pause_lp
 cp 1eh ;space
 jr nz, pause_lp
 ret

pause_sub:
 call kscan
 ld HL, pause_prev
 ld B, A
 ld A, (HL)
 cp B
 ret z
 ld A, B
 ld (HL), A
 ret

pause_mes:
 db "-- PAUSE --"

engine_beep_data:
 ds 20  ;0bh TIM_BEEP_WAIT最大数
bomp_beep_data:
 db 160, 70, 240, 120, 250, 111, 238, 111, 180, 135, 230
 db 160, 70, 240, 120, 250, 111, 238, 111, 180, 135, 230
drift_beep_data:
 db 222, 244, 210, 250, 210, 240, 200, 230, 190, 210, 180
 db 222, 244, 210, 250, 210, 240, 200, 230, 190, 210, 180
;------- タイマー --------
TIM3:
 push AF
 LD A,4
 JR TIMER
TIM10:
 push AF
 LD A,12
 JR TIMER
TIM30:
 push AF
 LD A,37
 JR TIMER
TIM100:
 push AF
 ld A, 200
 jr TIMER
TIM127:
 push AF
 call kscan
 cp 51h
 jp z, endpoint
 or A
 jr nz, TIM30 + 1
 ld A, 188
 JR TIMER
TIM255:
 push AF
 LD A,255
TIMER:
 LD DE,0
 INC DE;LOOP
 DB 0,0,0,0,0,0
 CP D
 jr Z, TIMER_E
 JR TIMER+3
TIMER_E:
 pop AF
 ret
 
kscan_prev:
 db overwrite
kscan_ex:
 push HL
 push BC
 push DE
 call kscan
 ld HL, kscan_prev
 or A
 jr z, kscan_ex2
 ld B, A
 cp (HL)
 ld A, 0
 jr z, kscan_exE
 ld A, B
kscan_ex2:
 ld (HL), A
 cp 51h
 jp z, endpoint
 or A
kscan_exE:
 pop DE
 pop BC
 pop HL
 ret
;--------- 乱数 -----------
RAND:;Aﾆｶｴｽ
 push HL
 push DE
RAND_HL:
 LD HL, overwrite
 LD D,H
 LD E,L
 ADD HL,HL
 ADD HL,HL
 ADD HL,DE
 LD DE,5D47H  ;最初のシード値
 ADD HL,DE
 LD (RAND_HL + 1),HL
 LD A,H
 pop DE
 pop HL
 RET
RAND4:
; ld A,R
; rra
; rra
; rra
; and 11000000b
; ret
 CALL RAND
 AND 00000011b
 RET
 
cp_hl_de:
 or A
 sbc HL, DE
 push AF
 add HL, DE
 pop AF
 ret

;--------- HL + DE * A --------
XSAN:
 OR A
 RET Z
XSANL:
 ADD HL,DE
 DEC A
 JR NZ,XSANL
 RET
 
;-------- BCD化 ------
NMEMRY:
 DB overwrite, overwrite, overwrite, overwrite, overwrite
NMEMRY_reserved:
 DB '0', '0', '0', '0', '0'
ASCN:;HL=数値(2byte)
 LD DE,NMEMRY
 LD BC,10000
 CALL WARIS
 LD BC,1000
 CALL WARIS
 LD BC,100
 CALL WARIS
 LD BC,10
 CALL WARIS
 LD A,L
 LD (DE),A
 LD B,5
 LD DE,NMEMRY
ASCL:
 LD A,(DE)
 CP 0
 JR Z,ASCL2
 JR ASC2
ASCL2:
 INC DE
 DJNZ ASCL
 LD B,1
 LD DE,NMEMRY+4
ASC2:
 LD A,(DE)
 LD C,30H
 ADD A,C
 LD (DE),A
 INC DE
 DJNZ ASC2
 RET
 
;-------- 16進数ログ ----------

;LOG16_MEMORY:
; DB 0, overwrite, overwrite, overwrite, overwrite
;LOG16:;HL = 数値
; push HL
; push DE
; push AF
; 
; ld DE, LOG16_MEMORY + 4
; 
; ld A, L
; and 0fh
; add A, 30h
; call LOG16_ASC
; ld (DE), A
; dec DE
; 
; ld A, L
; and 0f0h
; rra
; rra
; rra
; rra
; add A, 30h
; call LOG16_ASC
; ld (DE), A
; dec DE
; 
; ld A, H
; and 0fh
; add A, 30h
; call LOG16_ASC
; ld (DE), A
; dec DE
; 
; ld A, H
; and 0f0h
; rra
; rra
; rra
; rra
; add A, 30h
; call LOG16_ASC
; ld (DE), A
; 
; pop AF
; pop DE
; pop HL
; 
; ret
; 
;LOG16_ASC:
; cp 49		;1
; ret c
; cp 58		;9の次のASCII文字
; ret c
; add A, 7	;A-FをASCII文字に
; ret
; 
;;------- 数値テスト表示 -------
;log_POS_HL:;HL = 数値 DE = 桁行(y, x)
; push AF
; push BC
; push DE
; push HL
; 
; ld HL, log_space
; ld B, 6
; call msp
; 
; pop HL
; pop DE
; push DE
; push HL
; call LOG16
; 
; ld B, 5
; ld HL, LOG16_MEMORY
; call msp
; 
; pop HL
; pop DE
; pop BC
; pop AF
; ret
;
;log_HL_kcheck:
; push AF
; call keysc
; bit 5, A
; call nz, log_HL_kwait
; pop AF
; ret
;
;log_DE_kcheck:
; push AF
; ex DE, HL
; call keysc
; bit 5, A
; call nz, log_HL_kwait
; ex DE, HL
; pop AF
; ret
;
;log_A_kcheck:
; push BC
; push DE
; push HL
; push AF
; call keysc
; and 00100000b
; pop BC
; ld A, B
; call nz, log_A_kwait
; pop HL
; pop DE
; pop BC
; ret
;
;log_A_kwait:
; push AF
; call log_A
; call kwait
; pop AF
; ret
;
;log_0_kwait:
; push HL
; ld HL, 0
; call log_HL_kwait
; pop HL
; ret
; 
;log_SP_kwait:
; push HL
; ld HL, 0
; add HL, SP
; dec HL
; dec HL
; call log_HL_kwait
; pop HL
; ret
;
;log_DE_kwait:
; ex DE, HL
; call log_HL_kwait
; ex DE, HL
; ret
;
;log_DE:
; ex DE, HL
; call log_HL
; ex DE, HL
; ret
;
;log_BC_kwait:
; push HL
; push BC
; pop HL
; push BC
; call log_HL_kwait
; pop BC
; pop HL
; ret
;
;log_BC:
; push HL
; push BC
; pop HL
; push BC
; call log_HL
; pop BC
; pop HL
; ret
;
;log_HL_kwait:
; push AF
; call log_HL
; call kwait
; pop AF
; ret
;
;log_A:		;A=数値
; push DE
; push BC
; push HL
; push AF
; 
; ld HL, log_space
; ld DE, 0200h
; ld B, 5
; call msp
; 
; pop AF
; push AF
; ld H, 0
; ld L, A
; call ASCN
; ld DE, 0200h
; ld B, 5
; ld HL, NMEMRY
; call msp
; 
; pop AF
; pop HL
; pop BC
; pop DE
; ret
;
;log_Pos_A:		;A=数値 DE=座標
; push BC
; push HL
; push AF
; 
; ld HL, log_space
; ld B, 5
; push DE
; call msp
; pop DE
; 
; pop AF
; push AF
; push DE
; ld H, 0
; ld L, A
; call ASCN
; pop DE
; push DE
; ld B, 5
; ld HL, NMEMRY
; call msp
; 
; pop DE
; pop AF
; pop HL
; pop BC
; ret
;
;log_HL:		;HL=数値
; push AF
; push DE
; push BC
; push HL
; 
; ld HL, log_space
; ld DE, 0200h
; ld B, 6
; call msp
; 
; pop HL
; push HL
; call LOG16
; 
; ld DE, 0200h
; ld B, 5
; ld HL, LOG16_MEMORY
; call msp
; 
; pop HL
; pop BC
; pop DE
; pop AF
; ret
;
;log_space:
; db "      "		;スペース
;
;------- 割り算 -------
WSAN:		;HL / A = A 割り切れZ あまりHL
 push BC
 push DE
 
 ld C, A
 ld B, 0
 xor A
 ld DE, 0
WSAN1:
 sbc HL, BC
 jr c, WSAN2
 inc DE
 jr z, WSAN3
 jr WSAN1
WSAN2:
 add HL, BC
WSAN3:
 ld A, E
 pop DE
 pop BC
 ret

WARIS:
 XOR A
;WARI1:
 SBC HL,BC
 JR C,WARI2
 INC A
 JR WARIS+1
WARI2:
 ADD HL,BC
 LD (DE),A
 INC DE		;
 RET
 
;--------- GRAMにキャラ設置 ------------
grp_set:;HL_data,DE_(y,x),B_databit
 ld A, 1
grp_set_A:  ;A=0 AND A=1 OR A=2 XOR
 ld (write_lcd_loop + 1), A
grp_set_com:
 ld A, B
 ld (write_lcd_B + 1), A	;LCDに移すbit数
;up,non direction
grp_set_0:
 ld A,E
 ld (grp_set_or1+1),A
 ld A,D
 and 3fh
 ld D,0
grp_set_set1:
 inc D
 sub 8
 jr nc,grp_set_set1
 dec D
 ld C, A
 ld A, D
 ld (write_lcd_D + 1), A	;基準行
 ld A, C
 add A,8
grp_set_set2:
 ld (grp_set_shiftd_bit+1),A
 ld C,A
 ld A,8
 sub C
 ld (grp_set_shiftu_bit+1),A

grp_set_or1:
 ld A, overwrite;0~143 転送先アドレスの増分
 ld (write_lcd_E + 1), A	;基準列ドット
 
 ld DE,graph_area	;DE = 転送先アドレス
 push HL					;HL
 push BC						;BC
grp_set_shiftd_bit:
 ld C,overwrite		;shift bit count
 ld A,(HL)
 ex AF,AF'
 ld A,C
 or A
 jr z,grp_set_shiftd_bit_2
 ex AF,AF'
grp_set_shiftd_bit_l:
 sla A
 dec C
 jr nz,grp_set_shiftd_bit_l
 ex AF,AF'
grp_set_shiftd_bit_2:
 ex AF,AF'
grp_set_change1:
 ld (DE),A
 ;call log_DE_kwait
grp_set_shiftd_l:
 inc HL
 inc DE
 djnz grp_set_shiftd_bit
 
 exx
 ld HL, graph_area
 call write_lcd
 exx

 ex DE,HL
 ld DE,xdots
 add HL,DE


 pop BC
grp_set_shiftu_bit_b:
 ld DE,graph_area + xdots	;DE = 転送先アドレス + xdots
 ld HL, write_lcd_D + 1
 
 inc (HL)
 pop HL			;data2
grp_set_shiftu_bit:
 ld C,overwrite		;shift bit count
 ld A,(HL)
grp_set_shiftu_bit_l
 srl A
 dec C
 jr nz,grp_set_shiftu_bit_l
 ld (DE),A
grp_set_shiftu_l:
 inc HL
 inc DE
 djnz grp_set_shiftu_bit
 exx
 ld HL, graph_area + 144
 call write_lcd
 exx
 
 ret

;座標指定でグラフィックを上書き
grp_overwrite:
 xor A
 call grp_set_A
 ret

;----------- ずらしたキャラクターをLCDに書き込む ------------
write_lcd:	;HL = データ始まり (graph_area or graph_area + 144)
write_lcd_D:
 ld D, overwrite		;基準何行目か
write_lcd_E:
 ld E, overwrite		;基準基準列ドット目か
write_lcd_B:
 ld B, overwrite		;LCDに移すビット数
 
write_lcd_exec:	;HL = データ始まり (graph_area or graph_area + 144)
 ld C, 0    ;芝生踏んでるカウント数

 ld A, D
 and 00000111b
 or 10110000b
 out (40h), A				;行アドレス設定
 
 ld A, E
 and 00001111b
 out (40h), A				;桁ビット下位4ビット
 
 ld A, E
 rra
 rra
 rra
 rra
 and 00001111b
 or 00010000b
 out (40h), A				;桁ビット上位4ビット
 
 ld A, 0e0h
 out (40h), A

write_lcd_loop:
 ld A, overwrite	;0=AND処理 1=XOR処理(ORから変更) 2=XOR処理 3=OR処理
 or A
 jr z, write_lcd_loop1
 cp 3
 jr z, write_lcd_or
 in A, (41h)
 in A, (41h)
 cp 0ffh
 jr c, write_lcd_loop1
 inc C
write_lcd_loop1:
 xor (HL)
write_lcd_loop2:
 inc HL
 
 ;ex AF, AF'
 ;call write_lcd_set_E
 ;ex AF, AF'
 
 inc E
 out (41h), A
 djnz write_lcd_loop
 
 ld A, 0eeh
 out (40h), A

read_lcd_A:
 ld A, overwrite
 or A
 ret z
 ld A, C
 cp 9 ;0ffhを踏んでるビット縦数 0 ~ 8 踏んでない 9 ~ 12 踏んでる
 ld A, 1
 jr c, read_lcd_2
 ld A, 2
read_lcd_2:
 ld (on_sand), A
 ret
 
write_lcd_or:
 in A, (41h)
 in A, (41h)
 or (HL)
 jr write_lcd_loop2

;-------- ランレングス圧縮された背景データを展開する --------
unzip:		;HL=展開元アドレス DE=展開先アドレス
 ;call log_HL_kwait
 ;call log_HL_kwait
 push HL
 push AF
 ld BC, screen
unzip_loop1:
 ld A, (HL) ;値
 exx
 ld E, A  ;値
 exx
 inc HL
 ld A, (HL) ;カウント
 exx
 ld D, A  ;カウント
 ld A, E
 exx
unzip_loop_2:
 ld (DE), A
 inc DE
 dec BC
 exx
 dec D	;カウント
 exx
 jr nz, unzip_loop_2
 
 inc HL
 xor A
 or B
 jr nz, unzip_loop1
 or C
 jr nz, unzip_loop1
 pop AF
 pop HL
 ret
 
;-------- clear screen ------
cls:
 push HL
 push DE
 push BC
 ld HL, fram_1
 ld (HL), 0
 ld DE, fram_1 + 1
 ld BC, 144
 ldir
 ld DE, 0
 ld B, 8
cls_lp:
 push BC
 ld B, 144
 ld HL, fram_1
 call grp
 inc D
 ld E, 0
 pop BC
 djnz cls_lp
 xor A
 ld (vram_view), A
 pop BC
 pop DE
 pop HL
 ret

;-------- fram clear --------
fram_clear:
;背景描画
 ld HL, fram_0
 ld DE, fram_0 + 1
 ld (HL), 0ffh
 ld BC, screen * 4 - 1
 ldir

 call fram_clear_sign

 xor A
; ld (scroll_block), A
; ld (current_bg_index), A
; ld (scroll_pos_x), A
; ld (scroll_vec), A
; ld (scroll_vec + 1), A
; ld (scroll_vec + 2), A
; ld (scroll_vec + 3), A
;21 byte -> 13 byte
 ld HL, scroll_vec
 ld (HL), A
 ld DE, scroll_vec + 1
 ld BC, 6
 ldir
;芝描画取得フラグリセット
 ld (read_lcd_A + 1), A

;物理値初期化
 ld HL, car_physics
 ld DE, car_physics + 1
 ld (HL), A
 ld BC, physics_num - 1
 ldir

;ルーチン一次保存値初期化
 xor A
 ld HL, CARMOVE_MOVEPOS_VEC
 ld (HL), A
 ld DE, CARMOVE_MOVEPOS_VEC + 1
 ld BC, 40 - 1
 ldir

;一次保存値初期化
 ld HL, car_temp_values
 ld (HL), A
 ld DE, car_temp_values + 1
 ld BC, 18 - 1
 ldir

;カレント変数not reserbed初期化 しない
 ;ld HL, CARMOVE_COMPUTER_NEXT_crnt

;故障変数初期化
 ld HL, ENGINE_BLOW
 ld (HL), A
 ld DE, ENGINE_BLOW + 1
 ld BC, 4 - 1
 ldir

;敵ピットインフラグ初期化
 ;ld HL, CARMOVE_AUTO_PITIN_FLAG
 ;ld (HL), A
 ;inc HL
 ;ld (HL), A
 ;inc HL
 ;ld (HL), A
 ;inc HL
 ;ld (HL), A
 ;ld A, 75h
 ;ld (ENGINE_OVERHEET + 1), A
 ret

fram_clear_sign:
;サイン表示
 xor A
 ld HL, sign_fram_0
 ld (HL), A
 ld DE, sign_fram_0 + 1
 ld BC, 69
 ldir

 ld HL, lap_P
 ld DE, sign_fram_0
 ld BC, 6
 ldir

 ld HL, lap_L
 ld DE, sign_fram_0 + 7 * 2
 ld BC, 6
 ldir
 ret
;下図のような分布にする
;lap_info_pos_lap:  ;順位残り周回数
; db "P", overwrite, "L", overwrite, overwrite
lap_P:
 db 0FEh,0FEh,12h,12h,12h,0Ch
;lap_info_rank:
; db overwrite, overwrite, overwrite, overwrite, overwrite, overwrite, 0
lap_L:
 db 0FEh,0FEh,80h,80h,80h,80h
;lap_info_laprem:
; db overwrite, overwrite, overwrite, overwrite, overwrite, overwrite, 0
; db overwrite, overwrite, overwrite, overwrite, overwrite, overwrite, 0
;lap_info_time:
; db overwrite, overwrite, overwrite, overwrite, overwrite, overwrite, 0
; db overwrite, overwrite, overwrite, overwrite, overwrite, overwrite, 0
; db overwrite, overwrite, overwrite, overwrite, overwrite, overwrite, 0
; db overwrite, overwrite, overwrite, overwrite, overwrite, overwrite, 0
; db overwrite, overwrite, overwrite, overwrite, overwrite, overwrite, 0
lap_T:;T
 db 02,02,0FEh,0FEh,02,02
lap_F:;F
 db 0FEh,0FEh,12h,12h,12h,12h
 
;-------- LCD書き込みのために使う ---------
graph_area:
 ds xdots
 ds xdots
 
;-------- キー入力 --------
keysc_prevkey:
 db overwrite
keysc_key:
 db overwrite
keysc:		;0bit (left,right,z,x,tab,space,down,up) 7bit
 di
 ld A, (keysc_key)
 ld (keysc_prevkey), A
 xor A
 out (11h),A
 ld A,08h		;4bit (tab space down up) 7bit
 out (11h),A
 call 8aadh
 in A,(10h)
 and 0f0h
 ld B,A
 
 xor A
 out (11h),A
 ld A,10h
 out (11h),A
 call 8aadh
 in A,(10h)
 and 03h		;0bit (left right / /) 3bit
 or B
 ld B,A
 
 xor A
 out (11h),A
 ld A,04h
 out (11h),A
 call 8aadh
 in A,(10h)
 rla
 rla
 and 0ch
 or B			;0bit (/ / z x) 3bit
 
 ei
 call keysc_numkey
 ld (keysc_key), A
 ret

keysc_numkey:
 ld C, A
 ld B, 0
 di
 xor A
 out (11h),A
 ld A,20h
 out (11h),A
 call 8aadh
 in A,(10h)
 and 10h		;2
 jr z, keysc_numkey_1
 set 6, B
keysc_numkey_1:
 xor A
 out (11h),A
 ld A,40h
 out (11h),A
 call 8aadh
 in A,(10h)		;4 6
 bit 3, A
 jr z, keysc_numkey_2
 set 0, B
keysc_numkey_2:
 bit 5, A
 jr z, keysc_numkey_3
 set 1, B
keysc_numkey_3:
 xor A
 out (11h),A
 ld A,80h
 out (11h),A
 call 8aadh
 in A,(10h)
 and 10h		;8
 jr z, keysc_numkey_4
 set 7, B
keysc_numkey_4:
 ld A, B
 or C
 ei
 ret
;------ グラフィック --------
;------ キャラクタ ----------
chara_del:
 db 0,0,0,0,0,0
prompt_l:
 db 00,08,1Ch,3Eh,7Fh,00
prompt_r:
 db 00,7Fh,3Eh,1Ch,08,00
;------ タイトル -------
title:
 db 00,00,00,00,00,40h,0E0h,0E0h,0E0h,0E0h,0E0h,0E0h,0F0h,0FCh,0FFh,0FFh,0EFh,0E3h,0E0h
 db 0E6h,49h,09h,06h,0C0h,0F8h,0FFh,07Fh,07Fh,073h,070h,070h,070h,0F0h,0F0h,0F0h,0F0h,070h
 db 070h,030h,030h,00,00,00,00,00,80h,0E0h,0F8h,0FCh,7Eh,01Eh,01Eh,00Eh,000h,40h,0E0h,0E0h
 db 0E0h,0E0h,0E0h,0F8h,0FCh,0FEh,0EEh,0E6h,0E0h,0F0h,0F8h,0FEh,0FEh,0EEh,0E6h,0E0h,0E0h,40h
 db 00h,60h,70h,70h,0F0h,0F8h,0FCh,0FEh,07Eh,076h,70h,70h,70h,30h,00,00,00,00,00,00,00,80h
 db 0E0h,0F8h,0FCh,07Ch,1Ch,00,00,00,00
 db 00,0C0h,0F0h,0FCh,0FCh,3Ch,0Ch,00h,0E0h,0F8h,0FEh,0FFh,1Fh,07h,81h,0F0h,0FCh,0FCh,7Ch,0Ch
 db 00,04h,07h,03h,01h,00h,00h,80h,0C0h,0F0h,0FCh,0FFh,3Fh,0Fh,33h,38h,30h,38h,0E0h,0F8h,78h
 db 00,0C0h,0F8h,0FEh,07Fh,1Fh,1Fh,1Dh,1Ch,1Ch,1Ch,1Ch,1Ch,08h,00h,00h,02h,07h,03h,81h,0C0h
 db 0F0h,0FCh,03Fh,1Fh,1Fh,1Bh,18h,18h,19h,1Fh,0Eh,0Eh,0Eh,0Eh,0EEh,0FEh,0FFh,0FFh,7Fh,0Fh,0Eh
 db 0Eh,0Eh,7Eh,76h,00h,70h,0C0h,0F0h,0F0h,40h,0C0h,0F8h,0FEh,07Fh,1Fh,1Fh,1Dh,1Ch,1Ch,1Ch,1Ch,1Ch,0Ch
 db 01,01,01,01,40h,70h,7Ch,7Fh,3Fh,0Fh,03h,00,00,00,01,01,01,01,00,00,00,00,00,40h,70h,78h
 db 7Eh,3Fh,1Fh,07h,01h,00,00,00,06,06,06,07,03,71h,7Ch,3Fh,1Fh,47h,41h,40h,60h,60h,60h,60h
 db 70h,70h,78h,78h,5Ch,7Ch,5Eh,6Eh,5Fh,7Fh,5Fh,7Fh,5Dh,7Ch,5Ch,7Ch,5Ch,7Ch,5Ch,7Ch,5Ch,7Ch
 db 5Ch,7Ch,5Ch,7Fh,5Fh,7Fh,5Fh,7Ch,5Ch,7Ch,5Ch,7Ch,5Ch,7Ch,5Ch,7Ch,4Eh,47h,73h,7Ch,3Fh,1Fh
 db 07,01,00,00,00,00,00,00,00,00,00,00
 
;------ 車 -------
car0:
 DB 0,0,0,96,176,240,240,176,96,0,0,0,0,0,0,6
 DB 15,14,13,15,6,0,0,0
 ;db 255, 255, 255, 255, 255, 255, 255, 255, 255, 255
 ;db 255, 255, 255, 255, 255, 255, 255, 255, 255, 255
 ;db 255, 255, 255, 255
car1:
 DB 0,0,0,0,240,248,120,216,48,0,0,0,0,0,0,6
 DB 15,10,13,7,6,0,0,0
car2:
 DB 0,0,128,128,192,96,248,232,248,176,224,0,0,0,1,7
 DB 6,13,14,3,1,0,0,0
car3:
 DB 0,0,128,224,96,192,64,192,160,224,192,0,0,0,3,6
 DB 7,2,3,3,3,3,1,0
car4:
 DB 0,0,192,224,224,64,192,160,224,192,0,0,0,0,3,7
 DB 6,3,3,5,7,3,0,0
car5:
 DB 0,0,192,96,224,64,192,192,192,192,128,0,0,0,1,7
 DB 6,3,2,3,5,7,3,0
car6:
 DB 0,0,128,224,96,176,112,192,128,0,0,0,0,0,1,1
 DB 3,6,31,23,31,13,7,0
car7:
 DB 0,0,0,96,240,80,176,224,96,0,0,0,0,0,0,0
 DB 15,31,30,27,12,0,0,0
car8:
 DB 0,0,0,96,240,176,112,240,96,0,0,0,0,0,0,6
 DB 13,15,15,13,6,0,0,0
car9:
 DB 0,0,0,96,224,176,80,240,96,0,0,0,0,0,0,12
 DB 27,30,31,15,0,0,0,0
car10:
 DB 0,0,0,128,192,112,176,96,224,128,0,0,0,7,13,31
 DB 23,31,6,3,1,1,0,0
car11:
 DB 0,128,192,192,192,192,64,224,96,192,0,0,0,3,7,5
 DB 3,2,3,6,7,1,0,0
car12:
 DB 0,0,192,224,160,192,192,96,224,192,0,0,0,0,3,7
 DB 5,3,2,7,7,3,0,0
car13:
 DB 0,192,224,160,192,64,192,96,224,128,0,0,0,1,3,3
 DB 3,3,2,7,6,3,0,0
car14:
 DB 0,224,176,248,232,248,96,192,128,128,0,0,0,0,0,1
 DB 3,14,13,6,7,1,0,0
car15:
 DB 0,0,0,48,216,120,248,240,0,0,0,0,0,0,0,6
 DB 7,13,10,15,6,0,0,0
car_crash:
 db 0E2h,14h,28h,0C4h,83h,44h,5Ah,84h,64h,48h,74h,82h
 db 0CCh,31h,30h,28h,23h,30h,36h,5Dh,16h,20h,33h,18h
car_slip_gr:
 db 0A5h,48h,00h,94h,2Ah,80h,25h,48h
;----------- ピット -------------
pit_engine_l:
 db 10h, 14h, 1eh, 07h, 0bh, 14h
pit_engine_r:
 db 14h, 0bh, 07h, 1eh, 14h, 10h
pit_fuel1:
 db 8, 8, 8, 7fh, 0ffh, 0
pit_fuel2:
 db 0, 0, 1eh, 1eh, 1fh, 0
pit_wing_u:
 db 40h, 60h, 7fh, 67h, 60h, 40h
;------- コース0 -------
cource0_0:
mini_5:
 DB 128,5,255,29,0,3,255,1,0,65,255,1,0,3,255,32
 DB 128,10,255,29,0,3,255,1,0,18,8,1,0,27,8,1
 DB 0,15,64,1,0,2,255,1,0,3,255,32,128,10,255,29
 DB 0,3,255,1,0,65,255,1,0,3,255,32,128,10,255,29
 DB 0,3,255,1,0,3,4,1,0,28,4,1,0,32,255,1
 DB 0,3,255,32,128,10,255,29,0,3,255,1,0,65,255,1
 DB 0,3,255,32,128,10,255,29,0,3,255,1,0,7,8,1
 DB 0,50,2,1,0,6,255,1,0,3,255,32,128,5
cource0_1:
mini_0:
 DB 128,5,255,1,249,62,121,4,127,1,57,5,25,3,153,2
 DB 137,61,128,5,255,39,63,1,31,1,15,1,135,3,71,2
 DB 67,3,35,2,33,3,17,3,16,2,8,5,4,5,2,5
 DB 1,2,0,62,128,5,255,33,127,1,63,1,15,1,3,1
 DB 129,1,64,1,48,1,12,1,6,1,1,1,0,21,32,1
 DB 0,45,2,1,0,28,128,5,255,30,127,1,7,1,1,1
 DB 0,1,240,1,14,1,2,1,1,1,0,97,128,1,0,3
 DB 128,5,255,30,0,3,254,1,3,1,0,47,4,1,0,56
 DB 128,5,255,29,3,1,0,2,252,1,3,1,0,7,8,1
 DB 0,56,252,1,2,2,1,1,241,37
cource0_2:
mini_2:
 DB 137,58,153,2,25,3,57,5,121,4,127,1,249,65,255,1
 DB 128,5,0,51,128,1,0,7,1,2,2,5,4,5,8,5
 DB 16,2,17,3,33,3,35,2,67,3,71,2,135,3,15,1
 DB 31,1,63,1,255,42,128,5,0,78,32,1,0,14,1,1
 DB 6,1,12,1,48,1,64,1,129,1,3,1,15,1,63,1
 DB 127,1,255,36,128,5,0,9,4,1,0,88,1,1,2,1
 DB 14,1,240,1,0,1,1,1,7,1,127,1,255,33,128,5
 DB 0,55,32,1,0,45,3,1,254,1,0,3,255,33,128,5
 DB 241,31,225,1,129,1,2,1,4,1,24,1,96,1,128,1
 DB 0,54,4,1,0,9,3,1,252,1,0,2,3,1,255,32
 DB 128,5
cource0_4:
mini_7:
 DB 143,31,135,1,129,1,64,1,32,1,24,1,6,1,1,1
 DB 0,64,192,1,63,1,0,2,192,1,255,32,128,5,0,32
 DB 64,1,0,43,2,1,0,24,192,1,127,1,0,3,255,33
 DB 128,5,0,98,128,1,64,1,112,1,15,1,0,1,128,1
 DB 224,1,254,1,255,33,128,5,0,8,32,1,0,84,128,1
 DB 96,1,48,1,12,1,2,1,129,1,192,1,240,1,252,1
 DB 254,1,255,36,128,5,0,59,128,2,64,5,32,4,34,1
 DB 16,5,8,2,136,3,132,3,196,2,194,3,226,2,225,3
 DB 240,1,248,1,252,1,255,42,128,5,145,58,153,2,152,3
 DB 156,5,158,4,254,1,159,65,255,1,128,5
cource0_5:
mini_17:
 DB 128,5,255,29,192,1,0,2,63,1,192,1,0,64,1,1
 DB 6,1,24,1,32,1,64,1,129,1,135,1,143,34,128,5
 DB 255,30,0,3,127,1,192,1,0,19,16,1,0,84,128,5
 DB 255,30,254,1,224,1,128,1,0,1,15,1,112,1,64,1
 DB 128,1,0,77,8,1,0,23,128,5,255,33,254,1,252,1
 DB 240,1,192,1,129,1,2,1,12,1,48,1,96,1,128,1
 DB 0,43,16,1,0,52,128,5,255,39,252,1,248,1,240,1
 DB 225,3,226,2,194,3,196,2,132,3,136,3,8,2,16,5
 DB 32,5,64,5,128,2,0,53,1,1,0,8,128,5,255,1
 DB 159,62,158,4,254,1,156,5,152,3,153,2,145,61
mini_1:
 DB 137,72,143,1,137,71,0,18,32,1,0,33,16,1,0,79
 DB 64,1,0,237,1,1,0,111,8,1,0,57,32,1,0,35
 DB 145,72,241,1,145,71

hiway_0:
 DB 129,5,249,118,121,6,57,6,25,6,137,2,143,1,128,5
 DB 255,84,127,4,63,4,31,4,143,3,135,2,71,2,67,3
 DB 35,1,33,4,17,1,16,4,8,5,4,5,2,5,1,5
 DB 0,3,128,5,255,71,127,2,63,1,31,2,15,1,135,1
 DB 71,1,67,1,33,1,17,1,16,1,8,3,4,2,2,3
 DB 1,5,0,37,4,1,0,5,128,5,255,59,127,2,63,1
 DB 31,2,143,1,135,1,71,1,35,1,33,1,17,1,16,1
 DB 8,1,4,2,2,1,1,2,0,21,16,1,0,40,128,5
 DB 255,51,127,1,63,1,15,1,7,1,131,1,97,1,17,1
 DB 8,2,4,1,2,2,1,1,0,64,32,1,0,10,128,5
 DB 255,44,127,1,63,1,31,1,15,1,3,1,193,1,32,1
 DB 16,1,12,1,2,1,1,1,0,21,64,1,0,38,64,1
 DB 0,23
hiway_1:
 DB 255,1,137,143,0,20,128,1,0,67,128,1,0,255,0,4,128,1
 DB 0,211,6,1,0,29,4,1,0,87,8,1,0,41
hiway_7:
 DB 143,1,137,2,25,6,57,6,121,6,249,118,129,5,0,3
 DB 1,5,2,5,4,5,8,5,16,4,17,1,33,4,35,1
 DB 67,3,71,2,135,2,143,3,31,4,63,4,127,4,255,84
 DB 128,5,0,5,4,1,0,37,1,5,2,3,4,2,8,3
 DB 16,1,17,1,33,1,67,1,71,1,135,1,15,1,31,2
 DB 63,1,127,2,255,71,128,5,0,40,16,1,0,21,1,2
 DB 2,1,4,2,8,1,16,1,17,1,33,1,35,1,71,1
 DB 135,1,143,1,31,2,63,1,127,2,255,59,128,5,0,10
 DB 32,1,0,64,1,1,2,2,4,1,8,2,17,1,97,1
 DB 131,1,7,1,15,1,63,1,127,1,255,51,128,5,0,23
 DB 64,1,0,38,64,1,0,21,1,1,2,1,12,1,16,1
 DB 32,1,193,1,3,1,15,1,31,1,63,1,127,1,255,44
 DB 128,5
hiway_8:
 DB 128,5,255,39,63,1,31,1,7,1,3,1,193,1,32,1
 DB 16,1,12,1,2,1,1,1,0,68,128,2,64,2,32,2
 DB 16,3,8,2,136,3,132,2,196,3,194,1,226,2,128,5
 DB 255,33,127,1,31,1,15,1,7,1,129,1,64,1,48,1
 DB 8,1,6,1,1,1,0,64,128,1,64,1,32,1,16,1
 DB 8,1,4,1,130,1,194,1,193,1,225,1,224,1,240,2
 DB 248,1,252,2,254,2,255,14,128,5,255,31,15,1,1,1
 DB 0,2,240,1,14,1,1,1,0,16,128,1,0,48,192,1
 DB 32,1,24,1,6,1,1,1,128,1,224,1,240,1,252,1
 DB 255,27,128,5,255,29,7,1,0,2,192,1,56,1,7,1
 DB 0,45,2,1,0,19,224,1,28,1,3,1,0,2,240,1
 DB 252,1,255,32,128,5,255,29,0,3,255,1,0,66,254,1
 DB 1,1,0,2,192,1,255,35,128,5,255,29,0,3,255,1
 DB 0,11,16,1,0,53,252,1,3,1,0,2,240,1,255,36
hiway_9:
 DB 62,1,162,142,34,1,0,1,255,142,0,2,255,142,0,2
 DB 255,142,0,2,255,142,0,2,127,142,0,1
hiway_15:
 DB 226,2,194,1,196,3,132,2,136,3,8,2,16,3,32,2
 DB 64,2,128,2,0,15,64,1,0,52,1,1,2,1,12,1
 DB 16,1,96,1,129,1,3,1,7,1,31,1,127,1,255,39
 DB 128,5,255,14,254,2,252,2,248,2,240,1,224,1,193,1
 DB 130,1,4,1,8,1,16,1,32,1,64,1,128,1,0,65
 DB 1,1,6,1,56,1,192,1,0,1,3,1,31,1,255,37
 DB 128,5,0,1,254,1,2,1,250,16,2,1,254,1,0,1
 DB 255,3,252,1,248,1,224,1,0,1,1,1,6,1,56,1
 DB 192,1,0,44,1,1,0,15,128,1,0,4,1,1,14,1
 DB 240,1,0,2,7,1,127,1,255,34,128,5,0,1,255,1
 DB 0,1,255,16,0,1,255,1,0,1,255,7,252,1,240,1
 DB 0,2,3,1,28,1,224,1,0,65,31,1,224,1,0,2
 DB 3,1,63,1,255,32,128,5,0,1,255,1,0,1,255,16
 DB 0,1,255,1,0,1,255,10,192,1,0,2,1,1,254,1
 DB 0,13,32,1,0,51,3,1,252,1,0,3,255,32,128,5
 DB 0,1,127,1,64,1,95,16,64,1,127,1,0,1,255,11
 DB 240,1,0,2,3,1,252,1,0,52,16,1,0,12,255,1
 DB 0,3,255,32,128,5
hiway_25:
 DB 255,66,0,1,62,9,190,68,255,66,0,10,255,134,0,10
 DB 255,134,0,10,255,57,127,1,31,1,15,1,7,2,3,3
 DB 1,3,255,66,0,10,255,50,127,1,63,1,15,1,7,1
 DB 3,1,1,1,0,12,255,66,0,10,255,43,127,1,63,1
 DB 15,1,7,1,3,1,1,1,0,19
hiway_26:
 DB 128,1,190,56,130,86,128,1,255,25,127,3,63,3,31,3
 DB 15,3,7,2,3,3,1,3,0,99,255,2,127,3,63,3
 DB 31,3,15,3,7,3,3,3,1,2,0,255,0,184,128,3,192,2
 DB 224,3,240,2,248,2,252,3,254,2,255,11,1,1,65,85
 DB 1,1
hiway_27:
 DB 143,3,31,4,63,5,127,4,255,128,0,3,1,4,2,5
 DB 4,4,8,5,17,4,35,5,71,4,143,5,31,4,63,5
 DB 127,4,255,92,0,39,1,4,2,5,4,4,8,5,17,4
 DB 35,5,71,4,143,2,255,1,143,2,31,4,63,5,127,4
 DB 255,56,0,75,1,4,2,5,4,4,8,5,17,4,35,5
 DB 71,4,143,5,31,4,63,5,127,4,255,20,0,111,1,4
 DB 2,5,4,4,8,5,17,4,35,5,71,4,143,2,241,3
 DB 226,4,196,5,136,4,16,5,32,4,64,5,128,4,0,110
hiway_28:
 DB 255,255,255,255,255,66,143,3,31,4,63,5,127,4,255,128,0,3,1,4
 DB 2,5,4,4,8,5,17,4,35,5,71,4,143,5,31,4
 DB 63,5,127,4,255,92
hiway_33:
 DB 255,66,0,10,255,36,127,1,63,1,31,1,7,1,3,1
 DB 1,1,0,26,255,66,0,10,255,29,127,1,63,1,31,1
 DB 15,1,3,1,1,1,0,33,255,66,0,10,255,22,127,1
 DB 63,1,31,1,15,1,3,1,1,1,0,38,192,1,224,1
 DB 255,66,0,10,255,17,31,1,15,1,7,1,1,1,0,39
 DB 128,1,224,1,240,1,248,1,254,1,255,69,0,10,255,17
 DB 0,38,192,1,224,1,248,1,252,1,255,75,0,10,255,17
 DB 0,32,128,1,224,1,240,1,252,1,254,1,255,14
hiway_34:
 DB 0,10,128,2,192,3,224,2,240,2,248,3,252,2,254,3
 DB 255,30,0,1,254,85,0,4,128,1,192,1,240,1,248,1
 DB 254,1,255,49,0,1,255,85,0,1,248,1,252,1,255,55
 DB 0,1,255,85,0,1,255,57,0,1,255,85,0,1,255,57
 DB 0,1,255,85,0,1,255,57,0,1,127,85,0,1
hiway_35:
 DB 255,21,254,4,252,5,248,4,241,5,226,4,196,5,136,4
 DB 16,5,32,4,64,5,128,4,0,74,255,57,254,4,252,5
 DB 248,4,241,2,255,1,241,2,226,4,196,5,136,4,16,5
 DB 32,4,64,5,128,4,0,38,255,93,254,4,252,5,248,4
 DB 241,5,226,4,196,5,136,4,16,5,32,4,64,5,128,4
 DB 0,2,255,129,254,4,252,5,248,4,241,2,255,255,255,33
hiway_36:
 DB 0,39,1,4,2,5,4,4,8,5,17,4,35,5,71,4
 DB 143,5,31,3,255,1,63,5,127,4,255,56,0,75,1,4
 DB 2,5,4,4,8,5,17,4,35,5,71,4,143,5,31,4
 DB 63,5,127,4,255,20,0,111,1,4,2,5,4,4,8,5
 DB 17,4,35,5,71,4,143,2,241,3,226,4,196,5,136,4
 DB 16,5,32,4,64,5,128,4,0,110,255,21,254,4,252,5
 DB 248,4,241,5,226,4,196,5,136,4,16,5,32,4,64,5
 DB 128,4,0,74,255,57,254,4,252,5,248,4,241,5,226,3
 DB 254,1,196,5,136,4,16,5,32,4,64,5,128,4,0,38
hiway_37:
 DB 255,255,255,33,143,3,31,4,63,5,127,4,255,128,0,3,1,4
 DB 2,5,4,4,8,5,17,4,35,5,71,4,143,5,31,4
 DB 63,5,127,4,255,92,0,39,1,4,2,5,4,4,8,5
 DB 17,4,35,5,71,4,143,4,255,1,31,4,63,5,127,4
 DB 255,56,0,75,1,4,2,5,4,4,8,5,17,4,35,5
 DB 71,4,143,5,31,4,63,5,127,4,255,20
hiway_41:
 DB 255,66,0,10,255,17,0,32,255,85,0,10,255,17,0,32
 DB 255,85,0,10,255,17,0,32,255,85,0,10,255,17,0,32
 DB 255,85,0,10,255,17,0,32,255,85,0,10,255,17,0,32
 DB 255,19
hiway_44:
 DB 255,93,254,4,252,5,248,4,241,5,226,4,196,5,136,4
 DB 16,5,32,4,64,5,128,4,0,2,255,129,254,4,252,5
 DB 248,4,241,2,255,255,255,255,255,66
hiway_45:
 DB 0,111,1,4,2,5,4,4,8,5,17,4,35,5,71,4
 DB 143,2,241,3,226,4,196,5,136,4,16,5,32,4,64,5
 DB 128,4,0,110,255,21,254,4,252,5,248,4,241,5,226,4
 DB 196,5,136,4,16,5,32,4,64,5,128,4,0,74,255,57
 DB 254,4,252,5,248,4,241,4,255,1,226,4,196,5,136,4
 DB 16,5,32,4,64,5,128,4,0,38,255,93,254,4,252,5
 DB 248,4,241,5,226,4,196,5,136,4,16,5,32,4,64,5
 DB 128,4,0,2,255,129,254,4,252,5,248,4,241,2
hiway_49:
 DB 0,1,254,63,0,1,255,1,0,10,255,17,0,32,1,1
 DB 7,1,15,1,63,1,127,1,255,14,0,1,255,63,0,1
 DB 255,1,0,10,255,17,0,38,3,1,7,1,31,1,63,1
 DB 255,9,0,1,255,63,0,1,255,1,0,10,255,17,248,1
 DB 240,1,224,1,128,1,0,39,1,1,7,1,15,1,31,1
 DB 127,1,255,3,0,1,255,63,0,1,255,1,0,10,255,22
 DB 254,1,252,1,248,1,240,1,192,1,128,1,0,38,3,1
 DB 7,1,0,1,255,63,0,1,255,1,0,10,255,29,254,1
 DB 252,1,248,1,240,1,192,1,128,1,0,34,127,63,0,1
 DB 255,1,0,10,255,36,254,1,252,1,248,1,224,1,192,1
 DB 128,1,0,26
hiway_50:
 DB 255,255,255,177,31,1,63,1,255,142,0,3,1,1,3,1,15,1
 DB 31,1,127,1,255,136,0,10,1,2,3,3,7,2,15,2
 DB 31,3,63,2,127,3,255,117
hiway_56:
 DB 128,5,255,29,254,1,248,1,240,1,193,1,134,1,8,1
 DB 48,1,64,1,128,1,0,60,1,1,6,1,24,1,96,1
 DB 129,1,7,1,31,1,63,1,255,33,128,5,255,34,254,1
 DB 252,1,240,1,224,1,195,1,4,1,24,1,32,1,192,1
 DB 0,19,32,1,0,39,1,1,2,1,12,1,48,1,192,1
 DB 3,1,7,1,31,1,63,1,255,28,128,5,255,40,254,1
 DB 248,1,240,1,193,1,134,1,8,1,16,1,96,1,128,1
 DB 0,58,3,1,4,1,24,1,96,1,128,1,3,1,7,1
 DB 31,1,127,1,255,23,128,5,255,45,254,1,252,1,240,1
 DB 224,1,131,1,4,1,24,1,32,1,64,1,128,1,0,41
 DB 4,1,0,14,1,1,6,1,8,2,16,2,33,2,67,2
 DB 135,2,15,2,31,2,63,2,127,2,255,8,128,5,255,51
 DB 252,1,248,1,224,1,193,1,2,1,12,1,16,1,96,1
 DB 128,1,0,63,1,2,2,3,4,2,8,2,16,1,17,1
 DB 33,1,35,1,67,1,71,1,135,1,128,5,255,56,254,1
 DB 252,1,240,1,224,1,131,1,4,1,8,1,48,1,64,1
 DB 128,1,0,11,2,1,0,51,8,1,0,9
hiway_57:
 DB 255,66,0,10,255,43,254,1,252,1,240,1,224,1,192,1
 DB 128,1,0,19,255,66,0,10,255,50,254,1,252,1,240,1
 DB 224,1,192,1,128,1,0,12,255,66,0,10,255,57,254,1
 DB 248,1,240,1,224,2,192,3,128,3,255,66,0,10,255,68
 DB 143,3,31,4,63,5,127,4,255,50,0,10,255,68,0,3
 DB 1,4,2,5,4,4,8,5,17,4,35,5,71,4,143,5
 DB 31,4,63,5,127,4,255,14,0,1,124,9,125,68
hiway_58:
 DB 0,29,1,3,3,2,7,3,15,2,31,2,63,3,127,2
 DB 255,11,128,1,190,85,128,1,0,255,0,33,255,2,254,3,252,3
 DB 248,3,240,3,224,3,192,3,128,2,0,122,255,25,254,3
 DB 252,3,248,3,240,3,224,2,192,3,128,3,0,99,1,1
 DB 125,142,1,1
hiway_64:
 DB 128,5,255,62,252,1,248,1,224,1,225,1,194,1,196,2
 DB 132,1,136,3,8,1,16,3,32,3,64,3,128,3,0,53
 DB 128,5,255,76,254,3,252,3,248,4,241,3,225,1,226,2
 DB 194,1,196,2,132,1,136,3,16,3,32,4,64,3,128,3
 DB 0,15,32,1,0,11,128,5,255,102,254,3,252,4,248,3
 DB 241,3,226,3,194,1,196,3,136,3,16,3,32,3,64,4
 DB 128,3,0,1,128,5,255,128,254,3,252,4,248,3,241,1
 DB 64,5,255,139,64,5,255,139

tech_1:
 DB 137,72,143,1,137,71,0,44,255,1,0,4,255,1,0,57
 DB 32,1,0,80,255,1,0,4,255,1,0,21,128,6,0,111
 DB 255,1,0,4,255,1,0,21,15,1,0,116,255,1,0,4
 DB 255,1,0,90,4,1,0,3,145,44,241,1,1,4,254,1
 DB 0,8,4,1,0,35,254,1,1,4,241,1,145,44
tech_3:
 DB 137,72,143,1,137,71,0,46,64,1,0,47,255,1,0,4
 DB 255,1,0,111,128,6,0,21,255,1,0,4,255,1,0,58
 DB 8,1,0,57,15,1,0,21,255,1,0,4,255,1,0,138
 DB 255,1,0,4,255,1,0,44,145,44,241,1,1,4,254,1
 DB 0,8,128,1,0,28,2,1,0,6,254,1,1,4,241,1
 DB 145,44
tech_7:
 DB 253,43,1,1,255,1,0,4,255,1,0,6,128,1,0,37
 DB 255,1,0,4,255,1,1,1,253,43,255,43,0,1,255,1
 DB 0,4,255,1,0,36,32,1,0,7,255,1,0,4,255,1
 DB 0,1,255,86,0,1,255,1,0,4,255,1,0,44,255,1
 DB 0,4,255,1,0,1,255,86,0,1,255,1,0,4,255,1
 DB 0,6,128,1,0,15,2,1,0,21,255,1,0,4,255,1
 DB 0,1,255,86,0,1,255,1,0,4,255,1,0,44,255,1
 DB 0,4,255,1,0,1,255,43,191,43,128,1,255,1,0,4
 DB 255,1,0,38,2,1,0,5,255,1,0,4,255,1,128,1
 DB 191,43
tech_13:
 DB 137,44,143,1,128,4,127,1,0,44,127,1,128,4,143,1
 DB 137,44,0,94,255,1,0,4,255,1,0,54,4,1,0,61
 DB 240,1,0,21,255,1,0,4,255,1,0,111,1,6,0,21
 DB 255,1,0,4,255,1,0,93,4,1,0,44,255,1,0,4
 DB 255,1,0,44,145,72,241,1,145,71
tech_31:
 DB 255,1,128,5,255,1,129,1,128,136,255,1,128,5,255,88
 DB 127,1,63,2,31,2,15,2,7,5,3,8,1,9,0,21
 DB 255,1,128,5,255,74,127,1,63,2,31,2,15,2,7,1
 DB 3,2,1,2,0,52,255,1,128,5,255,60,127,1,63,2
 DB 31,2,15,2,7,1,3,2,1,2,0,66,255,1,128,5
 DB 255,56,127,1,31,1,7,1,1,1,0,78,255,1,128,5
 DB 255,52,63,1,15,1,7,1,1,1,0,52,128,2,192,2
 DB 224,3,240,2,248,2,252,3,254,3,255,13
tech_34:
 DB 137,72,143,1,137,71,0,230,1,1,0,36,64,1,0,52
 DB 1,1,0,168,8,1,0,86,241,97,242,1,226,6,196,5
 DB 132,1,136,5,16,5,32,6,64,5,128,5,1,1,0,7
tech_37:
 DB 255,1,128,5,255,48,63,1,15,1,3,1,0,47,128,1
 DB 192,1,224,2,240,1,248,1,252,1,254,2,255,26,128,5
 DB 255,1,128,5,255,46,127,1,0,42,192,1,224,1,240,2
 DB 248,1,252,1,254,1,255,37,128,5,255,1,128,5,255,46
 DB 0,43,255,44,128,5,255,1,128,5,255,45,31,1,0,43
 DB 255,44,128,5,255,1,128,5,255,45,0,43,255,45,128,5
 DB 255,1,128,5,255,44,3,1,0,43,255,45,128,5
tech_40:
 DB 255,120,254,5,252,6,248,5,241,6,226,2,255,255,255,255,255,210
tech_48:
 DB 128,5,255,29,252,1,248,1,224,1,128,1,3,1,12,1
 DB 48,1,192,1,0,62,1,1,6,1,8,1,48,1,64,1
 DB 129,1,7,1,15,1,63,1,127,1,255,25,128,10,255,33
 DB 254,1,248,1,240,1,192,1,1,1,6,1,24,1,96,1
 DB 128,1,0,62,1,1,6,1,8,1,48,1,64,1,129,1
 DB 3,1,15,1,31,1,127,1,255,20,128,10,255,38,252,1
 DB 240,1,224,1,128,1,3,1,12,1,48,1,64,1,128,1
 DB 0,62,1,1,2,1,12,1,16,1,96,1,128,1,3,1
 DB 7,1,31,1,63,1,255,15,128,10,255,42,254,1,248,1
 DB 240,1,224,1,227,1,226,24,130,1,2,1,4,1,24,1
 DB 224,1,0,30,1,1,0,8,3,1,12,1,16,1,96,1
 DB 128,1,3,1,7,1,31,1,63,1,255,10,128,10,255,72
 DB 254,1,248,1,224,1,192,1,1,1,6,1,24,1,96,1
 DB 128,1,0,39,3,1,4,1,24,1,32,1,192,1,1,1
 DB 7,1,15,1,63,1,127,1,255,4,128,10,255,77,252,1
 DB 240,1,192,1,0,1,3,1,12,1,48,1,192,1,0,40
 DB 3,1,4,1,24,1,32,1,192,1,1,1,3,1,15,1
 DB 255,1,128,5
tech_49:
 DB 128,5,255,4,7,41,0,44,255,50,128,5,255,4,0,54
 DB 128,8,255,1,0,22,255,50,128,5,255,4,0,15,255,1
 DB 1,6,0,63,255,50,128,5,255,4,0,85,255,50,128,5
 DB 255,4,0,85,255,50,128,5,255,4,0,29,126,3,0,1
 DB 126,24,0,1,126,28,0,1,126,24,0,1,126,22,0,1
tech_51:
 DB 128,5,255,29,248,1,128,1,0,1,7,1,120,1,128,1
 DB 0,63,7,1,248,1,0,2,7,1,255,31,128,10,255,31
 DB 240,1,0,2,7,1,120,1,128,1,0,13,32,1,0,48
 DB 1,1,62,1,192,1,0,2,63,1,255,29,128,10,255,33
 DB 224,1,0,2,7,1,120,1,128,1,0,43,4,1,0,18
 DB 15,1,240,1,0,2,7,1,255,28,128,10,255,34,254,1
 DB 224,1,0,2,7,1,120,1,128,1,0,61,1,1,126,1
 DB 128,1,0,2,63,1,255,26,128,10,255,36,252,1,192,1
 DB 0,2,7,1,120,1,128,1,0,27,16,1,0,33,3,1
 DB 4,1,8,1,16,1,33,1,67,1,135,1,15,1,31,1
 DB 63,1,127,1,255,19,128,10,255,38,248,1,128,1,0,2
 DB 7,1,120,1,128,1,0,66,1,1,2,1,4,1,8,1
 DB 16,1,17,1,35,1,67,1,135,1,15,1,31,1,63,1
 DB 127,1,255,10,128,5
tech_54:
 DB 128,5,255,80,254,1,252,1,240,1,192,1,1,1,6,1
 DB 24,1,96,1,128,1,0,25,1,1,0,15,255,1,0,2
 DB 255,1,128,10,255,85,252,1,248,1,224,1,128,1,3,1
 DB 12,1,48,1,64,1,128,1,0,36,63,1,33,2,71,1
 DB 72,1,136,1,144,3,128,5,255,89,254,1,248,1,240,1
 DB 192,1,1,1,6,1,24,1,32,1,192,1,0,4,8,1
 DB 0,36,128,5,255,94,252,1,240,1,224,1,128,1,3,1
 DB 12,1,16,1,96,1,128,1,0,36,128,5,255,99,254,1
 DB 248,1,240,1,224,1,131,1,4,1,24,1,32,1,192,1
 DB 0,13,128,4,0,14,128,5,255,105,254,1,248,1,240,1
 DB 241,2,226,3,228,1,196,3,200,2,136,1,144,2,16,17
tech_55:
 DB 128,5,255,1,0,2,255,1,0,29,255,1,0,3,255,97
 DB 128,5,144,3,136,1,72,1,71,1,33,2,63,1,0,29
 DB 255,1,0,3,255,97,128,5,0,31,1,1,0,6,255,1
 DB 0,3,255,97,128,5,0,36,128,1,96,1,31,1,0,2
 DB 128,1,255,97,128,5,0,9,64,1,0,21,192,1,32,1
 DB 24,1,4,1,131,1,224,1,240,1,248,1,254,1,255,99
 DB 128,5,16,17,144,2,136,1,200,2,196,3,228,1,226,3
 DB 241,2,240,1,248,1,254,1,255,105,128,5
tech_57:
 DB 128,5,255,40,252,1,248,1,240,1,224,1,227,1,196,1
 DB 136,1,16,2,32,1,64,1,128,1,0,44,8,1,0,23
 DB 1,1,2,1,4,1,8,1,16,1,33,1,67,1,71,5
 DB 135,1,143,6,128,5,255,48,254,1,252,1,248,1,240,1
 DB 225,1,194,1,196,1,132,1,8,1,16,1,32,1,64,1
 DB 128,1,0,78,128,5,255,57,254,1,252,1,248,1,240,1
 DB 225,1,193,1,194,1,132,1,8,1,16,1,32,1,64,2
 DB 128,1,0,11,64,1,0,56,128,5,255,66,254,1,252,1
 DB 248,1,240,1,224,1,193,1,130,1,132,1,8,1,16,2
 DB 32,1,64,1,128,2,0,58,128,5,255,75,254,1,252,1
 DB 248,2,240,2,241,2,225,2,226,2,194,2,196,2,132,2
 DB 136,2,8,2,16,4,32,4,64,4,128,4,0,9,8,1
 DB 0,16,128,5,255,99,254,4,252,4,248,4,240,2,241,26

;courcebl:
; DB 255, 255, 255, 255, 255, 255, 255, 99
courcebl:
 DB 0,1,254,142,0,2,255,142,0,2,255,142,0,2,255,142
 DB 0,2,255,142,0,2,127,142,0,1
pit_upper:
 DB 128,5,255,29,0,3,255,1,0,65,1,1,2,2,4,1
 DB 9,1,19,2,39,1,79,2,159,1,63,1,127,2,255,14
 DB 0,7,255,1,128,10,255,29,0,3,255,1,0,76,1,2
 DB 2,1,4,1,9,2,19,1,39,2,79,1,159,1,191,1
 DB 63,1,127,1,255,3,0,7,255,1,128,10,255,29,0,3
 DB 255,1,0,88,1,1,2,1,4,2,255,1,0,7,255,1
 DB 128,10,255,29,0,3,255,1,0,92,255,1,0,7,255,1
 DB 128,10,255,29,0,3,255,1,0,59,240,1,16,2,240,1
 DB 16,2,240,1,0,12,48,1,16,7,48,1,0,5,255,1
 DB 0,7,255,1,128,10,255,29,0,3,255,1,0,59,255,1
 DB 0,2,255,1,0,2,255,1,0,26,255,1,0,7,255,1
 DB 128,5
pit_center:
 DB 128,5,255,29,0,3,255,1,0,59,255,1,0,2,255,1
 DB 0,2,255,1,0,12,192,1,64,7,192,1,0,5,255,1
 DB 0,7,255,1,128,10,255,29,0,3,255,1,26,1,22,1
 DB 26,1,22,1,26,1,22,1,26,1,22,1,26,1,22,1
 DB 26,1,22,1,26,1,22,1,26,1,22,1,26,1,22,1
 DB 26,1,22,1,26,1,22,1,26,1,22,1,26,1,22,1
 DB 26,1,22,1,26,1,22,1,26,1,22,1,26,1,22,1
 DB 26,1,22,1,26,1,22,1,26,1,22,1,26,1,22,1
 DB 26,1,22,1,26,1,22,1,26,1,22,1,26,1,22,1
 DB 26,1,22,1,26,1,22,1,26,1,22,1,26,1,22,1
 DB 26,1,255,1,0,2,255,1,0,2,255,1,0,26,255,1
 DB 0,7,255,1,128,10,255,29,0,3,255,1,0,59,255,1
 DB 0,2,255,1,0,2,255,1,0,26,255,1,0,7,255,1
 DB 128,10,255,29,0,3,255,1,0,59,255,1,0,2,255,1
 DB 0,2,255,1,0,12,3,1,1,7,3,1,0,5,255,1
 DB 0,7,255,1,128,10,255,29,0,3,255,1,0,59,255,1
 DB 0,2,255,1,0,2,255,1,0,26,255,1,0,7,255,1
 DB 128,10,255,29,0,3,255,1,0,59,255,1,128,2,255,1
 DB 128,2,255,1,0,12,12,1,4,7,12,1,0,5,255,1
 DB 0,7,255,1,128,5
pit_bottom:
 DB 128,5,255,29,0,3,255,1,0,92,255,1,0,7,255,1
 DB 128,10,255,29,0,3,255,1,0,92,255,1,0,7,255,1
 DB 128,10,255,29,0,3,255,1,0,92,255,1,0,7,255,1
 DB 128,10,255,29,0,3,255,1,0,92,255,1,0,7,255,1
 DB 128,10,255,29,0,3,255,1,0,81,128,2,64,2,32,2
 DB 144,2,200,2,228,1,255,1,0,7,255,1,128,10,255,29
 DB 0,3,255,1,0,66,128,1,64,2,32,2,144,2,200,2
 DB 228,2,242,2,249,2,252,2,254,2,255,8,0,7,255,1
 DB 128,5

;-------------- キャラクター -------------
winner_0:
 db 00,80h,80h,0E0h,0E0h,0E0h,0E0h,0E0h,00,00,00,00,00,00,0FFh,0FDh,0FFh,028h,28h
 db 28h,0EFh,0FDh,0FFh,00,00,00,00,00,00,00,00,00,00,00,00,00,00
 db 01,81h,0F1h,1Fh,08h,08,08,7Fh,0C0h,00,00,00,00,00,0Fh,0FFh,0FFh,0FFh,0FFh,0FFh,0FFh,0FFh
 db 0Fh,00,00,00,00,00,00,0FCh,1Ch,1Ch,1Ch,0FCh,30h,30h,20h
 db 00,1Fh,38h,0F8h,00,0E0h,00,00,0F8h,3Fh,1Ch,00,0C0h,40h,40h,7Fh,7Fh,7Fh,0C1h,7Fh,7Fh,7Fh
 db 40h,40h,0C0h,00,80h,0E0h,78h,0DFh,0EBh,0D5h,0ABh,05Fh,0FEh,0F0h,00
 db 0F0h,10h,10h,1Fh,50h,3Fh,30h,0B0h,5Fh,10h,10h,10h,1Fh,00,00,00,00,01,1Fh,00,00,00,00,00,0FFh,00,03
 db 07h,0FFh,7Ah,0EDh,0BAh,0DFh,0FFh,07h,03,00
 db 0FFh,00,00,00,04,06,05,04,04,00,00,00,00,00,00,00,00,00,00,00,00
 db 00,00,00,03,02,02,02,47h,97h,96h,97h,6Ah,03,02,02,0FEh
winner_1:
 db 00,00,00,00,0F8h,28h,28h,28h,0E8h,38h,00,00
 db 00,0C0h,7Ch,0FFh,0FFh,0FFh,0FFh,0FFh,0FFh,0FFh,7Ch,0C0h
 db 0c0h,41h,41h,7Fh,7Fh,7Fh,0C1h,7Fh,7Fh,7Fh,41h,41h
;-------------- 固定変数群 --------------
;------- メッセージ --------
title_push_z:
 db 14,"Xｷｰ ｦｵｼﾃｸﾀﾞｻｲ!"
title_developper:
 db 12,"2020 TS.SOFT"

title_menu_1:
 db 16,"ｾｯﾃｨﾝｸﾞ  ﾌﾘｰｿｳｺｳ"
title_menu_2:
 db 21,"ｽﾌﾟﾘﾝﾄ  ﾀｲｷｭｳ  ｵﾌﾟｼｮﾝ"
;------- コース --------
cource_maxsize:		;y, x
 db 255, 255

;スピードウェイ
cource0_mapping:
cource0_lap:;スプリント タイキュウ
 db 14, 48
cource0_size:		;y, x
 db 10, 8
cource0_arrangement:
 db 0,1,1,1,1,1,1,2
 db 3,4,4,4,4,4,4,5
 db 6,7,7,7,7,7,7,6
 db 8,9,10,11,12,7,7,6
 db 13,14,15,16,17,18,7,6
 db 19,20,7,7,21,22,23,24
 db 6,25,26,7,7,7,7,7
 db 27,28,29,23,23,1,2,7
 db 30,17,18,7,7,7,5,7
 db 7,21,22,23,23,23,24,7
cource0_carpos:
 dw 114 + 48	;y(2byte)
 dw 46	;x(2byte)
cource0_pitpos:
 dw 129 + 48	;y(2byte)
 dw 115	;x(2byte)

cource0_objlist:	;ID(1byte), pointx(2byte), pointy(2byte), rectx(2byte), recty(2byte) が並ぶ
 db 0, 1
 dw 0, 0, 0, 0		;車1

 db 1, 1
 dw 0, 0, 0, 0		;車2

 db 2, 1
 dw 0, 0, 0, 0		;車3

 db 3, 1
 dw 0, 0, 0, 0		;車4

 db 4, 1
 dw 0, 0, 0, 0		;beep

 db 5, 29        ;障害物
 dw 0, 0, 5, 480
 dw 5, 0, 1147, 3
 dw 1147, 3, 5, 477
 dw 144, 54, 76, 282
 dw 220, 54, 125, 97
 dw 345, 54, 663, 10
 dw 345, 64, 668, 80
 dw 1013, 64, 17, 32
 dw 139, 96, 5, 240
 dw 130, 144, 9, 144
 dw 720, 144, 293, 48
 dw 97, 180, 7, 60
 dw 345, 192, 87, 48
 dw 864, 192, 144, 51
 dw 1008, 192, 5, 48
 dw 288, 240, 288, 48
 dw 864, 285, 283, 54
 dw 432, 288, 432, 51
 dw 210, 336, 10, 41
 dw 345, 336, 87, 7
 dw 1003, 339, 144, 141
 dw 210, 377, 222, 4
 dw 210, 381, 510, 3
 dw 432, 384, 288, 6
 dw 432, 390, 432, 45
 dw 864, 400, 22, 32
 dw 432, 477, 571, 3
 dw 0, 432, 144, 48
 dw 0, 477, 1152, 20	;下天井
;
 db 6, 1				;ピット
 dw 103, 240, 27, 48
;
 ;db 7, 1
 ;dw 0, 211, 139, 16

 db 7, 1				;ゴール
 dw 0, 153+58, 139, 21

 db 0dh, 1				;ゴール 下
 dw 0, 174+58, 139, 21

 db 0eh, 1				;ゴール 上
 dw 0, 132+58, 139, 21
;
 db 8, 4        ;トンネル始まり
 dw 844, 192, 20, 96
 dw 432, 144, 22, 96
 dw 416, 435, 16, 42
 dw 210, 384, 16, 96
; 
 db 9, 4        ;トンネル終わり
 dw 864, 243, 18, 42
 dw 410, 144, 22, 48
 dw 432, 435, 15, 42
 dw 192, 336, 18, 144

 db 80h         ;エンドサイン
 ;db 80h
;
 ;ポイント
 db 40		;ポイントの数
 dw 51, 114
 db 255
 dw 73, 67
 db 185
 dw 111, 36
 db 185
 dw 188, 23
 db 236
 dw 247, 21
 db 236
 dw 324, 10
 db 236
 dw 505, 10
 db 236
 dw 952, 10
 db 236
 dw 1022, 34
 db 185
 dw 1051, 79
 db 185
 dw 1073, 150
 db 236
 dw 1078, 213
 db 120
 dw 1046, 249
 db 100
 dw 994, 270
 db 140
 dw 935, 271
 db 236
 dw 865, 263
 db 236
 dw 631, 203
 db 236
 dw 419, 159
 db 236
 dw 337, 160
 db 120
 dw 294, 176
 db 130
 dw 257, 221
 db 130
 dw 254, 276
 db 130
 dw 275, 314
 db 125
 dw 316, 346
 db 185
 dw 399, 355
 db 236
 dw 494, 346
 db 236
 dw 659, 343
 db 236
 dw 806, 345
 db 180
 dw 875, 358
 db 100
 dw 906, 385
 db 80
 dw 902, 428
 db 80
 dw 885, 452
 db 150
 dw 833, 461
 db 236
 dw 735, 460
 db 236
 dw 432, 443
 db 236
 dw 116, 369
 db 236
 dw 90, 343
 db 200
 dw 70, 300
 db 200
 dw 58, 257
 db 236
 dw 50, 188
 db 255
 
;モント シティ
cource1_mapping:
cource1_lap:;スプリント タイキュウ
 db 15, 56
cource1_size:		;y, x
 db 10, 6
cource1_arrangement:
 db 0,1,0,2,0,0
 db 3,4,3,4,3,3
 db 5,6,2,4,3,3
 db 7,3,4,4,3,3
 db 7,3,4,4,3,3
 db 8,9,0,0,10,11
 db 12,13,4,7,14,15
 db 16,4,4,7,4,7
 db 17,18,4,19,3,7
 db 20,21,4,22,0,23
cource1_carpos:
 dw 114 + 48 * 3	;y(2byte)
 dw 46	;x(2byte)
cource1_pitpos
 dw 129 + 48 * 3;y(2byte)
 dw 115	;x(2byte)

cource1_objlist:	;ID(1byte), pointx(2byte), pointy(2byte), rectx(2byte), recty(2byte) が並ぶ
 db 0, 1
 dw 0, 0, 0, 0		;車1

 db 1, 1
 dw 0, 0, 0, 0		;車2

 db 2, 1
 dw 0, 0, 0, 0		;車3

 db 3, 1
 dw 0, 0, 0, 0		;車4

 db 4, 1
 dw 0, 0, 0, 0		;beep

 db 5, 31         ;障害物
 dw 0, 0, 5, 480
 dw 5, 0, 184, 99
 dw 189, 0, 5, 41
 dw 194, 0, 670, 4
 dw 526, 4, 338, 44
 dw 243, 44, 45, 203
 dw 288, 44, 189, 199
 dw 532, 48, 332, 195
 dw 96, 99, 93, 1
 dw 238, 103, 5, 144
 dw 139, 144, 99, 103
 dw 130, 240, 8, 8
 dw 859, 243, 5, 237
 dw 130, 248, 19, 136
 dw 149, 248, 1, 89
 dw 97, 276, 7, 60
 dw 288, 285, 149, 195
 dw 571, 285, 5, 51
 dw 283, 288, 5, 95
 dw 720, 304, 5, 127
 dw 725, 304, 17, 32
 dw 571, 336, 149, 95
 dw 149, 337, 39, 46
 dw 244, 337, 39, 46
 dw 138, 384, 11, 1
 dw 139, 385, 10, 59
 dw 185, 425, 103, 7
 dw 576, 431, 144, 4
 dw 283, 432, 5, 48
 dw 576, 477, 283, 3
 dw 0, 481, 864, 12  ;下の天井
;
 db 6, 1				;ピット
 dw 103, 336, 27, 48
;
 ;db 7, 1
 ;dw 0, 304, 139, 16

 db 7, 1				;ゴール
 dw 0, 153+151, 139, 21

 db 0dh, 1				;ゴール 下
 dw 0, 174+151, 139, 21

 db 0eh, 1				;ゴール 上
 dw 0, 132+151, 139, 21

 db 0ah, 2      ;高架下
 dw 477, 217, 55, 26
 dw 437, 285, 134, 22
;
 db 0bh, 2      ;高架上
 dw 571, 243, 27, 42
 dw 410, 243, 27, 42
 
 db 0ch, 1      ;高架交差
 dw 437, 243, 134, 42
 
;
 db 80h         ;エンドサイン
 ;db 80h
;
 ;ポイント
 db 43		;ポイントの数
 dw 46, 222
 db 255
 dw 47, 193
 db  255
 dw 50, 156
 db  255
 dw 56, 140
 db  165
 dw 103, 125
 db  146
 dw 140, 116
 db  255
 dw 193, 110
 db  136
 dw 210, 47
 db  175
 dw 238, 22
 db  145
 dw 292, 9
 db  255
 dw 374, 8
 db  255
 dw 442, 8
 db  255
 dw 488, 31
 db  136
 dw 508, 69
 db  255
 dw 514, 111
 db  255
 dw 498, 243  ;高架下
 db  253      ;消す
 dw 498, 289  ;高架下
 db  252      ;表示
 dw 499, 363
 db  255
 dw 520, 410
 db  175
 dw 561, 448
 db  167
 dw 625, 452
 db  255
 dw 691, 453
 db  255
 dw 746, 447
 db  182
 dw 779, 431
 db  181
 dw 793, 387
 db  191
 dw 787, 347
 db  215
 dw 770, 300
 db  191
 dw 725, 269
 db  182
 dw 648, 258
 db  180
 dw 570, 254
 db  255
 dw 369, 250
 db  255
 dw 299, 250
 db  255
 dw 242, 271
 db  155
 dw 216, 304
 db  180
 dw 213, 350
 db  136
 dw 213, 386
 db  80
 dw 167, 411
 db  50
 dw 162, 439
 db  50
 dw 140, 453
 db  50
 dw 107, 429
 db  255
 dw 82, 376
 db  255
 dw 67, 336
 db  255
 dw 60, 284
 db  255

;?だいたいコース情報473byte?
cource2_mapping:
cource2_lap:;スプリント タイキュウ
 db 18, 64
cource2_size:		;y, x
 db 7, 5
cource2_arrangement:
 db 1, 3, 2, 6, 6
 db 0, 1, 4, 6, 6
 db 7, 0, 1, 3, 2
 db 8, 0, 5, 2, 0
 db 9, 5, 3, 4, 0
 db 0, 6, 6, 6, 0
 db 5, 3, 3, 3, 4
cource2_carpos:
 dw 114	;y(2byte)
 dw 46	;x(2byte)
cource2_pitpos:
 dw 129	;y(2byte)
 dw 115	;x(2byte)
cource2_objlist:	;ID(1byte), pointx(2byte), pointy(2byte), rectx(2byte), recty(2byte) が並ぶ
 db 0, 1
 dw 0, 0, 0, 0		;車1

 db 1, 1
 dw 0, 0, 0, 0		;車2

 db 2, 1
 dw 0, 0, 0, 0		;車3

 db 3, 1
 dw 0, 0, 0, 0		;車4

 db 4, 1
 dw 0, 0, 0, 0		;beep
 
 db 5, 22			;障害物 領域
 dw 0, 0, 5, 153
 dw 5, 0, 715, 3
 dw 427, 3, 293, 96
 dw 144, 45, 144, 4
 dw 139, 49, 5, 2
 dw 149, 49, 139, 2
 dw 139, 51, 10, 186
 dw 288, 93, 139, 3
 dw 130, 96, 9, 141
 dw 283, 96, 5, 96
 dw 293, 96, 134, 3
 dw 288, 99, 5, 90
 dw 715, 99, 5, 237
 dw 97, 132, 7, 60
 dw 432, 141, 150, 6
 dw 571, 147, 11, 144
 dw 0, 157, 5, 176
 dw 293, 189, 139, 6
 dw 288, 192, 5, 3
 dw 130, 237, 441, 3
 dw 139, 240, 432, 51
 dw 0, 333, 715, 20	;下の天井
 
 db 6, 1				;ピット
 dw 103, 192, 27, 48

 db 7, 1				;ゴール
 dw 0, 153, 139, 21

 db 0dh, 1				;ゴール 下
 dw 0, 174, 139, 21

 db 0eh, 1				;ゴール 上
 dw 0, 132, 139, 21

 db 0fh, 3
 dw 288, 46, 31, 4
 dw 432, 190, 31, 4
 dw 398, 142, 34, 4

 db 80h

 ;ポイント
 db 33;28		;ポイントの数
 
 dw 45, 74
  db 255
 dw 45, 41
  db 255
 dw 91, 16
  db 165
 dw 144, 12
  db 180
 dw 207, 11
  db 190
 dw 303, 11
  db 100
 dw 326, 42
  db 80
 dw 302, 64
  db 110
 dw 260, 65
  db 145
 dw 234, 77
  db 165
 dw 207, 132
  db 190
 dw 207, 176
  db 255
 dw 237, 203
  db 155
 dw 310, 208
  db 255
 dw 370, 216
  db 110
 dw 410, 216
  db 100
 dw 470, 188
  db 90
 dw 447, 157
  db 140
 dw 396, 164
  db 138
 dw 370, 138
  db 90
 dw 385, 109
  db 100
 dw 406, 106
  db 255
 dw 587, 105
  db 190
 dw 634, 136
  db 160
 dw 657, 235
  db 255
 dw 648, 281
  db 200
 dw 605, 305
  db 190
 dw 516, 310
  db 255
 dw 114, 314
  db 255
 dw 80, 314
  db 255
 dw 62, 276
  db 165
 dw 44, 215
  db 255
 dw 43, 140
  db 255


;cource3_mapping:
;cource3_lap:;スプリント タイキュウ
; db 18, 99
;cource3_size:		;y, x
; db 9, 6
;cource3_arrangement:
; db 0, 1, 2, 3, 4
; db 5, 6, 7, 8, 9
; db 10, 11, 12, 13, 14
; db 15, 16, 17, 18, 19
;cource3_carpos:
; dw 66	;y(2byte)
; dw 66	;x(2byte)
;cource3_pitpos
; dw 10	;y(2byte)
; dw 10	;x(2byte)

;---------- 車 ------------
car_acceleration_table:	;加速度テーブル 0rpmから100ごとの1fpsあたりの加速度 127で1km/(s^2 fps)に相当 車の加速度
 db 49,49,49,49,49,49,49,49,49,49,49,49,49,49,49,49,49,49,49,49,49,49,49,49,49,49,49,49,49
 db 49,50,51,52,53,54,54,55,56,57,58,59,59,60,61,62,63,64,64,65,66,67,68,69,69,70,71
 db 72,73,74,74,75,76,77,78,79,80,80,81,81,82,82,83,83,84,84,85,85,86,86,87,87,88,88
 db 89,89,90,90,91,91,92,92,93,93,93,93,93,93,93,93,93,93,93,93,94,94,94,94,94,94,94
 db 94,94,94,94,95,95,95,95,95,95,95,95,95,95,95,96,95,95,95,95,95,95,95,95,95,95,95
 db 94,94,94,94,94,94,94,94,94,94,94,94,12,12,12,6,6,3,3,0,0,0,0,0,0,0,0,0,0
 db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
 db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
 db 0,0,0,0,0,0,0,0,0,0,

car_acceleration_tune_table: ;チューン設定による加速度増分 0 ~ 6600 rpm の 67段階分
 db 1,1,2,3,5,6,7,9,10,11,12,14,15,16,18,19,20,21,23,24,25,27,28,29
 db 31,27,28,29,31,29,30,31,31,30,29,28,27,26,25,24,23,22,21,20,20,19
 db 18,17,16,15,14,13,12,11,10,10,9,8,7,6,5,4,3,2,1,1,1

;------- 敵の設定 --------
;設定するの難しい
car_coms_speedlevel:
 db 35;ルイス
 db 11;シューマッハ
 db 1;セナ
car_coms_corner_level:
 db 2;ルイス
 db 3;シューマッハ
 db 4;セナ
car_coms_names_1:
 db "RUIS"
 db "SMAH"
 db "A.SE"
car_coms_names_2:
 db "ﾎﾟﾋﾉ"
 db "STTK"
 db "KYUK"
car_coms_names_3:
 db "ﾓﾃｷﾞ"
 db "ｽｽﾞｷ"
 db "ﾌｼﾞ "
;---------- 初期パラメータ 1 ~ 8等 25byte -----------
car_engine_normal:			;丈夫 1 ~ 8 軽い
 db 5
car_tune_normal:			;低回転域 1 ~ 8 ピーキー
 db 4
car_radiator_normal:		;空力重視 1 ~ 8 機能重視
 db 5
car_brake_normal:			;ブレーキ強さ 1 ~ 8
 db 4
car_engine_brake_normal:	;エンジンブレーキ強さ 1 ~ 8
 db 5
car_tire_normal:			;耐久性 1 ~ 4 グリップ
 db 5
car_wing_normal:			;コーナー重視 1 ~ 8 最高速重視
 db 3
car_hundle_normal:			;軽い 1 ~ 8 重い - 移動幅を制限する重さで、pwm制御する感じではない
 db 3
car_suspension_normal:		;耐久性 1 ~ 8 グリップ重視 値によって速度ごとの荷重変化のしやすさ、荷重限界値が変わる。
 db 5
car_transmission_normal:	;0bit~2bit:今のシフト 3bit~5bit:壊れているシフト gear 6bit:(1:manual 0:automatice)
 db 0
car_gear_gear_normal:	;ギア比 1~8
 db 4
car_gear_shiftup_normal:	;シフトアップ value*100 rpm
 db 121
car_rpm_normal:		;回転数ダミー
 db 5
car_gear_retio_normal:		;ギアレシオ 1.00 で レッドゾーン手前245キロ出せる
 dw 0130h, 0cbh, 9ch, 74h, 5eh, 4fh
 

car_engine_endrun:			;軽い1 ~ 8 丈夫
 db 7
car_tune_endrun:			;低回転域 1 ~ 8 ピーキー
 db 4
car_radiator_endrun:		;空力重視 1 ~ 8 機能重視
 db 7
car_brake_endrun:			;ブレーキ強さ 1 ~ 8
 db 4
car_engine_brake_endrun:	;エンジンブレーキ強さ 1 ~ 8
 db 5
car_tire_endrun:			;耐久性 1 ~ 8 グリップ
 db 2
car_wing_endrun			;コーナー重視 1 ~ 8 最高速重視
 db 4
car_hundle_endrun:			;軽い 1 ~ 8 重い - 移動幅を制限する重さで、pwm制御する感じではない
 db 5
car_suspension_endrun:	;耐久性 1 ~ 8 グリップ重視
 db 4
car_transmission_endrun:	;0bit~2bit:今のシフト 3bit~5bit:壊れているシフト gear 6bit:(1:manual 0:automatice)
 db 0
car_gear_gear_endrun:	;ギア比 1 ~ 8
 db 7
car_gear_shiftup_endrun:	;シフトアップ value*100 rpm
 db 121
car_rpm_endrun:			;回転数ダミー
 db 0
car_gear_retio_endrun:		;ギアレシオ 1.00 で レッドゾーン手前245キロ出せる
 dw 0130h, 0cbh, 9ch, 74h, 5eh, 4fh

car_engine_hispeed:			;丈夫 1 ~ 8 軽い
 db 3
car_tune_hispeed:			;低回転域 1 ~ 8 ピーキー
 db 7
car_radiator_hispeed:		;空力重視 1 ~ 8 機能重視
 db 3
car_brake_hispeed:			;ブレーキ強さ 1 ~ 8
 db 5
car_engine_brake_hispeed:	;エンジンブレーキ強さ 1 ~ 8
 db 5
car_tire_hispeed:			;耐久性 1 ~ 4 グリップ
 db 5
car_wing_hispeed:			;コーナー重視 1 ~ 8 最高速重視
 db 6
car_hundle_hispeed:			;軽い 1 ~ 8 重い - 移動幅を制限する重さで、pwm制御する感じではない
 db 5
car_suspension_hispeed:	;耐久性 1 ~ 8 グリップ重視
 db 5
car_transmission_hispeed:	;0bit~2bit:今のシフト 3bit~5bit:壊れているシフト gear 6bit:(1:manual 0:automatice)
 db 0
car_gear_gear_hispeed:	;ギア比 1 ~ 8
 db 7
car_gear_shiftup_hispeed:	;シフトアップ value*100 rpm
 db 147
car_rpm_hispeed:		;回転数ダミー
 db 0
car_gear_retio_hispeed:		;ギアレシオ 1.00 で レッド??ーン手前245キロ出せる
 dw 280, 182, 138, 101, 82, 70

car_engine_technical:			;軽い 1 ~ 8 丈夫
 db 2
car_tune_technical:			;低回転域 1 ~ 8 ピーキー
 db 5
car_radiator_technical:		;空力重視 1 ~ 8 機能重視
 db 4
car_brake_technical:			;ブレーキ強さ 1 ~ 8
 db 7
car_engine_brake_technical:	;エンジンブレーキ強さ 1 ~ 8
 db 7
car_tire_technical:			;耐久性 1 ~ 4 グリップ
 db 6
car_wing_technical:			;コーナー重視 1 ~ 8 最高速重視
 db 2
car_hundle_technical:			;軽い 1 ~ 8 重い - 移動幅を制限する重さで、pwm制御する感じではない
 db 5
car_suspension_technical:		;耐久性 1 ~ 8 グリップ重視
 db 6
car_transmission_technical:	;0bit~2bit:今のシフト 3bit~5bit:壊れているシフト gear 6bit:(1:manual 0:automatice)
 db 0
car_gear_gear_technical:	;ギア比 1~8
 db 5
car_gear_shiftup_technical:	;シフトアップ value*100 rpm
 db 132
car_rpm_technical:		;回転数ダミー
 db 0
car_gear_retio_technical:		;ギアレシオ 1.00 で レッドゾーン手前245キロ出せる
 dw 138h, 0d2h, 0a2h, 79h, 62h, 52h

;--------------- 変数群 -----------------
reserved_0:
 db overwrite
reserved_1:
 db overwrite
speed_wait:
 db 8
objlist:	;車、障害物、ピット、ゴールあらゆる物体のIDを格納
 ds 48		;48までで済めば一番いいけどそうも行かないっぽいね
objlist_pos_rect:
 ds 48 * 8	;オブジェクトの座標、サイズを格納 x, y, width, height
objlist_point_num:
 db overwrite
objlist_points:;CPUを操作するポイント x, y, speed(1byte)
 ds 48 * 5
;------- 現在のfetchNo -----------
objno:
 db overwrite	;0 ~ 3
;------- 車変数その2 ---------
g_count_1:
 db overwrite, overwrite
g_count_2:
 db overwrite, overwrite
g_count_3:
 db overwrite, overwrite
g_count_4:
 db overwrite, overwrite
car_crash_fl:
 db overwrite ;自車クラッシュ中なら32
car_pitin:
 db overwrite ;各車ピットイン中か
 db overwrite
 db overwrite
 db overwrite
car_pit_pos:;x x y y ...
 db overwrite, overwrite, overwrite, overwrite
 db overwrite, overwrite, overwrite, overwrite
 db overwrite, overwrite, overwrite, overwrite
 db overwrite, overwrite, overwrite, overwrite
;------- 車 自車のみ-------
car_physics:
car_dir:			;0 ~ 31   グラフィック描画用 32でクラッシュ
 db overwrite
 db overwrite
 db overwrite
 db overwrite
car_degree:			;0  ~ 65535 ドリフト角度調整用
 db overwrite, overwrite
car_acceleration:	;0 ~ 255 127で1km/(s^2 fps)に相当 車の加速度
 db overwrite
 db overwrite
 db overwrite
 db overwrite
car_speed:			;0 ~ 255 実速度を再現(前に進む速度) = タイヤのスピード - 空転速度y
 db overwrite
 db overwrite
 db overwrite
 db overwrite
car_slip_x:			;横に空転しているか
 db overwrite
car_gravity:		;重力(y, x) -128 ~ 127, -128 ~ 127
 db overwrite, overwrite
car_pos_x:			;位置 x 0 ~ 65535
 db overwrite, overwrite
 db overwrite, overwrite
 db overwrite, overwrite
 db overwrite, overwrite
car_pos_y:			;位置 y 0 ~ 65535
 db overwrite, overwrite
 db overwrite, overwrite
 db overwrite, overwrite
 db overwrite, overwrite
car_pos_mod_x:		;描画位置補正 x 0 ~ 255
 db overwrite
car_pos_mod_y:		;描画位置補正 y 0 ~ 255
 db overwrite
 
car_temp_values:
CARMOVE_ADD_DEG_VALUE_CACHE:
 db overwrite 	;objno = 0
CARMOVE_HUNDLE_SLIPDIR:
 db overwrite 	;objno = 0
CARMOVE_HUNDLE_QUICK:
 db overwrite 	;objno = 0
 db overwrite 	;objno = 1
 db overwrite 	;objno = 2
 db overwrite 	;objno = 3
;ポイント
CARMOVE_NOW_POINT:
 db overwrite 	;objno = 0
 db overwrite 	;objno = 1
 db overwrite 	;objno = 2
 db overwrite 	;objno = 3
CARMOVE_COMPUTER_SPD:	;目標速度
 ;db 40, overwrite, overwrite, overwrite
 db 40, 40, 40, 40;overwrite, overwrite 	;objno = 0, 1, 2, 3
CARMOVE_AUTO_PITIN_FLAG:
 db overwrite, overwrite, overwrite, overwrite
lap_end:
 db overwrite ;fps分ループさせて、fps>=(lap_end)かつ敵車がラップする、でレース終了 1:ゴール 2:画面外に自車が消える
 db overwrite
 db overwrite
 db overwrite

;--------- カレント変数 not reserved ---------
CARMOVE_COMPUTER_NEXT_crnt:
 dw overwrite
CARMOVE_COMPUTER_NEXT_crnt_v:
 dw overwrite
CARMOVE_AUTO_PITIN_W_T_crnt:
 dw overwrite
CARMOVE_AUTO_PITIN_W_T_crnt_v:
 dw overwrite

car_st_value:
car_engine_st:		;設定値0 ~ 3bit 壊れたか壊れてないか 4bit
 db 5
car_tune_st:		;設定値0 ~ 3bit
 db 4
car_radiator_st:	;設定値0 ~ 3bit 壊れたか壊れてないか 4bit
 db 5
car_brake_st:		;設定値0 ~ 3bit 効かない1 ~ 8効く 7bit:trueで壊れている
 db 4
car_engine_brake_st:;設定値0 ~ 3bit 効かない1 ~ 8効く
 db 5
car_tire_st:		;タイヤのセッティング
 db 5
car_wing_st:		;設定値0 ~ 3bit 壊れたか壊れてないか trueで壊れてる 4bit
 db 3
car_hundle_st:		;設定値0 ~ 3bit 1軽い 8重い
 db 3
car_suspension_st:	;設定値0 ~ 3bit 壊れたか壊れてないか trueで壊れてる 4bit
 db 5
car_transmission_st:;0bit~2bit:今のシフト 3bit~5bit:壊れているシフト gear
 db 0
car_transmission_gear_st:	;ギア比 直接参照される値ではない ここからギアレシオを算出
 db 4
car_transmission_shiftup_st:;シフトアップ	*100prm
 db 121
car_rpm_st:   ;エンジン回転数 *100スケール 11 ~ 160
 db overwrite
 ;------ 16ビット -------
car_gear_ratio_st:	;設定されているギアレシオ
 ;dw 314, 182, 128, 102, 86, 74
 dw 314, 182, 128, 102, 86, 74
car_tire_rest_st:	;タイヤの残り。0になるとまともにグリップしない
 dw tire_restmax
 ;----- レース時に設定 -------
car_fuel_st:		;ガソリン量 0 ~ 65535
 db overwrite, overwrite
car_transmission_shiftdown_st:;シフトダウン(シフトごと)	*100prm レース開始時に設定
 db overwrite, overwrite, overwrite, overwrite, overwrite, overwrite
car_weight_st:		;重さ 1 ~ 255
 db overwrite
car_gear_speed_table_st:;ギア回転数ごとのスピード 16000rpmまで レース開始時に設定
 ds 160 * 6
car_elect_st: ;電気系 7ビット目:trueで故障
 db overwrite
car_weight_and_fuel_st: ;ガソリンを含んだ重さ
 db overwrite

;--------- セーブ ----------
SAVEDATA:
SAVE_NAME:  ;エントリーネーム
 db overwrite, overwrite, overwrite, overwrite, overwrite, overwrite, overwrite, overwrite
SAVE_NEXT_COURCE:
 db overwrite ;コース番号
SAVE_CLASS:
 db overwrite ;クラス
SAVE_POINT:
 db overwrite ;自機の獲得ポイント
 db overwrite ;敵1の獲得ポイント
 db overwrite ;敵2の獲得ポイント
 db overwrite ;敵3の獲得ポイント
SAVE_RECORD:  ;各コースのベストラップ モードは問わない
 dw 270fh;9999;overwrite, overwrite
 dw 270fh;9999;overwrite, overwrite
 dw 270fh;9999;overwrite, overwrite
 ;dw 270fh;9999;overwrite, overwrite
SAVE_SETTING:
;FILENAME = 8bytes
;Setting = 25bytes
 db overwrite, overwrite, overwrite, overwrite
 db overwrite, overwrite, overwrite, overwrite
 ds 25
;FILENAME = 8bytes
;Setting = 25bytes
 db overwrite, overwrite, overwrite, overwrite
 db overwrite, overwrite, overwrite, overwrite
 ds 25
;FILENAME = 8bytes
;Setting = 25bytes
 db overwrite, overwrite, overwrite, overwrite
 db overwrite, overwrite, overwrite, overwrite
 ds 25

;-------- 展開メモリ ---------
;01
;23
fram_0:
 ds screen
fram_1:
 ds screen
fram_2:
 ds screen
fram_3:
 ds screen

;------- ゲーム変数 ---------
game_class:
 db overwrite   ;2:ノービス 1:全日本 0:ワールドクラス
game_mode:
 db overwrite   ;ゲームモード 1:フリー走行 2:スプリントレース 3:タイキュウ
;周回処理をした時点で、残りラップ数を一つ減らしてラップタイムをリセットする。
;順位を出すときは残りラップ数を見て少ない車から順位を振る。
;残りラップ数が同じのときは、ラップタイムの大きさで大きいほうが順位が前
lap_max:
 db overwrite
lap_remind:     ;残り周回数
 db overwrite
 db overwrite
 db overwrite
 db overwrite
lap_time:      ;ラップタイム
 db overwrite, overwrite
rank:           ;順位
 db overwrite
 db overwrite
 db overwrite
 db overwrite
;------- コース ---------
cource_current:
 db overwrite       ;選択したコースNo
cource_inited:
 db overwrite       ;最初のUNZIPが実行されたか (0:no 1;yes)
cource_size:		;y, x (レジスタH=x, L=y)
 db overwrite, overwrite
cource_arrangement_address:
 db overwrite, overwrite
scroll_vec:			;y, x ((0, 0)の状態で一画面のみ標準表示 y=0~65535 x=0~65535)
 db overwrite, overwrite, overwrite, overwrite
scroll_block:		;y(8ドットを位置ブロックとして開始位置をいくら下にするか y 0~5
 db overwrite
scroll_pos_x:		;x(描画開始位置をどこにするか x 0~143
 db overwrite
current_bg_index:	;現在の左上BGindex
 db overwrite
bg_sizetotal:	;mapsize x * y
 db overwrite
;----------- 一次状態変数 --------------
;CARMOVE
CARMOVE_MOVEPOS_VEC:
 db overwrite, overwrite 	;objno = 0
 db overwrite, overwrite 	;objno = 1
 db overwrite, overwrite 	;objno = 2
 db overwrite, overwrite 	;objno = 3
CARMOVE_ADD_DIRECTION_VALUE_CACHE:
 db overwrite, overwrite 	;objno = 0
 db overwrite, overwrite 	;objno = 1
 db overwrite, overwrite 	;objno = 2
 db overwrite, overwrite 	;objno = 3
;ハンドル
CARMOVE_HUNDLE_V:	;Hbyteが1になると曲がれる
 db overwrite, overwrite 	;objno = 0
 db overwrite, overwrite 	;objno = 1
 db overwrite, overwrite 	;objno = 2
 db overwrite, overwrite 	;objno = 3
CARMOVE_COMPUTER_NEXT:	;次に目指す座標
 ;dw 46, 74, overwrite, overwrite
 dw 46, 74	;objno = 0
 dw 46, 74	;objno = 1
 dw 46, 74;overwrite, overwrite, overwrite, overwrite	;objno = 2
 dw 46, 74;overwrite, overwrite, overwrite, overwrite	;objno = 3
ENGINE_BLOW:
 db overwrite, overwrite  ;レッドゾーンの増分の加減とエンジン耐久度が加わっていく HiByteがFFになったら何かしら故障して減速 修理もすごい時間
ENGINE_OVERHEET:  ;(常に16 - エンジン耐久度(0 ~ 7))くらいが加わり、ラジエータの値とスピード/16の値を引く HiByteが1になったらTEMP HIちょっとずつ鈍くなる
 db overwrite, overwrite

;------------ 現在のカレントメモリ reserved -------------
g_count_crnt:
 dw overwrite
g_count_crnt_v:
 dw overwrite
car_pit_pos_crnt:
 dw overwrite
car_pit_pos_crnt_v:
 dw overwrite
car_pos_x_crnt:
 dw overwrite
car_pos_x_crnt_v:
 dw overwrite
car_pos_y_crnt:
 dw overwrite
car_pos_y_crnt_v:
 dw overwrite
CARMOVE_MOVEPOS_VEC_crnt:
 dw overwrite
CARMOVE_MOVEPOS_VEC_crnt_v:
 dw overwrite
CARMOVE_ADD_DIRECTION_VC_crnt:
 dw overwrite
CARMOVE_ADD_DIRECTION_VC_crnt_v:
 dw overwrite
CARMOVE_HUNDLE_V_crnt:
 dw overwrite
CARMOVE_HUNDLE_V_crnt_v:
 dw overwrite
 
;---------------------------------------------
sign_fram_0:
 ds 35
sign_fram_1:
 ds 35

jp endpoint
end